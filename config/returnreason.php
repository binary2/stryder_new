<?php
return [
"0" => "Select Return Reason",
"1" => "Product dose not fit well ",
"2" => "Incorrect product received ",
"3" => "Defective product received ",
"4" => "Quantity Unsatitsfactory ",
"5" => "I got an incomplete delivery ",
"6" => "I got a delayed delivery ",
"7" => "I changed my mind ",
"8" => "Look & Feel differs from what i show online ",
"9" => "It doesn't look good on me",
"10" => "The package seems to have been tampered with",
"11" => "Other"
];
