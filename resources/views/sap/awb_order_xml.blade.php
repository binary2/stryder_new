<?php echo '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>'; ?>

<STATUSUPDATE>
    <ORDERNO>{{$order_no}}</ORDERNO>
    @foreach($order_items as $key => $line_items)
        <ITEMSTATUS>
            <ITEMID>{{$line_items['shopify_item_id']}}</ITEMID>
            <REFID>{{$line_items['sap_id']}}</REFID>
            <PLANT>{{$line_items['store_code']}}</PLANT>
            <STATUSCODE>02</STATUSCODE>
            <REMARKS>{{$line_items['status']}}</REMARKS>
            <INVOICENO>{{$line_items['sap_pack_id']}}</INVOICENO>
            <AWBNO>{{$line_items['awb_number']}}</AWBNO>
            <RETURNAWBNO></RETURNAWBNO>
        </ITEMSTATUS>
     @endforeach
</STATUSUPDATE>