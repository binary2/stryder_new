<?php echo '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>'; ?>
<ORDER>
    <ORDERNO>{{ $SAPOrderNo }}</ORDERNO>

    <ORDERDATE><?= date('Y-m-d',strtotime($order_data['created_at'])) ?></ORDERDATE>
    <ORDERTIME><?= date('H:m:s',strtotime($order_data['created_at'])) ?></ORDERTIME>
    <ADDRESSINFO>
        <ADDRESSTYPE>SA</ADDRESSTYPE>
        <FIRSTNAME>{{$order_data['shipping_address']['first_name']}}</FIRSTNAME>
        <LASTNAME>{{$order_data['shipping_address']['last_name']}}</LASTNAME>
        <PHONENO>{{$order_data['shipping_address']['phone']}}</PHONENO>
        <EMAILID>{{$order_data['email']}}</EMAILID>
        
        <ADDRESS1>{{$order_data['shipping_address']['address1']}}</ADDRESS1>
        <ADDRESS2>{{$order_data['shipping_address']['address2']}}</ADDRESS2>
        <LANDMARK></LANDMARK>
        <CITY>{{$order_data['shipping_address']['city']}}</CITY>

    @if(array_key_exists(strtoupper($order_data['shipping_address']['province']),$state_code))
    <STATECODE>{{ $state_code[strtoupper($order_data['shipping_address']['province'])] }}</STATECODE>
    @endif
     <STATENAME>{{ $order_data['shipping_address']['province'] }}</STATENAME>
        <PINCODE>{{$order_data['shipping_address']['zip']}}</PINCODE>
        <COUNTRY>{{$order_data['shipping_address']['country']}}</COUNTRY>
    </ADDRESSINFO>
    <ADDRESSINFO>
        <ADDRESSTYPE>BA</ADDRESSTYPE>
        <FIRSTNAME>{{$order_data['billing_address']['first_name']}}</FIRSTNAME>
        <LASTNAME>{{$order_data['billing_address']['last_name']}}</LASTNAME>
        <PHONENO>{{$order_data['billing_address']['phone']}}</PHONENO>
        <EMAILID>{{$order_data['email']}}</EMAILID>
        
        <ADDRESS1>{{$order_data['billing_address']['address1']}}</ADDRESS1>
        <ADDRESS2>{{$order_data['billing_address']['address2']}}</ADDRESS2>
        <LANDMARK></LANDMARK>
        <CITY>{{$order_data['billing_address']['city']}}</CITY>
    @if(array_key_exists(strtoupper($order_data['shipping_address']['province']),$state_code))
    <STATECODE>{{ $state_code[strtoupper($order_data['shipping_address']['province'])] }}</STATECODE>
    @endif
    <STATENAME>{{ $order_data['shipping_address']['province'] }}</STATENAME>
        <PINCODE>{{$order_data['billing_address']['zip']}}</PINCODE>
        <COUNTRY>{{$order_data['billing_address']['country']}}</COUNTRY>
    </ADDRESSINFO>
    <PAYMENTINFO>
        <TOTALPAIDAMT>{{ $order_total_price }}</TOTALPAIDAMT>
        <PAYMENTDATE><?= date('Y-m-d',strtotime($order_transaction['created_at'])) ?></PAYMENTDATE>
        <PAYMENTTIME><?= date('H:m:s',strtotime($order_transaction['created_at'])) ?></PAYMENTTIME>
        @if(isset($order_transaction['receipt']['x_reference']))<TXNID>{{$order_transaction['receipt']['x_reference']}}</TXNID>@endif
        @if(isset($order_transaction['receipt']['txnid']))<TXNID>{{$order_transaction['receipt']['txnid']}}</TXNID>@endif
        {{-- @if(isset($order_transaction['receipt']['payment_id']))<TXNID>{{$order_transaction['receipt']['payment_id']}}</TXNID>@endif --}}
        @if(isset($mihpayid))<TXNID>{{$mihpayid}}</TXNID>@endif
        {{-- <PAYMENTINFO>@if(isset($order_transaction['id'])){{$order_transaction['id']}}@endif</PAYMENTINFO> --}}
       {{-- <TXNID>12345678912345</TXNID> --}}
    </PAYMENTINFO>
    <?php
        $line_items_gst = collect($order_data['line_items']);
    ?>
    @foreach($order_items as $key => $line_items)

        <?php
            if($line_items['get_order_item']['total_discounts'] > 0){
                $discount = $line_items['get_order_item']['total_discounts']/$line_items['get_order_item']['quantity'];
                
            }else{
                $discount = 0.00;
               
            }
            //$discount=round($discount);
        ?>
    <ORDERLINE>
        
        <ITEMID>{{$line_items['get_order_item']['shopify_item_id']}}</ITEMID>
        <REFID>{{$line_items['sap_id']}}</REFID>
        <STATUSCODE>01</STATUSCODE>
        <MATERIALNO>{{$line_items['sku']}}</MATERIALNO>
        <PRICE>{{$line_items['get_order_item']['unit_price']}}</PRICE>
        <DISCOUNT>{{$discount}}</DISCOUNT>
         <PAIDAMT>{{number_format($line_items['get_order_item']['unit_price'] - $discount, 2, '.', '')}}</PAIDAMT>
        <PLANT>{{$line_items['store_code']}}</PLANT>
        <COUPONCODE>{{$line_items['get_order_item']['discount_code']}}</COUPONCODE>
       
    </ORDERLINE>
    @endforeach
</ORDER>