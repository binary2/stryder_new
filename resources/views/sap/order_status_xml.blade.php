<?php echo '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>'; ?>

<STATUSUPDATE>
    <ORDERNO>{{$order_name}}</ORDERNO>
    @foreach($order_status_array as $key => $line_items)
        <ITEMSTATUS>
            <ITEMID>{{$line_items['shopify_item_id']}}</ITEMID>
            <REFID>{{$line_items['transaction_id']}}</REFID>
            <PLANT>{{$line_items['store_code']}}</PLANT>
            <STATUSCODE>{{$line_items['status']}}</STATUSCODE>
            <REMARKS></REMARKS>
            <INVOICENO>{{$line_items['invoice_number']}}</INVOICENO>
            <AWBNO>{{$line_items['awb_number']}}</AWBNO>
            @if($line_items['status'] == "08")
                <RETURNAWBNO>{{$line_items['awb_number']}}</RETURNAWBNO>
            @else
                <RETURNAWBNO></RETURNAWBNO>
            @endif
        </ITEMSTATUS>
     @endforeach
</STATUSUPDATE>