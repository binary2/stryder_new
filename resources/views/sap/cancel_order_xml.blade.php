<?php echo '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>'; ?>
<STATUSUPDATE>
<ORDERNO>{{ $order_number }}</ORDERNO>
@foreach($order_items as $key => $line_items)
<ITEMSTATUS>
<ITEMID>{{ $line_items['item_id'] }}</ITEMID>
<REFID>{{ $line_items['reference_id'] }}</REFID>
<PLANT>{{ $line_items['store_code'] }}</PLANT>
<STATUSCODE>06</STATUSCODE>
<REMARKS></REMARKS>
<INVOICENO>NULL</INVOICENO>
<AWBNO></AWBNO>
<RETURNAWBNO></RETURNAWBNO>
</ITEMSTATUS>
@endforeach
</STATUSUPDATE>
