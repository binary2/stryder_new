<?php echo '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>'; ?>
<STATUSUPDATE>
<ORDERNO>{{$order_no['order_number']}}</ORDERNO>
@foreach($order_items as $key => $value)
<ITEMSTATUS>
<ITEMID>{{$value['shopify_item_id']}}</ITEMID>
<REFID>{{$value['sap_id']}}</REFID>
<PLANT>{{$value['store_code']}}</PLANT>
<STATUSCODE>09</STATUSCODE>
<REMARKS>{{$value['return_remark']}}</REMARKS>
<INVOICENO>{{$value['sap_pack_id']}}</INVOICENO>
<AWBNO>{{$value['awb_number']}}</AWBNO>
<RETURNAWBNO>{{$value['return_awb_number']}}</RETURNAWBNO>
</ITEMSTATUS>
@endforeach
</STATUSUPDATE>