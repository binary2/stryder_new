<?php echo '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>'; ?>
<STATUSUPDATE>
<ORDERNO>{{ $order_no }}</ORDERNO>
@foreach($item_data as  $value)
<ITEMSTATUS>
<ITEMID>{{$value['ITEMID']}}</ITEMID>     
<REFID>{{$value['REFID']}}</REFID>      
<STATUSCODE>06</STATUSCODE>
<REMARKS>{{$value['REMARKS']}}</REMARKS>           
<INVOICENO></INVOICENO> 
<AWBNO>{{$value['AWBNO']}}</AWBNO>     
<RETURNAWBNO>{{$value['RETURNAWBNO']}}</RETURNAWBNO>   
</ITEMSTATUS>
@endforeach
</STATUSUPDATE>