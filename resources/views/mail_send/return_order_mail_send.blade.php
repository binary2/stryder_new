<style type="text/css">
	.d_flex{display: flex;}.txt_conten,.txt_title,b{font-size: calc(12px + 3*(100vw - 800px)/800);}b{display: block;}
</style>
<span class="d_flex">
	<span  class="txt_title">Order Number : </span>
	<span class="txt_content">{{$body['order_number']}}</span>
</span>
<span class="d_flex">
	<span class="txt_title">Customer Name : </span>
	<span class="txt_content">{{$body['customer_name']}}</span>
</span>
<span class="d_flex">
	<span class="txt_title">Customer Address : </span>
	<span class="txt_content">{{$body['shipping_address']}}</span>
</span>
<span class="d_flex">
	<span class="txt_title">Customer Email : </span>
	<span class="txt_content">{{$body['email']}}</span>
</span>
<span class="d_flex">
	<span class="txt_title">Customer Phone no : </span>
	<span class="txt_content">{{$body['phone']}}</span>
</span>
<span class="d_flex">
	<span class="txt_title">Product Name : </span>
	<span class="txt_content">{{$body['title']}}</span>
</span>
<span class="d_flex">
	<span class="txt_title">Sku : </span>
	<span class="txt_content">{{$body['sku']}}</span>
</span>
<span class="d_flex">
	<span class="txt_title">Quantity : </span>
	<span class="txt_content">{{$body['quantity']}}</span>
</span>
<span class="d_flex">
	<span class="txt_title">Price : </span>
	<span class="txt_content">{{$body['price'] * $body['quantity']}}</span>
</span>
<span class="d_flex">
	<span class="txt_title">Paid Amount : </span>
	<span class="txt_content">{{round($body['price'] * $body['quantity'] - $body['total_discounts'])}}</span>
</span>
<span class="d_flex">
	<span class="txt_title">Customer Remarks : </span>
	<span class="txt_content">{{$body['remarks']}}</span>
</span>
<b>Thanks</b>

