<svg xmlns="http://www.w3.org/2000/svg" width="20" height="16.44" viewBox="0 0 20 16.44">
  <g id="Icons_Renewed_2-14" data-name="Icons Renewed_2-14" transform="translate(0 -2.66)">
    <path id="surface1" d="M0,6.1v13H10.83L20,6.1ZM3.5,9.6a3.43,3.43,0,0,1,1.87.34s-.25.62-.37,1c-.42-.17-2.08-.46-2.08.16,0,.25.08.42,1.33.71.5.13,1.25.38,1.42,1.13L7,9.6H8.41l1.59,4v-4h1.88a1.88,1.88,0,0,1,0,3.75h-.63V14.6H9l-.25-.75a2.48,2.48,0,0,1-1,.21,2.41,2.41,0,0,1-1-.21l-.25.75H5l.21-.49a2.81,2.81,0,0,1-1.71.49,3.09,3.09,0,0,1-1.83-.37L2,13.15c.79.5,2.21.41,2.21,0s-.63-.46-1.46-.71A1.46,1.46,0,0,1,1.67,11C1.67,10.48,2.08,9.6,3.5,9.6Zm7.75,1.25V12.1h.63a.64.64,0,0,0,.62-.62.65.65,0,0,0-.62-.63ZM7.71,11l-.63,1.79a1,1,0,0,0,.63.17,1,1,0,0,0,.62-.17Z" fill="#a8a8a8"/>
    <rect id="Rectangle_4" data-name="Rectangle 4" width="19.96" height="2.91" transform="translate(0.02 2.66)" class="svgover" />
  </g>
</svg>