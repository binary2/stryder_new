<svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 20 20">
  <g id="Icons_Renewed_2-24" data-name="Icons Renewed_2-24" transform="translate(0)">
    <ellipse id="Ellipse_7" data-name="Ellipse 7" cx="3.79" cy="3.85" rx="3.79" ry="3.85" transform="translate(6.21 0)" class="svgover"/>
    <path id="Path_140" data-name="Path 140" d="M10,8a4.67,4.67,0,0,0-4.67,4.67V18.6A1.4,1.4,0,0,0,6.72,20h6.56a1.4,1.4,0,0,0,1.39-1.4V12.69A4.67,4.67,0,0,0,10,8Z" transform="translate(0 -0.04)" />
    <ellipse id="Ellipse_8" data-name="Ellipse 8" cx="1.89" cy="1.92" rx="1.89" ry="1.92" transform="translate(0.45 9.975)" class="svgover" />
    <path id="Path_141" data-name="Path 141" d="M2.34,14A2.33,2.33,0,0,0,0,16.34v3a.7.7,0,0,0,.7.7H4a.69.69,0,0,0,.69-.7v-3A2.33,2.33,0,0,0,2.34,14Z" transform="translate(0 -0.04)" />
    <ellipse id="Ellipse_9" data-name="Ellipse 9" cx="1.89" cy="1.92" rx="1.89" ry="1.92" transform="translate(15.77 9.975)" class="svgover" />
    <path id="Path_142" data-name="Path 142" d="M17.66,14a2.33,2.33,0,0,0-2.33,2.33v3A.69.69,0,0,0,16,20h3.3a.7.7,0,0,0,.7-.7v-3A2.33,2.33,0,0,0,17.66,14Z" transform="translate(0 -0.04)" />
  </g>
</svg>