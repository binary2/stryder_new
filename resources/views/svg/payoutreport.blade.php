<svg xmlns="http://www.w3.org/2000/svg" width="20.001" height="20" viewBox="0 0 20.001 20">
  <g id="Group_37" data-name="Group 37" transform="translate(-1826 -853)">
    <g id="Group_35" data-name="Group 35" transform="translate(1826 854)">
      <g id="Icons_Renewed_2-06" data-name="Icons Renewed_2-06" transform="translate(0 0)">
        <rect id="Rectangle_3" data-name="Rectangle 3" width="1.664" height="10.439" transform="translate(2.691 1.504)" class="svgover"/>
        <path id="Path_78" data-name="Path 78" d="M8.567,0H2.761A2.293,2.293,0,0,0,.45,2.293v8.862a2.293,2.293,0,0,0,2.311,2.293H9.491V12.088H2.761a.906.906,0,0,1-.906-.906V2.293a.906.906,0,0,1,.906-.906H8.585a.906.906,0,0,1,.906.906V13.465a2.285,2.285,0,0,0,1.358-2.083V2.293A2.284,2.284,0,0,0,8.567,0Z" transform="translate(-0.45 0)" fill="#a8a8a8"/>
      </g>
    </g>
    <path id="Subtraction_11" data-name="Subtraction 11" d="M21,20H5V15.066h6a1.509,1.509,0,0,0,.284-.027h7.13V13.812H12.479a1.514,1.514,0,0,0,.02-.247v-.981h5.915V11.358H12.5V2A1.5,1.5,0,0,0,11,.5H5V0h9.55V6.187H21V20ZM7.587,16.277V17.5H18.414V16.277ZM20.624,5.087H15.956V.366l4.667,4.719Z" transform="translate(1825 853)" fill="#a8a8a8"/>
  </g>
</svg>