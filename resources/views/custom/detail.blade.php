<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    @if(!empty($state) && !empty($city) && !empty($slug))
    <title>Stryder Bikes Store in {{ $slug }} - Stryder Bikes</title>
    <meta name="description" content="Use our store finder to locate Stryder Bikes stores in {{ $slug }}. Know more about the Stryder Bikes {{ $slug }} store timings, address, directions & contact number.">
    @else
    <title>Store Locator</title>
    @endif
    <link rel="stylesheet" href="https://mdlw.tatainternational.com/storelocator.css">
    <!-- <link rel="stylesheet" href="https://admin3157.gocolors.com/new_go_colors_india/public/storelocator.css"> -->
</head>
<body>
    <div class="container" id="storelocatorpage">
        <!-- <img src="https://admin3157.gocolors.com/new_go_colors_india/public/Store_Locator.jpg" alt="" class="storeImg desktop_image">
        <img src="https://admin3157.gocolors.com/new_go_colors_india/public/Store-Locator_mobile.jpg" alt="" class="mobile_image storeImg"> -->
        <div class="storecontentgallery">
            <div class="storecontent detailviews">
                <h1>Stryder Bikes - {{ $StoreMaster_data['store_name'] }}, {{ $StoreMaster_data['store_city'] }}</h1>
                @if(!empty($StoreMaster_data['store_address']))
                <span class="storedata">
                    <b>ADDRESS:</b>
                    <i>{{ $StoreMaster_data['store_address'] }}</i>
                </span>
                @endif
                @if(!empty($StoreMaster_data['store_phone_number']))
                <span class="storedata">
                    <b>PHONE:</b>
                    <i><a href="tel:{{ $StoreMaster_data['store_phone_number'] }}">{{ $StoreMaster_data['store_phone_number'] }}</a></i>
                </span>
                @endif
                @if(!empty($StoreMaster_data['store_time']))
                <span class="storedata">
                    <b>Store Time :</b>
                    <i>{{ $StoreMaster_data['store_time'] }}</i>
                </span>
                @endif
                @if(!empty($StoreMaster_data['whatsapp_number']))
                <span class="storedata">
                    <b>Whatsapp Number:</b>
                    <i><a href="tel:{{ $StoreMaster_data['whatsapp_number'] }}">{{ $StoreMaster_data['whatsapp_number'] }}</a></i>
                </span>

                @endif
                <span class="storedata">
                    <a href="javascript:history.back()" class="moredetails"><b>back</b></a>
                </span>
            </div>
            @if(!empty($StoreMaster_data['google_map']))
            <div class="halfcontainer google_map">
                    <iframe id="map" src="{{ $StoreMaster_data['google_map']  }}" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
                </div>
            @endif
        </div>
    </div>
    <br><br>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
</body>
</html>