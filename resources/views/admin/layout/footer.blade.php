    {{ Html::script('backend/js/jquery-2.1.4.min.js', [],IS_SECURE) }}

    <!-- Bootstrap Core JavaScript -->
    {{ Html::script('backend/js/popper.min.js', [],IS_SECURE) }}
    {{ Html::script('backend/js/bootstrap.min.js', [],IS_SECURE) }}
    
    <!-- Slimscroll JavaScript -->
    {{ Html::script('backend/js/jquery.slimscroll.js', [],IS_SECURE) }}

    <!-- Fancy Dropdown JS -->
    {{ Html::script('backend/js/dropdown-bootstrap-extended.js', [],IS_SECURE) }}

    <!-- FeatherIcons JavaScript -->
    {{ Html::script('backend/js/feather.min.js', [],IS_SECURE) }}

    <!-- Toggles JavaScript -->
    {{ Html::script('backend/js/toggles.min.js', [],IS_SECURE) }}
    {{ Html::script('backend/js/toggle-data.js', [],IS_SECURE) }}
    
    <!-- Init JavaScript -->
    {{ Html::script('backend/js/init.js', [],IS_SECURE) }}
    {{ Html::script('backend/js/dashboard5-data.js', [],IS_SECURE) }}
    
    <!-- Daterangepicker JavaScript -->
    {{ Html::script('backend/js/daterangepicker.js',[],IS_SECURE) }}
    {{ Html::script('backend/js/jquery-datepicker.min.js', [],IS_SECURE) }}
    {{-- {{ Html::script('backend/js/moment.min.js', [],IS_SECURE) }} --}}

    {{ Html::script('backend/js/toster.min.js',[],IS_SECURE) }}
    {{ Html::script('backend/js/timeout.js',[],IS_SECURE) }}
    {{ Html::script('backend/js/jquery.inputmask.bundle.js',[],IS_SECURE) }}
    {{ Html::script('backend/js/jquery.form.min.js', [],IS_SECURE)}}
    {{ Html::script('backend/js/jquery.dataTables.min.js',[],IS_SECURE) }}
    {{ Html::script('backend/js/dataTables.bootstrap4.min.js',[],IS_SECURE) }}
    {{ Html::script('backend/js/dataTables.dataTables.min.js',[],IS_SECURE) }}
    {{ Html::script('backend/js/fnstandrow.js', [],IS_SECURE)}}
    {{ Html::script('backend/js/nprogress.js',[],IS_SECURE) }}
    {{ Html::script('backend/js/jquery.multi-select.js', [],IS_SECURE) }}
    {{ Html::script('backend/js/select2.min.js', [],IS_SECURE) }}
    {{ Html::script('backend/js/sweetalert.js', [],IS_SECURE) }}
    {{ Html::script('backend/js/sweetalert.min.js', [],IS_SECURE) }}


@yield('script')
