<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<nav class="hk-nav hk-nav-light">
    <a href="javascript:void(0);"  class="hk-nav-close"><span class="feather-icon"><i data-feather="x"></i></span></a>
    <div class="nicescroll-bar">
        <div class="navbar-nav-wrap">
            <ul class="navbar-nav flex-column">
                <li class="nav-item">
                    
                    <a class="nav-link" href="{{route('dealership.enquiry.index') }}" >
                        {{-- <i>@include('svg.sonycenter')</i> --}}
                        <i class='fas fa-tags'></i>
                        <span class="nav-link-text">Dealership Enquiry</span>
                    </a>
                </li>
                <li class="nav-item">
                    
                    <a class="nav-link" href="{{route('general.enquiry.index') }}" >
                        {{-- <i>@include('svg.sonycenter')</i> --}}
                         <i class='fas fa-tags'></i>
                        <span class="nav-link-text">General Enquiry</span>
                    </a>
                </li>
                <li class="nav-item">
                    
                    <a class="nav-link" href="{{route('register.bike.index') }}" >
                        {{-- <i>@include('svg.sonycenter')</i> --}}
                        <i class='fa fa-pencil-square-o'></i>
                        <span class="nav-link-text">Register Your Bike</span>
                    </a>
                </li><li class="nav-item">
                    
                    <a class="nav-link" href="{{route('return_order.index') }}" >
                        <i>@include('svg.sonycenter')</i>
                        {{-- <i class='fa fa-pencil-square-o'></i> --}}
                        <span class="nav-link-text">Return Orders</span>
                    </a>
                </li>
                </li><li class="nav-item">
                    
                    <a class="nav-link" href="{{route('storemasters.index') }}" >
                        <i>@include('svg.sonycenter')</i>
                        {{-- <i class='fa fa-pencil-square-o'></i> --}}
                        <span class="nav-link-text">Store Master</span>
                    </a>
                </li>

                {{-- <li class="nav-item ">
                    <a class="nav-link" href="javascript:void(0);" data-toggle="collapse" data-target="#username">
                        <i class='fa fa-user-o'></i>
                        <span class="nav-link-text">{{Auth::guard('admin')->user()->name}}</span>
                    </a>
                    <ul id="username" class="nav flex-column collapse collapse-level-1">
                        <li class="nav-item">
                            <ul class="nav flex-column">
                                <li class="nav-item">
                                    <ul class="nav flex-column">
                                        <li class="nav-item">
                                            <a href="{{route('admin.logout')}}" class="nav-link" href="alerts.html">
                                                <i class="fas fa-sign-out-alt"></i>
                
                                                <span class="nav-link-text">Logout</span>
                                            </a>
                                        </li>
                                    </ul>
                                </li>    
                            </ul>
                        </li>
                    </ul>
                </li> --}}

                <li class="nav-item ">
                    <a href="{{route('admin.logout')}}" class="nav-link" href="alerts.html">
                        <i class="fas fa-sign-out-alt"></i>
                        <span class="nav-link-text">Logout</span><span class="nav-link-text ml-5 pl-5">{{Auth::guard('admin')->user()->name}}</span>
                    </a>
                </li>

            </ul>   
        </div>
    </div>
</nav>
<div id="hk_nav_backdrop" class="hk-nav-backdrop"></div>