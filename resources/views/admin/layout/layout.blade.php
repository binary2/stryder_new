<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <title>Stryder</title>
        <meta name="description" content="Shopify sync" />
        <!--  Css files  -->
        @include('admin.layout.header_main')
        @yield('style')
    </head>
    <body id="body_close_class">
        @include('admin.layout.loader')
        <div class="hk-wrapper hk-vertical-nav hk-icon-nav" >
            @yield('start_form')
            <!-- Nav-Bar-->

            @yield('header')
            @include('admin.layout.navbar')
            <!-- Content Start here -->
            <div class="hk-pg-wrapper">
                @yield('content')
            </div>
            <!-- Content End here -->
            <!-- footer contant -->
            @yield('end_form')
        </div>
        @include('admin.layout.footer')
    </body>
</html>