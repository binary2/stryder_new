<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <title>Stryder Login</title>
    <meta name="description" content="A responsive bootstrap 4 admin dashboard template by hencework" />

    <!-- Favicon -->
    <link rel="shortcut icon" href="">
    <link rel="icon" href="" type="image/x-icon">

    <!-- Custom CSS -->
    {{ Html::style('backend/css/toster.min.css',[],IS_SECURE) }}
    {{ Html::style('backend/css/style.css',[],IS_SECURE) }}
    {{ Html::style('backend/css/binary.css',[],IS_SECURE) }}
</head>

<body>
    <!-- Preloader -->
    @include('admin.layout.loader')
    <!-- /Preloader -->
    <div class="hk-wrapper">
        <!-- Main Content -->
        <div class="hk-pg-wrapper hk-auth-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-xl-12 pa-0">
                        <div class="auth-form-wrap pt-xl-0 pt-70">
                            <div class="auth-form w-xl-30 w-lg-55 w-sm-75 w-100">
                                <a class="auth-brand text-center d-block mb-20" href="#">
                                    <!-- <img class="brand-img" src="/backend/img/orbymart_logo.png" alt="brand" width="200px" /> -->
                                </a>
                                <form action="<?= route('admin.login.post') ?>" id="login_form" autocomplete="off"  class="login-form" method="post">
                                {{ csrf_field() }}
                                <div class="form-group">
                                    <input class="form-control" autocomplete="off"  placeholder="Email" type="text" name="email"
                                        value="" id="email">
                                    <span class="text-danger">{{ $errors->first('email') }}</span>
                                </div>
                                <div class="form-group">
                                    <div class="input-group">
                                        <input class="form-control" autocomplete="off"  placeholder="Password" type="password"
                                            name="password" value="" id="password">
                                    </div>
                                    <span class="text-danger">{{ $errors->first('password') }}</span>
                                </div>
                                <button class="btn btn-primary btn-block" type="submit" id="login">Login</button>
                                </form>
                                <!-- <a href="" class="forgotpassword">Forgot
                                    password?</a> -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /Main Content -->
    </div>  
    <!-- jQuery -->
    {{ Html::script('backend/js/jquery.min.js',[],IS_SECURE) }}
    {{ Html::script('backend/js/popper.min.js',[],IS_SECURE) }}
    {{ Html::script('backend/js/bootstrap.min.js',[],IS_SECURE) }}
    {{ Html::script('backend/js/jquery.slimscroll.js',[],IS_SECURE) }}
    {{ Html::script('backend/js/dropdown-bootstrap-extended.js',[],IS_SECURE) }}
    {{ Html::script('backend/js/owl.carousel.min.js',[],IS_SECURE) }}
    {{ Html::script('backend/js/feather.min.js',[],IS_SECURE) }}
    {{ Html::script('backend/js/init.js',[],IS_SECURE) }}
    {{ Html::script('backend/js/login-data.js',[],IS_SECURE) }}
    {{ Html::script('backend/js/toster.min.js',[],IS_SECURE) }}
</body>

</html>
<script>
  @if(Session::has('message'))
  toastr.options =
  {
    "closeButton" : true,
    "progressBar" : true
  }
        toastr.success("{{ session('message') }}");
  @endif

  @if(Session::has('error'))
  toastr.options =
  {
    "closeButton" : true,
    "progressBar" : true
  }
        toastr.error("{{ session('error') }}");
  @endif

</script>

@include('admin.layout.alert')
