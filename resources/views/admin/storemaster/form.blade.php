<div class="container">
    <div class="row">
        <div class="col-xl-12">
            <section class="hk-sec-wrapper">
                <div class="row">
                    <div class="col-sm">
                        <div class="row">
                            <div class="col-md-6 form-group">
                                <label for="customerTag">Store Name <sup class="text-danger">*</sup></label>
                                <?= Form::text('store_name',null ,['class' => 'form-control store_name error_remove','id'=>'store_name','placeholder'=>'Enter Store Name']); ?>
                                <span id="store_name_error" class="help-inline text-danger error_msg"><?= $errors->first('store_name') ?></span>
                            </div>
                            <div class="col-md-6 form-group">
                                <label for="customerGroup">Store Code <sup class="text-danger">*</sup></label>
                                <?= Form::text('store_code',null ,['class' => 'form-control store_code error_remove','id'=>'store_code','placeholder'=>'Enter Store Code','maxlength'=>6]); ?>
                                <span id="store_code_error" class="help-inline text-danger error_msg"><?= $errors->first('store_code') ?></span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 form-group">
                                <label for="customerTag">Store Country <sup class="text-danger">*</sup></label>
                                <?= Form::select('store_country',[null => 'Select Country'] + ['India' => 'India','Nepal'=>'Nepal'],null ,['class' => 'form-control store_city error_remove','id'=>'store_country']); ?>
                                <span id="store_city_error" class="help-inline text-danger error_msg"><?= $errors->first('store_country') ?></span>
                            </div>
                            <div class="col-md-6 form-group">
                                <label for="customerTag">Store City <sup class="text-danger">*</sup></label>
                                <?= Form::text('store_city',null ,['class' => 'form-control store_city error_remove','id'=>'store_city','placeholder'=>'Enter Store City']); ?>
                                <span id="store_city_error" class="help-inline text-danger error_msg"><?= $errors->first('store_city') ?></span>
                            </div>
                            <div class="col-md-6 form-group">
                                <label for="customerTag">Store State <sup class="text-danger">*</sup></label>
                                <?= Form::text('store_state',null ,['class' => 'form-control store_state error_remove','id'=>'store_state','placeholder'=>'Enter Store State']); ?>
                                <span id="store_state_error" class="help-inline text-danger error_msg"><?= $errors->first('store_state') ?></span>
                            </div>
                            <div class="col-md-6 form-group">
                                <div class="col-md-15 form-group">
                                    <label for="customerTag">Store Pin Code <sup class="text-danger">*</sup></label>
                                    <?= Form::text('store_pincode',null ,['class' => 'form-control store_pincode error_remove allow_number','id'=>'store_pincode','placeholder'=>'Enter Store Pincode','maxlength'=>6]); ?>
                                    <span id="store_pincode_error" class="help-inline text-danger error_msg"><?= $errors->first('store_pin_code') ?></span>
                                </div>
                            </div>
                            <div class="col-md-6 form-group">
                                <label for="customerTag">Store Address<sup class="text-danger">*</sup></label>
                                <?= Form::textarea('store_address',null ,['class' => 'form-control store_address error_remove','id'=>'store_address','placeholder'=>'Enter Store Address','cols'=>'10','rows'=>'5']); ?>
                                <span id="store_address_error" class="help-inline text-danger error_msg"><?= $errors->first('store_address') ?></span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 form-group">
                                <div class="col-md-15 form-group">
                                    <label for="customerTag">Enter Slug<sup class="text-danger">*</sup></label>
                                    <?= Form::text('store_slug',null ,['class' => 'form-control','id'=>'store_pincode','placeholder'=>'Enter Slug']); ?>
                                    <span id="store_slug_error" class="help-inline text-danger error_msg"><?= $errors->first('store_slug') ?></span>
                                </div>
                                <div class="col-md-15 form-group">
                                    <label for="customerTag">Contact Number <sup class="text-danger">*</sup></label>
                                    <?= Form::text('store_phone_number',null ,['class' => 'form-control store_phone_number error_remove allow_number','id'=>'phone','placeholder'=>'Enter Store Mobile Number']); ?>
                                    <span id="store_phone_number_error" class="help-inline text-danger error_msg"><?= $errors->first('store_phone_number') ?></span>
                                </div>
                            
                            </div>
                            <div class="col-md-6 form-group">
                                <label for="customerTag">Google Emmbeded Code<sup class="text-danger">*</sup></label>
                                <?= Form::textarea('google_map',null ,['class' => 'form-control store_address error_remove','id'=>'store_address','placeholder'=>'Enter Google Emmbeded Code','cols'=>'10','rows'=>'8']); ?>
                                <span id="google_map_error" class="help-inline text-danger error_msg"><?= $errors->first('google_map') ?></span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 form-group">
                                <label for="customerTag">Email ID <sup class="text-danger">*</sup></label>
                                <?= Form::text('store_email',null ,['class' => 'form-control store_email error_remove','id'=>'store_email','placeholder'=>'Enter Email ID']); ?>
                                <span id="store_email_error" class="help-inline text-danger error_msg"><?= $errors->first('store_email') ?></span>
                            </div>
                        </div>    
                        <div class="row">
                            
                            <div class="col-md-6 form-group">
                                <label for="customerTag">Google Link</label>
                                <?= Form::textarea('google_link',null ,['class' => 'form-control google_link error_remove','id'=>'google_link','placeholder'=>'Enter Google Link','cols'=>'10','rows'=>'5']); ?>
                                <span id="google_link_error" class="help-inline text-danger error_msg"><?= $errors->first('google_link') ?></span>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </section>
    </div>
</div>