@extends('admin.layout.layout')

@section('style')

<?= Html::style('backend/css/daterangepicker.css') ?>

<style type="text/css">

	#center{

		text-align: center;

	}

</style>

@endsection

@section('header')

<nav class="navbar navbar-expand-xl navbar-light fixed-top hk-navbar">

	<a id="navbar_toggle_btn" class="navbar-toggle-btn nav-link-hover" href="javascript:void(0);"><span class="feather-icon"><i data-feather="menu"></i></span></a>

	<div class="hk-pg-header col-md-6">

		<h4 class="hk-pg-title"><span class="pg-title-icon"><span class="feather-icon"></span></span>StoreMaster</h4>

	</div>

	<div class="text-align-right col-md-6">

		<a href="<?= route('storemasters.index') ?>" role="button" class="btn btn-light btn-sm mr-2 buttonOverAnimation cancel" title="Cancel">Cancel</a>

		<button class="btn btn-primary btn-sm mr-2 buttonOverAnimation save" value="Save & New" id="save" title="Save">Save</button>

	</div>

</nav>



@stop

@section('content')

<!-- {!! Form::model($storemaster_data,['id'=>'master_form']) !!} -->

<?= Form::model($storemaster_data,['id'=>'master_form'])?>

@include('admin.storemaster.form')

@stop

@section('end_form')

<?= Form::close(); ?>

	<div class="text-align-right col-md-12">

        <a href="<?= route('storemasters.index') ?>" role="button" class="btn btn-light btn-sm mr-2 buttonOverAnimation cancel" title="Cancel">Cancel</a>

        <button type="button" class="btn btn-primary btn-sm mr-2 buttonOverAnimation save" title="Save">Save</button>

	</div>

@stop



@section('script')

<?= Html::script('backend/js/jquery.form.min.js', [], IS_SECURE)?>

<?= Html::script('backend/js/moment.min.js')?>

<?= Html::script('backend/js/daterangepicker.js')?>

<?= Html::script('backend/js/daterangepicker-data.js')?>

<?= Html::script('backend/js/jquery.dataTables.min.js')?>

<script type="text/javascript">

	$(".allow_number").keypress(function(h){if(8!=h.which&&0!=h.which&& 46!=h.which&&(h.which<48||h.which>57))return!1});

</script>

<script type="text/javascript">



	$(document).ready(function() {



		$(".save").click(function(e){



			e.preventDefault();

			var url="<?= URL::route('storemasters.update',$storemaster_data->store_master_id) ?>";

			var method_type='post';

			var token="<?= csrf_token()?>";



			$("#master_form").ajaxSubmit({



				url:url,

				type:method_type,

				data:{ "_token " : token },



				success:function(result){



					if(result.success==true)

					{

						toastr.success('StoreMaster Updated  Successfully');

						setTimeout(function(){

							window.location.href="<?=route('storemasters.index')?>";

							}, 1000);

					}	

				},

				error:function(resobj){

					toastr.error('Something are Wrong');



					$.each(resobj.responseJSON.errors, function(k,v){

						$('#'+k+'_error').text(v);



					});    

				}





			});

		})

	});

    // allow only number

$(".allow_time").keypress(function(h){

    var keyCode =h.which ? h.which : h.keyCode

       if (!(keyCode >= 48 && keyCode <= 58) && !(keyCode >= 65 && keyCode <= 65)   && !(keyCode >= 97 && keyCode <= 97) && !(keyCode >= 109 && keyCode <= 109) && !(keyCode >= 77 && keyCode <= 77)  && !(keyCode >= 32 && keyCode <= 32) && !(keyCode >=  112 && keyCode <= 112) && !(keyCode >= 80 && keyCode <= 80)) {

             return !1;

           }

 });

</script>  

@endsection