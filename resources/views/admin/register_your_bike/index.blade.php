@section('style')
    div.dataTables_wrapper {
        width: 800px;
        margin: 0 auto;
    }
@stop
@extends('admin.layout.layout')
@section('header')
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js"></script>
    <nav class="navbar navbar-expand-xl navbar-light fixed-top hk-navbar">
        <a id="navbar_toggle_btn" class="navbar-toggle-btn nav-link-hover" href="javascript:void(0);"><span class="feather-icon"><i data-feather="menu"></i></span></a>
        <div class="hk-pg-header col-md-6">
            <h4 class="hk-pg-title"><span class="pg-title-icon"><span class="feather-icon"></span></span>{{ __('Register Your Bike') }}</h4>
        </div>
        <div class="text-align-right col-md-6 pr-40">
    
        </div>
    </nav>
@stop
@section('content')
<div class="container">
    <div class="row">
        <div class="col-xl-12"> 
            <section class="hk-sec-wrapper"> 
                <form id="filter" action="<?= route('register.bike.export') ?>" method="GET">  
                    {{ csrf_field()}}
                    <div class="row justify-content-end">
                       
                        <div class="col-md-3">
                            <input id="date_range" class="form-control input-sm" type="text" placeholder="Selete date range" name="yoy[]"></i>
                        </div>
                        
                        <button class="btn btn-primary btn-sm mr-2" id="export_data" title="{{ __('names.export') }}"><i class="fas fa-file-export"></i>{{ __('names.export') }} </button>
                    </div>
                </form>
            </section>    
        </div>
    </div>
</div>
    <div class="container">
        <div class="row">
            <div class="col-xl-12">
               
            <section class="hk-sec-wrapper">
                <div class="row">
                        <div class="col-sm">
                            <div class="table-wrap">
                                <table id="master_table" class="table table-hover w-100 display pb-30">
                                    <thead>
                                        <tr>
                                            <th></th>
                                            <th>Bike Type</th>
                                            <th>Date of Purchase</th>
                                            <th>Date of Registration</th>
                                            <th>Motor no.</th>
                                            <th>Frame no.</th>
                                            <th>Retail Name</th>
                                            <th>Retail City</th>
                                            <th>Product Name</th>
                                            <th>Product Price</th>
                                            <th>Buyers Name</th>
                                            <th>Email</th>
                                            <th>Gender</th>
                                            <th>Age</th>
                                            <th>Contact No.</th>
                                            <th>State</th>
                                            <th>City</th>
                                            <th>Address</th>
                                            <th>Pincode</th>           
                                            <th>Comment</th> 
                                            <th>Invoice</th>               
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
@stop
@section('script')
    <!-- AJAX FOR DATA TABLE  START-->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js"></script>
    <script type="text/javascript">
        var table = "master_table";
        
        var token = "{{ csrf_token() }}";
    //  $(document).ready(function(){  
    //     $('#master_table').dataTable({
           
    //     });
    // });
        var url="<?= URL::route('register.bike.index')?>";

        $(function() 
        {      
            var master = $('#master_table').dataTable({
                 scrollX:true,
                "bProcessing": true,
                "bServerSide": true,
                "autoWidth": true,
                "aaSorting": [
                    [1, "desc"]
                ],
                lengthMenu: [
                    [ 50, 100,200,500],
                    [ '50','100','200','500']
                ],
                "fnServerParams": function(aoData) {
                //send other data to server side
                    var form_data = $('#filter').serializeArray();
                    $.each(form_data, function(i, val) {
                        aoData.push(val);
                    });
                    aoData.push({
                        "name": "act",
                        "value": "fetch"
                    });
                    server_params = aoData;
                },
                "sAjaxSource": url,
                "aoColumns": [
                    {
                        "mData": "created_at",
                        bSortable: false,
                        bVisible: false,
                       
                    },

                    { "mData": "bike_type",sWidth: "10%",bSortable: true,bVisible:true},
                    {
                        "mData": "date_of_purchase", "className": "tddate", sWidth: "40%", bSortable: true, bVisible: true,
                        mRender: function (v, t, o) {
                            var expected_date = '-';
                            var date = o['date_of_purchase'];                            
                            var months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
                                var d = new Date(date.split(" "));
                            console.log(d,"date")
                                var dd = d.getDate();
                                var mm = months[d.getMonth()];
                                var yy = d.getFullYear();

                                var finalTime =  dd + " " + mm + ", " + yy;
            
            
                                var change_date = finalTime;
                           
                            return change_date;
                        }
                    },
                    {
                        "mData": "created_at", "className": "tddate", sWidth: "40%", bSortable: true, bVisible: true,
                        mRender: function (v, t, o) {
                            var expected_date = '-';
                            var date = o['created_at'];                            
                            var months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
                                var d = new Date(date.split(" "));
                           
                                var dd = d.getDate();
                                var mm = months[d.getMonth()];
                                var yy = d.getFullYear();

                                var finalTime =  dd + " " + mm + ", " + yy;
            
            
                                var change_date = finalTime;
                           
                            return change_date;
                        }
                    },
                    { "mData": "Motor_no",sWidth: "10%",bSortable: true,bVisible:true},
                    { "mData": "frame_number",sWidth: "10%",bSortable: true,bVisible:true},
                    { "mData": "retail_name",sWidth: "5%",bSortable: true},
                    { "mData": "retail_city",sWidth: "5%",bSortable: true},
                    { "mData": "product_name",sWidth: "10%",bSortable: true},
                    { "mData": "product_price",sWidth: "5%",bSortable: true},
                    { "mData": "personal_retail_name",sWidth: "7%",bSortable: true},
                    { "mData": "email",sWidth: "5%",bSortable: true},
                    { "mData": "gender",sWidth: "5%",bSortable: true},
                    { "mData": "age",sWidth: "5%",bSortable: true},
                    { "mData": "phone",sWidth: "5%",bSortable: true},
                    { "mData": "state",sWidth: "5%",bSortable: true},
                    { "mData": "city",sWidth: "5%",bSortable: true},
                    { "mData": "address",sWidth: "5%",bSortable: true},
                    { "mData": "pincode",sWidth: "5%",bSortable: true},
                    { "mData": "comment",sWidth: "15%",bSortable: true},
                    { "mData": "id",
                        bSortable: false,
                        sWidth: "2px",
                        sClass: "text-center",
                        mRender: function(v, t, o) {
                            var id=o['id'];

                            var url = "<?= URL::route('invoice-download',['id'=>':id']) ?>";
                            url = url.replace(':id',id);

                            var act_html = "<div class='btn-group'>"
                            +"<a href="+url+" title='Edit' data-placement='top' class='btn btn-xs btn-default' style='font-size:15px; line-height:9px; padding: 8px 10px'><span class='badge badge-success''>Download Invoice</span></a>"
                            +"</div>";
                            return act_html;
                        }
                    },
                ],
                fnPreDrawCallback : function() { 
                    NProgress.start();
                },
                fnDrawCallback : function (oSettings) {
                    NProgress.done();
                }
            });  
            master.fnSetFilteringDelay(1000);

            $("#date_range").daterangepicker({
                autoUpdateInput: false,
                locale: {
                    "cancelLabel": "Clear",
                },
                ranges: {
                    'Today': [moment(), moment()],
                    'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                    'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                    'Last 30 Days': [moment().subtract(30, 'days'), moment().add(4,'days')],
                    'This Month': [moment().startOf('month'), moment().endOf('month')],
                    'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                },
                opens: "right",
                startDate: moment().subtract(30, 'days'),
                endDate: moment().add(4,'days')
            });

            $("#date_range").on('apply.daterangepicker', function(ev, picker) {
                $(this).val(picker.startDate.format('YYYY-MM-DD') + ' - ' + picker.endDate.format('YYYY-MM-DD'));
                master.fnStandingRedraw();
            });
        });
        
    </script>
        <script type="text/javascript">
    function invoice_download(id){

        var url = "<?= URL::route('invoice-download',['id'=>':id']) ?>";
        url = url.replace(':id',id);
        var token = "<?=csrf_token()?>";
        $.ajax({
        url: url,
        type: 'get',
        data: {},
        dataType: 'json',   
        success : function(resp)
        {
            if(resp.success == true){
                var table= $('#orders').DataTable();
                toastr.success('Product update Successfully');
                table.ajax.reload();
            }
        },
        });  
    }
    </script>
    <!-- AJAX FOR DATA TABLE  END-->
    @include('admin.layout.alert')
@stop