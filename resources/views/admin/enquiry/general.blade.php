@extends('admin.layout.layout')

@section('header')
 <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js"></script>
    <nav class="navbar navbar-expand-xl navbar-light fixed-top hk-navbar">
        <a id="navbar_toggle_btn" class="navbar-toggle-btn nav-link-hover" href="javascript:void(0);"><span class="feather-icon"><i data-feather="menu"></i></span></a>
        <div class="hk-pg-header col-md-6">
            <h4 class="hk-pg-title"><span class="pg-title-icon"><span class="feather-icon"></span></span>{{ __('General Enquiry') }}</h4>
        </div>
        <div class="text-align-right col-md-6 pr-40">
    
        </div>
    </nav>
@stop
@section('content')
<div class="container">
    <div class="row">
        <div class="col-xl-12"> 
            <section class="hk-sec-wrapper"> 
                <form id="filter" action="<?= route('general.enquiry.export') ?>" method="GET">  
                    {{ csrf_field()}}
                    <div class="row justify-content-end">
                       
                        <div class="col-md-3">
                            <input id="date_range" class="form-control input-sm" type="text" placeholder="Selete date range" name="yoy[]"></i>
                        </div>
                        
                        <button class="btn btn-primary btn-sm mr-2" id="export_data" title="{{ __('names.export') }}"><i class="fas fa-file-export"></i>{{ __('names.export') }} </button>
                    </div>
                </form>
            </section>    
        </div>
    </div>
</div>
    <div class="container">
        <div class="row">
            <div class="col-xl-12">
               
            <section class="hk-sec-wrapper">
                <div class="row">
                        <div class="col-sm">
                            <div class="table-wrap">
                                <table id="master_table" class="table table-hover w-100 display pb-30">
                                    <thead>
                                        <tr>
                                            <th></th>
                                            <th>Enquiry Date</th>
                                            <th>Name</th>
                                            <th>Contact NO</th>
                                            <th>Email</th>
                                            <th>State Name</th>
                                            <th>Pincode</th>           
                                            <th>Comment</th>           
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
@stop
@section('script')
    <!-- AJAX FOR DATA TABLE  START-->

    <script type="text/javascript">
        var table = "master_table";
        
        var token = "{{ csrf_token() }}";
            
      
        var url="<?= URL::route('general.enquiry.index')?>";

        $(function() 
        {      
            var master = $('#master_table').dataTable({
                "bProcessing": true,
                "bServerSide": true,
                "autoWidth": true,
                "aaSorting": [
                    [1, "desc"]
                ],
                lengthMenu: [
                    [ 50, 100,200,500],
                    [ '50','100','200','500']
                ],
                "fnServerParams": function(aoData) {
                //send other data to server side
                    var form_data = $('#filter').serializeArray();
                    $.each(form_data, function(i, val) {
                        aoData.push(val);
                    });
                    aoData.push({
                        "name": "act",
                        "value": "fetch"
                    });
                    server_params = aoData;
                },
                "sAjaxSource": url,
                "aoColumns": [
                    {
                        mData: "created_at",
                        bSortable: false,
                        bVisible: false,
                        sWidth: "3%",
                        sClass: 'text-center',
        
                    },
                    {
                        "mData": "created_at", "className": "tddate", sWidth: "40%", bSortable: true, bVisible: true,
                        mRender: function (v, t, o) {
                            var expected_date = '-';
                            var date = o['created_at'];                            
                            var months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
                                var d = new Date(date.split(" "));
                           
                                var dd = d.getDate();
                                var mm = months[d.getMonth()];
                                var yy = d.getFullYear();

                                var finalTime =  dd + " " + mm + ", " + yy;
            
            
                                var change_date = finalTime;
                           
                            return change_date;
                        }
                    },

                    { "mData": "name",sWidth: "15%",bSortable: true,bVisible:true},
                    { "mData": "phone_number",sWidth: "5%",bSortable: true},
                    { "mData": "email",sWidth: "5%",bSortable: true},
                    { "mData": "state_name",sWidth: "5%",bSortable: true},
                    { "mData": "pincode",sWidth: "5%",bSortable: true},
                    { "mData": "comment",sWidth: "20%",bSortable: true}
                ],
                fnPreDrawCallback : function() { 
                    NProgress.start();
                },
                fnDrawCallback : function (oSettings) {
                    NProgress.done();
                }
            });  
            master.fnSetFilteringDelay(1000);
                        $("#date_range").daterangepicker({
                autoUpdateInput: false,
                locale: {
                    "cancelLabel": "Clear",
                },
                ranges: {
                    'Today': [moment(), moment()],
                    'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                    'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                    'Last 30 Days': [moment().subtract(30, 'days'), moment().add(4,'days')],
                    'This Month': [moment().startOf('month'), moment().endOf('month')],
                    'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                },
                opens: "right",
                startDate: moment().subtract(30, 'days'),
                endDate: moment().add(4,'days')
            });

            $("#date_range").on('apply.daterangepicker', function(ev, picker) {
                $(this).val(picker.startDate.format('YYYY-MM-DD') + ' - ' + picker.endDate.format('YYYY-MM-DD'));
                master.fnStandingRedraw();
            });
        });
        
    </script>
    <!-- AJAX FOR DATA TABLE  END-->
    @include('admin.layout.alert')
@stop