<?php



return[

    'product_name' => 'Product',

    'collection_name' => 'Collection',

    'order_name' => 'Order',

    'customer_name' => 'Customer',

    'report' => 'Sync Report',

    'back' =>'Back',

    'inventory' => 'Inventory',

    'import' => 'Import',

    'export' => 'Export',

    'cancel' => 'Cancel',

    'save_new' => 'Save & New',

    'save_exit' => 'Save & Exit',

    'save' => 'Save',

    'storemaster' => 'StoreMaster',

    'create_master' => 'Create Store',

    'delete' => 'Delete',

    'Day'=>'Day',

    'OpeningTime'=>'Opening Time',

    'ClosingTime'=>'Closing Time',

    'OpeningTimeEnt'=>'Enter Opening Time',

    'ClosingTimeEnt'=>'Enter Closing Time',
    'Order' => 'Orders',
    'role' => 'Role',
    'user' => 'User',

    'RTO'=>"Unfortunately, we've been unable to deliver the below :item_name from Order No.:order_no. The shipment will be returned to us.
    We'll keep you updated on the refund status.",

    'Picked_Up'=>"We’ve picked up your order No.:order_no for the below :item_name. Once the package reaches us, we will issue your refund within 5 days.
    We'll keep you updated on the refund status.",

    'ws_cancel'=>"We are very sorry. Unfortunately, due to some technical errors, we've had to cancel the below :item_name from your Order No.:order_no
    We'll keep you updated on the refund status.",

    'return_request'=>"We’ve received your order return request for below the :item_name from Order No.:order_no. Once the package reaches us, we will issue your refund within 5 days.
    We'll keep you updated on the refund status.",

     'customer_cancel_request'=>"We've received your request to cancel the :item_name from Order No.:order_no
The request has been processed and we'll keep you updated on the refund status.",

    'customer_refund_close'=>"We’ve issued you a refund for the below :item_name from your Order No.:order_no
     The refund will reflect in the original payment mode within 5-7 working days.",
    
];



?>