<?php $__env->startSection('style'); ?>

<?= Html::style('backend/css/daterangepicker.css') ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('start_form'); ?>

<?php echo Form::open(['class' => 'form-horizontal', 'id' => 'storemaster_form']); ?> 

<?php $__env->stopSection(); ?>

<?php $__env->startSection('header'); ?>

<nav class="navbar navbar-expand-xl navbar-light fixed-top hk-navbar">

    <a id="navbar_toggle_btn" class="navbar-toggle-btn nav-link-hover" href="javascript:void(0);"><span class="feather-icon"><i data-feather="menu"></i></span></a>

    <div class="hk-pg-header col-md-6">

        <h4 class="hk-pg-title"><span class="pg-title-icon"><span class="feather-icon"></span></span>StoreMaster</h4>

    </div>

    <div class="text-align-right col-md-6" >

        <a href="<?= route('storemasters.index') ?>" role="button" class="btn btn-light btn-sm mr-2 buttonOverAnimation cancel" title="Cancel">Cancel</a>

        <button type="button" class="btn btn-primary btn-sm mr-2 buttonOverAnimation save" title="Save & New">Save & New</button>

        <button type="button" class="btn btn-primary btn-sm mr-2 buttonOverAnimation save"title="Save & Exit">Save & Exit</button>

    </div>

</nav>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>

<?php echo $__env->make('admin.storemaster.form', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

<?php $__env->stopSection(); ?>



<?php $__env->startSection('end_form'); ?>

<?= Form::close(); ?>

<div class="text-align-right col-md-6" style="margin-left: 650px">

        <a href="<?= route('storemasters.index') ?>" role="button" class="btn btn-light btn-sm mr-2 buttonOverAnimation cancel" title="Cancel">Cancel</a>

        <button type="button" class="btn btn-primary btn-sm mr-2 buttonOverAnimation save" title="Save & New">Save & New</button>

        <button type="button" class="btn btn-primary btn-sm mr-2 buttonOverAnimation save"title="Save & Exit">Save & Exit</button>

</div>

&nbsp;

<?php $__env->stopSection(); ?>

<?php $__env->startSection('script'); ?>

<?= Html::script('backend/js/jquery.form.min.js')?>

<?= Html::script('backend/js/moment.min.js')?>

<?= Html::script('backend/js/daterangepicker.js')?>

<?= Html::script('backend/js/daterangepicker-data.js')?>

<?= Html::script('backend/js/jquery.dataTables.min.js')?>

<script type="text/javascript">

    $(".allow_number").keypress(function(h){if(8!=h.which&&0!=h.which&& 46!=h.which&&(h.which<48||h.which>57))return!1});

</script>



<script type="text/javascript">    

    $(".save").click(function(e){

        e.preventDefault();

        var btn_name = $(this).attr('title');

        var val = $(this).val();

        var url = "<?= URL::route('storemasters.store') ?>";

        var method_type = 'post';

        var token = "<?=csrf_token()?>";



        $('#storemaster_form').ajaxSubmit({

            url: url,

            type: method_type,

            data: { "_token" : token },

            dataType: 'json',

            beforeSubmit : function()

            {

                $("[id$='_error']").empty();

            },

            success : function(resp)

            {  

                if(resp.success == true)

                {

                    if(btn_name == 'Save & Exit'){  

                        toastr.success('StoreMaster Added  Successfully');

                        setTimeout(function(){

                            window.location.href="<?=route('storemasters.index')?>"; 

                        }, 1000);                       

                    }else{

                        location.reload();  

                    }  

                }

            },

            error : function(respObj){    

                toastr.error('Something went wrong');

                $.each(respObj.responseJSON.errors, function(k,v){

                    $('#'+k+'_error').text(v);

                });

                $(".overlay").hide();

            }

        });

    });



    // allow only number

$(".allow_time").keypress(function(h){

    var keyCode =h.which ? h.which : h.keyCode

       if (!(keyCode >= 48 && keyCode <= 58) && !(keyCode >= 65 && keyCode <= 65)   && !(keyCode >= 97 && keyCode <= 97) && !(keyCode >= 109 && keyCode <= 109) && !(keyCode >= 77 && keyCode <= 77)  && !(keyCode >= 32 && keyCode <= 32) && !(keyCode >=  112 && keyCode <= 112) && !(keyCode >= 80 && keyCode <= 80)) {

             return !1;

           }

 });

</script>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.layout.alert', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php echo $__env->make('admin.layout.layout', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/html/stryder_new/resources/views/admin/storemaster/create.blade.php ENDPATH**/ ?>