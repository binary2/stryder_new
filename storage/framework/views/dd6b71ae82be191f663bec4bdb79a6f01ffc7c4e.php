<?php echo '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>'; ?>

<STATUSUPDATE>
    <ORDERNO><?php echo e($order_no); ?></ORDERNO>
    <?php $__currentLoopData = $order_items; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $line_items): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <ITEMSTATUS>
            <ITEMID><?php echo e($line_items['shopify_item_id']); ?></ITEMID>
            <REFID><?php echo e($line_items['sap_id']); ?></REFID>
            <PLANT><?php echo e($line_items['store_code']); ?></PLANT>
            <STATUSCODE>02</STATUSCODE>
            <REMARKS><?php echo e($line_items['status']); ?></REMARKS>
            <INVOICENO><?php echo e($line_items['sap_pack_id']); ?></INVOICENO>
            <AWBNO><?php echo e($line_items['awb_number']); ?></AWBNO>
            <RETURNAWBNO></RETURNAWBNO>
        </ITEMSTATUS>
     <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
</STATUSUPDATE><?php /**PATH /var/www/html/stryder_new/resources/views/sap/awb_order_xml.blade.php ENDPATH**/ ?>