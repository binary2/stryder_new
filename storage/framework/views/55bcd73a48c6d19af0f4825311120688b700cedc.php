    <?php echo e(Html::script('backend/js/jquery-2.1.4.min.js', [],IS_SECURE)); ?>


    <!-- Bootstrap Core JavaScript -->
    <?php echo e(Html::script('backend/js/popper.min.js', [],IS_SECURE)); ?>

    <?php echo e(Html::script('backend/js/bootstrap.min.js', [],IS_SECURE)); ?>

    
    <!-- Slimscroll JavaScript -->
    <?php echo e(Html::script('backend/js/jquery.slimscroll.js', [],IS_SECURE)); ?>


    <!-- Fancy Dropdown JS -->
    <?php echo e(Html::script('backend/js/dropdown-bootstrap-extended.js', [],IS_SECURE)); ?>


    <!-- FeatherIcons JavaScript -->
    <?php echo e(Html::script('backend/js/feather.min.js', [],IS_SECURE)); ?>


    <!-- Toggles JavaScript -->
    <?php echo e(Html::script('backend/js/toggles.min.js', [],IS_SECURE)); ?>

    <?php echo e(Html::script('backend/js/toggle-data.js', [],IS_SECURE)); ?>

    
    <!-- Init JavaScript -->
    <?php echo e(Html::script('backend/js/init.js', [],IS_SECURE)); ?>

    <?php echo e(Html::script('backend/js/dashboard5-data.js', [],IS_SECURE)); ?>

    
    <!-- Daterangepicker JavaScript -->
    <?php echo e(Html::script('backend/js/daterangepicker.js',[],IS_SECURE)); ?>

    <?php echo e(Html::script('backend/js/jquery-datepicker.min.js', [],IS_SECURE)); ?>

    

    <?php echo e(Html::script('backend/js/toster.min.js',[],IS_SECURE)); ?>

    <?php echo e(Html::script('backend/js/timeout.js',[],IS_SECURE)); ?>

    <?php echo e(Html::script('backend/js/jquery.inputmask.bundle.js',[],IS_SECURE)); ?>

    <?php echo e(Html::script('backend/js/jquery.form.min.js', [],IS_SECURE)); ?>

    <?php echo e(Html::script('backend/js/jquery.dataTables.min.js',[],IS_SECURE)); ?>

    <?php echo e(Html::script('backend/js/dataTables.bootstrap4.min.js',[],IS_SECURE)); ?>

    <?php echo e(Html::script('backend/js/dataTables.dataTables.min.js',[],IS_SECURE)); ?>

    <?php echo e(Html::script('backend/js/fnstandrow.js', [],IS_SECURE)); ?>

    <?php echo e(Html::script('backend/js/nprogress.js',[],IS_SECURE)); ?>

    <?php echo e(Html::script('backend/js/jquery.multi-select.js', [],IS_SECURE)); ?>

    <?php echo e(Html::script('backend/js/select2.min.js', [],IS_SECURE)); ?>

    <?php echo e(Html::script('backend/js/sweetalert.js', [],IS_SECURE)); ?>

    <?php echo e(Html::script('backend/js/sweetalert.min.js', [],IS_SECURE)); ?>



<?php echo $__env->yieldContent('script'); ?>
<?php /**PATH /var/www/html/stryder_new/resources/views/admin/layout/footer.blade.php ENDPATH**/ ?>