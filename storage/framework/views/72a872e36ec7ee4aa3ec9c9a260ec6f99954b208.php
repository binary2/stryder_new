<?php echo '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>'; ?>
<STATUSUPDATE>
<ORDERNO><?php echo e($order_no['order_number']); ?></ORDERNO>
<?php $__currentLoopData = $order_items; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
<ITEMSTATUS>
<ITEMID><?php echo e($value['shopify_item_id']); ?></ITEMID>
<REFID><?php echo e($value['sap_id']); ?></REFID>
<PLANT><?php echo e($value['store_code']); ?></PLANT>
<STATUSCODE>09</STATUSCODE>
<REMARKS><?php echo e($value['return_remark']); ?></REMARKS>
<INVOICENO><?php echo e($value['sap_pack_id']); ?></INVOICENO>
<AWBNO><?php echo e($value['awb_number']); ?></AWBNO>
<RETURNAWBNO><?php echo e($value['return_awb_number']); ?></RETURNAWBNO>
</ITEMSTATUS>
<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
</STATUSUPDATE><?php /**PATH /var/www/html/stryder_new/resources/views/sap/shopify_order_return_xml.blade.php ENDPATH**/ ?>