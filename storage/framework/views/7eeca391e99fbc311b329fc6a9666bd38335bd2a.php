<?php echo '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>'; ?>
<STATUSUPDATE>
<ORDERNO><?php echo e($order_number); ?></ORDERNO>
<?php $__currentLoopData = $order_items_data; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $line_items): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
<ITEMSTATUS>
<ITEMID><?php echo e($line_items['item_id']); ?></ITEMID>
<REFID><?php echo e($line_items['reference_id']); ?></REFID>
<PLANT><?php echo e($line_items['store_code']); ?></PLANT>
<STATUSCODE>07</STATUSCODE>
<REMARKS></REMARKS>
<INVOICENO>NULL</INVOICENO>
<AWBNO></AWBNO>
<RETURNAWBNO></RETURNAWBNO>
</ITEMSTATUS>
<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
</STATUSUPDATE><?php /**PATH /var/www/html/stryder_new/resources/views/sap/customer_order_cancel_xml.blade.php ENDPATH**/ ?>