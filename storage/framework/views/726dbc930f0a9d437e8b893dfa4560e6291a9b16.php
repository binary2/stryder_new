    <link rel="shortcut icon" href="favicon.ico">
    <link rel="icon" href="favicon.ico" type="image/x-icon">
    <?php echo e(Html::style('backend/css/jquery.dataTables.min.css',[],IS_SECURE)); ?>

    <?php echo e(Html::style('backend/css/responsive.dataTables.min.css',[],IS_SECURE)); ?>

    
    <?php echo e(Html::style('backend/css/toggles.css',[],IS_SECURE)); ?>

    <?php echo e(Html::style('backend/css/toggles-light.css',[],IS_SECURE)); ?>


    <!-- Select 2 CSS -->
    <?php echo e(Html::style('backend/css/select2.min.css',[],IS_SECURE)); ?>

    <?php echo e(Html::style('backend/css/multi-select.css',[],IS_SECURE)); ?>

    <?php echo e(Html::style('backend/css/sweetalert.min.css',[],IS_SECURE)); ?>

    <?php echo e(Html::style('backend/css/dropzone.css',[],IS_SECURE)); ?>


    <!-- Daterangepicker CSS -->
    <?php echo e(Html::style('backend/css/datepicker.css',[],IS_SECURE)); ?>

    <?php echo e(Html::style('backend/css/daterangepicker.css',[],IS_SECURE)); ?>

    <?php echo e(Html::style('backend/css/jquery-datepicker.css',[],IS_SECURE)); ?>


    <!-- File Upload CSS -->
    <?php echo e(Html::style('backend/css/jquery.fileuploader.css',[],IS_SECURE)); ?>

    <?php echo e(Html::style('backend/css/jquery.fileuploader-theme-thumbnails.css',[],IS_SECURE)); ?>

    <?php echo e(Html::style('backend/css/bootstrap-fileupload.css',[],IS_SECURE)); ?>


    <!-- Fontawesome Icons CSS -->
    <?php echo e(Html::style('backend/css/font-awesome.min.css',[],IS_SECURE)); ?>

    <?php echo e(Html::style('backend/css/all.min.css',[],IS_SECURE)); ?>


    <!-- Custom CSS -->
    <?php echo e(Html::style('backend/css/toster.min.css',[],IS_SECURE)); ?>

    <?php echo e(Html::style('backend/css/style.css',[],IS_SECURE)); ?>

    <?php echo e(Html::style('backend/css/binary.css',[],IS_SECURE)); ?>

<?php /**PATH /var/www/html/stryder_new/resources/views/admin/layout/header_main.blade.php ENDPATH**/ ?>