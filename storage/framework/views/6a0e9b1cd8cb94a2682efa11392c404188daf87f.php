<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <title>Stryder</title>
        <meta name="description" content="Shopify sync" />
        <!--  Css files  -->
        <?php echo $__env->make('admin.layout.header_main', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
        <?php echo $__env->yieldContent('style'); ?>
    </head>
    <body id="body_close_class">
        <?php echo $__env->make('admin.layout.loader', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
        <div class="hk-wrapper hk-vertical-nav hk-icon-nav" >
            <?php echo $__env->yieldContent('start_form'); ?>
            <!-- Nav-Bar-->

            <?php echo $__env->yieldContent('header'); ?>
            <?php echo $__env->make('admin.layout.navbar', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
            <!-- Content Start here -->
            <div class="hk-pg-wrapper">
                <?php echo $__env->yieldContent('content'); ?>
            </div>
            <!-- Content End here -->
            <!-- footer contant -->
            <?php echo $__env->yieldContent('end_form'); ?>
        </div>
        <?php echo $__env->make('admin.layout.footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    </body>
</html><?php /**PATH /var/www/html/stryder_new/resources/views/admin/layout/layout.blade.php ENDPATH**/ ?>