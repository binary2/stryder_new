<?php $__env->startSection('header'); ?>
    <nav class="navbar navbar-expand-xl navbar-light fixed-top hk-navbar">
        <a id="navbar_toggle_btn" class="navbar-toggle-btn nav-link-hover" href="javascript:void(0);"><span class="feather-icon"><i data-feather="menu"></i></span></a>
        <div class="hk-pg-header col-md-6">
            <h4 class="hk-pg-title"><span class="pg-title-icon"><span class="feather-icon"></span></span><?php echo e(__('Return Order')); ?></h4>
        </div>
        <div class="text-align-right col-md-6 pr-40">
    
        </div>
    </nav>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
    <div class="container">
        <div class="row">
            <div class="col-xl-12">
               
            <section class="hk-sec-wrapper">
                <div class="row">
                        <div class="col-sm">
                            <div class="table-wrap">
                                <table id="master_table" class="table table-hover w-100 display pb-30">
                                    <thead>
                                        <tr>
                                            <th></th>
                                            <th>Title</th>
                                            <th>SKU</th>
                                            <th>Quantity</th>
                                            <th>Return Quantity</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('script'); ?>
    <!-- AJAX FOR DATA TABLE  START-->
    <script type="text/javascript">
        var table = "master_table";
        
        var token = "<?php echo e(csrf_token()); ?>";
            
      
        var url="<?= URL::route('return_order.index')?>";

        $(function() 
        {      
            var master = $('#master_table').dataTable({
                "bProcessing": true,
                "bServerSide": true,
                "autoWidth": true,
                "aaSorting": [
                    [1, "desc"]
                ],
                lengthMenu: [
                    [ 50, 100,200,500],
                    [ '50','100','200','500']
                ],
                "sAjaxSource": url,
                "aoColumns": [
                    {
                        "mData": "return_order_id",
                        bSortable: false,
                        bVisible: false,
                        sWidth: "3%",
                       
                    },

                    { "mData": "title",bSortable: true,bVisible:true},
                    { "mData": "sku",bSortable: true},
                    { "mData": "total_qty",bSortable: true},
                    { "mData": "return_qty",bSortable: true},
                    {
                        mData:null,
                        bSortable:false,

                        mRender:function(v,t,o){
                            var html = '<a data-tool-tips="Approve  Request" onclick="approveRequest('+o['return_qty']+','+o['shopify_order_id']+','+o['sku']+','+o['return_reason']+')"><i class="fa fa-check" style="color:#6f7a7f;"></i></a>';
                            html += '<a data-tool-tips="Reject Request" onclick="cancelRequest('+o['return_qty']+','+o['shopify_order_id']+','+o['sku']+','+o['return_reason']+')"><i class="fa fa-close" style="color:#6f7a7f;"></i></a>';
                            return html;
                        },
                    },
                ],
                fnPreDrawCallback : function() { 
                    NProgress.start();
                },
                fnDrawCallback : function (oSettings) {
                    NProgress.done();
                }
            });  
            master.fnSetFilteringDelay(1000);
        });
        
    </script>
    <!-- AJAX FOR DATA TABLE  END-->
    <script type="text/javascript">
        

        function approveRequest(qty,shopify_order_id,sku,return_reason){

            swal({
                title: "Are you sure to approve this return order request?",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, confirm it!",
                cancelButtonText: "No, cancel!",
                closeOnConfirm: false,
                closeOnCancel: true
            },
            function(isConfirm2){
                if(isConfirm2){
                    orderStatus(qty,shopify_order_id,sku,return_reason,'approve');
                    swal("Confirm!", "order has been confirmed for return.", "success");
                }
            });
        }

        function cancelRequest(qty,shopify_order_id,sku,return_reason){

            swal({
                title: "Are you sure to reject this return order request?",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, confirm it!",
                cancelButtonText: "No, cancel!",
                closeOnConfirm: false,
                closeOnCancel: true
            },
            function(isConfirm2){
                if(isConfirm2){
                    orderStatus(qty,shopify_order_id,sku,return_reason,'reject');
                    swal("Confirm!", "order has been rejected for return.", "success");
                }
            });
        }
        

        function orderStatus(qty,shopify_order_id,sku,return_reason,type){

            var qty = qty;
            var sku = sku;
            var shopify_order_id = shopify_order_id;
            var return_reason = return_reason; 
            var type = type;

            $.ajax({
                type: 'post',
                url: "<?php echo e(route('return_order.status')); ?>",
                data: {
                    shopify_order_id: shopify_order_id,
                    qty: qty,
                    sku: sku,
                    return_reason : return_reason,
                    type: type,
                    '_token': '<?php echo e(csrf_token()); ?>'
                },
                beforeSend:function(){
                    $('.text-right').remove();

                },
                success: function (res) {
                    if (res.success == true) {
                       $('#master_table').DataTable().ajax.reload();
                       toastr.success(res.message); 
                    }
                },
                error: function (data) {
                    toastr.error('Something went wrong!');
                    console.log('Error:', data);
                  }
            });
        }
    </script>
    <?php echo $__env->make('admin.layout.alert', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layout.layout', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/html/stryder_new/resources/views/admin/return_order/index.blade.php ENDPATH**/ ?>