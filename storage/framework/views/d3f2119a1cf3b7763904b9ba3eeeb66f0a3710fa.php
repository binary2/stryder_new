<?php echo '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>'; ?>
<ORDER>
    <ORDERNO><?php echo e($SAPOrderNo); ?></ORDERNO>

    <ORDERDATE><?= date('Y-m-d',strtotime($order_data['created_at'])) ?></ORDERDATE>
    <ORDERTIME><?= date('H:m:s',strtotime($order_data['created_at'])) ?></ORDERTIME>
    <ADDRESSINFO>
        <ADDRESSTYPE>SA</ADDRESSTYPE>
        <FIRSTNAME><?php echo e($order_data['shipping_address']['first_name']); ?></FIRSTNAME>
        <LASTNAME><?php echo e($order_data['shipping_address']['last_name']); ?></LASTNAME>
        <PHONENO><?php echo e(preg_replace('/^\+?91|\|91|\D/', '',($order_data['shipping_address']['phone']))); ?></PHONENO>
        <EMAILID><?php echo e($order_data['email']); ?></EMAILID>
        <ADDRESS1><?php echo e($order_data['shipping_address']['address1']); ?></ADDRESS1>
        <ADDRESS2><?php echo e($order_data['shipping_address']['address2']); ?></ADDRESS2>
        <LANDMARK></LANDMARK>
        <CITY><?php echo e($order_data['shipping_address']['city']); ?></CITY>

    <?php if(array_key_exists(strtoupper($order_data['shipping_address']['province']),$state_code)): ?>
    <STATECODE><?php echo e($state_code[strtoupper($order_data['shipping_address']['province'])]); ?></STATECODE>
    <?php endif; ?>
     <STATENAME><?php echo e($order_data['shipping_address']['province']); ?></STATENAME>
        <PINCODE><?php echo e($order_data['shipping_address']['zip']); ?></PINCODE>
        <COUNTRY><?php echo e($order_data['shipping_address']['country']); ?></COUNTRY>
    </ADDRESSINFO>
    <ADDRESSINFO>
        <ADDRESSTYPE>BA</ADDRESSTYPE>
        <FIRSTNAME><?php echo e($order_data['billing_address']['first_name']); ?></FIRSTNAME>
        <LASTNAME><?php echo e($order_data['billing_address']['last_name']); ?></LASTNAME>
        <PHONENO><?php echo e(preg_replace('/^\+?91|\|91|\D/', '',($order_data['billing_address']['phone']))); ?></PHONENO>
        <EMAILID><?php echo e($order_data['email']); ?></EMAILID>
        <ADDRESS1><?php echo e($order_data['billing_address']['address1']); ?></ADDRESS1>
        <ADDRESS2><?php echo e($order_data['billing_address']['address2']); ?></ADDRESS2>
        <LANDMARK></LANDMARK>
        <CITY><?php echo e($order_data['billing_address']['city']); ?></CITY>
    <?php if(array_key_exists(strtoupper($order_data['shipping_address']['province']),$state_code)): ?>
    <STATECODE><?php echo e($state_code[strtoupper($order_data['shipping_address']['province'])]); ?></STATECODE>
    <?php endif; ?>
    <STATENAME><?php echo e($order_data['shipping_address']['province']); ?></STATENAME>
        <PINCODE><?php echo e($order_data['billing_address']['zip']); ?></PINCODE>
        <COUNTRY><?php echo e($order_data['billing_address']['country']); ?></COUNTRY>
    </ADDRESSINFO>
    <PAYMENTINFO>
        <TOTALPAIDAMT><?php echo e($order_total_price); ?></TOTALPAIDAMT>
        <PAYMENTDATE><?= date('Y-m-d',strtotime($order_transaction['created_at'])) ?></PAYMENTDATE>
        <PAYMENTTIME><?= date('H:m:s',strtotime($order_transaction['created_at'])) ?></PAYMENTTIME>
        <?php if(isset($order_transaction['receipt']['x_reference'])): ?><TXNID><?php echo e($order_transaction['receipt']['x_reference']); ?></TXNID><?php endif; ?>
        <?php if(isset($order_transaction['receipt']['txnid'])): ?><TXNID><?php echo e($order_transaction['receipt']['txnid']); ?></TXNID><?php endif; ?>
        
       
    </PAYMENTINFO>
    <?php
        $line_items_gst = collect($order_data['line_items']);
    ?>
    <?php $__currentLoopData = $order_items; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $line_items): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

        <?php
            if($line_items['get_order_item']['total_discounts'] > 0){
                $discount = $line_items['get_order_item']['total_discounts']/$line_items['get_order_item']['quantity'];
                
            }else{
                $discount = 0.00;
               
            }
            //$discount=round($discount);
        ?>
    <ORDERLINE>
        
        <ITEMID><?php echo e($line_items['get_order_item']['shopify_item_id']); ?></ITEMID>
        <REFID><?php echo e($line_items['sap_id']); ?></REFID>
        <STATUSCODE>01</STATUSCODE>
        <MATERIALNO><?php echo e($line_items['sku']); ?></MATERIALNO>
        <PRICE><?php echo e($line_items['get_order_item']['unit_price']); ?></PRICE>
        <DISCOUNT><?php echo e($discount); ?></DISCOUNT>
         <PAIDAMT><?php echo e(number_format($line_items['get_order_item']['unit_price'] - $discount, 2, '.', ',')); ?></PAIDAMT>
        <PLANT><?php echo e($line_items['store_code']); ?></PLANT>
        <COUPONCODE><?php echo e($line_items['get_order_item']['discount_code']); ?></COUPONCODE>
       
    </ORDERLINE>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
</ORDER><?php /**PATH /var/www/html/stryder_new/resources/views/sap/order_xml.blade.php ENDPATH**/ ?>