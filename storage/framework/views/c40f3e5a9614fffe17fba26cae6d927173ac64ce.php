<?php if(Session::has('message')): ?>
	<?php if(Session::get('message_type')): ?>
		<script>
	        $(document).ready(function(){
		        toastr.<?php echo e(Session::get('message_type')); ?>

		        ('<?php echo e(Session::get('message')); ?>');
	        });
	    </script>
    <?php endif; ?>
<?php endif; ?>
<?php if($errors->any()): ?>
    <script type="text/javascript">
        $(document).ready(function(){
            toastr.error("<b>There were some errors.</b>");
        });
    </script>
<?php endif; ?><?php /**PATH /var/www/html/stryder_new/resources/views/admin/layout/alert.blade.php ENDPATH**/ ?>