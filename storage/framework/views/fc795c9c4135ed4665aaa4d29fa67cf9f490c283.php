<?php $__env->startSection('style'); ?>

<?= Html::style('backend/css/sweetalert.min.css') ?>

<style type="text/css">

	.dataTables_wrapper .dataTables_filter input{

		width: 200px;

		margin-left: 10px;

		display: inline-block;

	}

</style>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('header'); ?>

<nav class="navbar navbar-expand-xl navbar-light fixed-top hk-navbar">

	<a id="navbar_toggle_btn" class="navbar-toggle-btn nav-link-hover" href="javascript:void(0);"><span class="feather-icon"><i data-feather="menu"></i></span></a>
    <div class="container">
	<div class="hk-pg-header col-md-6">

		<h4 class="hk-pg-title"><span class="pg-title-icon"><span class="feather-icon"></span></span><?php echo e(__('names.storemaster')); ?></h4>

	</div>

	<div class="text-align-right col-md-6" style="margin-left: -130px">

		<a href="<?= route('storemasters.create') ?>" class="btn btn-primary btn-sm mr-2 buttonOverAnimation" title="<?php echo e(__('names.create_master')); ?>"><?php echo e(__('names.create_master')); ?></a>

	</div>
   </div>
</nav>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>

<div class="container">

	<div class="row">

		<div class="col-xl-12">

			<section class="hk-sec-wrapper">

				<div class="row">

					<div class="col-sm">

						<div class="table-wrap">

							<table id="master_table" class="table table-hover w-100 display pb-30">

								<thead>

									<tr>

										<th></th>

										<th class="tdcustomer">Store Code</th>

										<th class="tdcustomer">Store Name</th>

										<th>Address</th>

										<th class="tdemailid">City</th>

										<th>Pincode</th>

										<th>State</th>

										<th>Latitude</th>

										<th>Longitude</th>

										<th class="tdmobile">Contact Number</th>

										
										<th >Start Time</th>

										<th class="tdactions">Action</th> 

										<th></th>                          

									</tr>

								</thead>

								<tfoot>

									<tr>

										 <th></th>

										<th class="tdcustomer">Store Code</th>

										<th class="tdcustomer">Store Name</th>

										<th>Address</th>

										<th class="tdcustomer">City</th>

										<th>Pincode</th>

										<th>State</th>

										<th>Latitude</th>

										<th>Longitude</th>

										<th class="tdmobile">Contact Number</th>

									

										<th>Start Time</th>

										<th class="tdactions">Action</th>

										<th></th>                           

									</tr>

								</tfoot>

							</table>

						</div>

					</div>

				</div>

			</section>

		</div>

	</div>

</div>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('script'); ?>

<?= Html::script('backend/js/jquery.form.min.js')?>

<?= Html::script('backend/js/jquery.dataTables.min.js') ?>

<?= Html::script('backend/js/dataTables.bootstrap4.min.js') ?>

<?= Html::script('backend/js/dataTables.dataTables.min.js') ?>

<?= Html::script('backend/js/fnstandrow.js')?>

<?= Html::script('backend/js/nprogress.js') ?>

<?=Html::script('backend/js/delete_script.js')?>

<?=Html::script('backend/js/sweetalert.min.js')?>



<!-- AJAX FOR DATA TABLE  START-->

<script type="text/javascript">

	var table = "master_table";

	var title = "Are you sure to delete selected record(s)";

	var text = "You will not be able to recover this record";

	var type = "warning";

	var delete_path = "<?=URL::route('storemasters.destroy')?>";

	var token = "<?php echo e(csrf_token()); ?>";

	var url = "<?=URL::route('storemasters.index')?>";

	

	$('.delete_record').click(function(){

		var delete_id = $('#master_table tbody');

		checkLength(delete_id);



	});



	$(function() 

	{      

		var master = $('#master_table').dataTable({

			"bProcessing": false,

			"bServerSide": true,

			"autoWidth": true,

			"aaSorting": [

			[11, "desc"]

			],

			lengthMenu: [

			[ 50, 100,200,500],

			[ '50','100','200','500']

			],

			"sAjaxSource": url,

			"aoColumns": [

			{ "mData": "store_master_id",bSortable: false,bVisible: false},

			{ "mData": "store_code","className": "tdcustomer",sWidth: "10%",bSortable: true},

			{ "mData": "store_name","className": "tdcustomer",sWidth: "20%",bSortable: true},

			{ "mData": "store_address",bVisible: false},

			{ "mData": "store_city","className": "tdcustomer",bVisible: true},

			{ "mData": "store_pincode",bVisible: true},

			{ "mData": "store_state",bVisible: true},

			{ "mData": "store_latitude",bVisible: false},

			{ "mData": "store_longitude",bVisible: false},

			{ "mData": "store_phone_number","className": "tdmobile",sWidth: "20%",bSortable: true},

			

			{ "mData": "store_time",bVisible: false},

			{

				mData: null,
				
				"className": "tdactions",

				bSortable: false,

				sWidth: "50px",

				sClass: "text-center",

				mRender: function(v, t, o) {

					var id = o['store_master_id'];

					var editurl = '<?=URL::route('storemasters.edit',":id")?>';

					editurl = editurl.replace(':id',o['store_master_id']);

					var delete_url = '<?=URL::route('storemasters.destroy',":id")?>';

					delete_url = delete_url.replace(':id',id);

					var act_html = "<div class='btn-group'>"

					+"<a href='"+editurl+"' title='Edit' data-toggle='tooltip' data-placement='top' class='btn btn-xs btn-default' style='font-size:15px; line-height:9px; padding: 8px 10px'><i class='glyphicon glyphicon-edit'></i></a>"

					+"<a title='Delete' onclick='deleteRecordData("+id+")' data-toggle='tooltip' data-placement='top' class='btn btn-xs btn-default' style='font-size:15px; line-height:9px; padding: 8px 10px;margin-left: -15px;'><i class='glyphicon glyphicon-trash' style='color: red'></i></a>"


					+"</div>"

					return act_html;



				}

			},

			{ "mData": "updated_at",bVisible: false},

			],

			fnPreDrawCallback : function() { 

				NProgress.start();

			},

			fnDrawCallback : function (oSettings) {

				NProgress.done();

			}

		});  



		master.fnSetFilteringDelay(1000);

	});



	function deleteRecordData(id)

	{

		swal({

			title: title,

			text: text,

			type: type,

			showCancelButton: true,

			confirmButtonText: "Yes, delete it!",

			cancelButtonText: "No, cancel it!",

			closeOnConfirm: true,

			closeOnCancel: true

		}, function(isConfirm) {

			if (isConfirm) {

				deleteRequest(delete_path,id,token);

			} 

		});

	}

</script>

<!-- AJAX FOR DATA TABLE  END-->

<?php echo $__env->make('admin.layout.alert', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layout.layout', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/html/stryder_new/resources/views/admin/storemaster/index.blade.php ENDPATH**/ ?>