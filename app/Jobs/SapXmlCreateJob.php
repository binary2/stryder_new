<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Http\Traits\OrderXmlFileCreateTrait;
use App\Http\Traits\ShopifyCurlTrait;
use App\Models\OrderStoreAssign;
use Illuminate\Support\Facades\Storage;
use App\Models\Pincode;
use Config;
use Log;

//class SapXmlCreateJob implements ShouldQueue
 class SapXmlCreateJob
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    use ShopifyCurlTrait;
    use OrderXmlFileCreateTrait;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public $sap_array;
    public function __construct($sap_array)
    {
        $this->sap_array = $sap_array;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        \Log::alert("Create SAPXML job start ");
        $sap_array = $this->sap_array;
        $shopify_order_id = $sap_array['order_id'];
        $main_path = 'public/SAP/order/';
        $state_code = Config::get('constant_array.state_code');
        $transport_mode = Config::get('constant_array.transport_mode');
        $courier_array = Config::get('constant_array.courier');

        $get_order_detail = Storage::get('public/order/'.$shopify_order_id.'/order.json');

        $order_data = json_decode($get_order_detail,1);

        $transaction_response = Storage::get('public/order/'.$shopify_order_id.'/transactions.json');

        $transaction_response = json_decode($transaction_response,true);
        
        foreach ($transaction_response["transactions"] as $transaction_key => $transaction_value) {
            if($transaction_value['status'] == 'success'){
                $success_transaction = $transaction_value;
            }
        }

        $order_items_query = OrderStoreAssign::select()
                                        ->with('getOrderItem','getStoreMaster')
                                        ->where('shopify_order_id',$shopify_order_id)
                                        ->where('store_code','!=',null)
                                        ->where('is_order_generate', 0);

        $order_items = $order_items_query->get()->toArray();

        if(!empty($success_transaction)){
            if(!empty($order_items)){
                $customer_shipping_state = $order_data['shipping_address']['province'];
                /*order json value get start*/
                $total_price=$this->getTotalPrice($shopify_order_id);
               
               // $line_items_price=$this->orderLineItemPriceGet($shopify_order_id);

                foreach ($order_items as $key => $single_items) {
                    $order_items[$key]['get_order_item']['unit_price']=$single_items['get_order_item']['unit_price'];
                    $order_items[$key]['get_order_item']['discount_code']=$single_items['get_order_item']['discount_code'];
                    $order_items[$key]['get_order_item']['total_discounts']=$single_items['get_order_item']['total_discounts'];
                    $order_items[$key]['get_order_item']['quantity']=$single_items['get_order_item']['quantity'];
                }
    
                /*order json value get End*/

                $line_item_tax_line = [];
                foreach ($order_items as $order_key => $order_value) {

                    $SAPOrderNo = $order_value['order_number'];
                   // $store_code_state = $order_value['get_store_master']['store_state'];

                }
                $shipping_charge = 0;
                if(isset($order_data['total_shipping_price_set'])){
                    foreach ($order_data['total_shipping_price_set'] as $shipping_key => $shipping_value) {
                        if($shipping_key == 'shop_money'){
                            $shipping_charge = $shipping_value['amount'];
                        }
                    }
                }

                /*//validation for user form fill start
                    Kamakshi Gandhi @ 30 Nov*/
                $shipping_address_first_name_validated = $this->removeSpecialChar($order_data['shipping_address']['first_name']);
                $shipping_address_last_name_validated = $this->removeSpecialSpace($order_data['shipping_address']['last_name']);
                $shipping_address_address1_validated = $this->removeSpecialSpaceAddress($order_data['shipping_address']['address1']);
                $shipping_address_address2_validated = $this->removeSpecialSpaceAddress($order_data['shipping_address']['address2']);
                $shipping_address_phone_no_validated = $this->validating($order_data['shipping_address']['phone']);
                
                $billing_address_first_name_validated = $this->removeSpecialChar($order_data['billing_address']['first_name']);
                $billing_address_last_name_validated = $this->removeSpecialSpace($order_data['billing_address']['last_name']);
                $billing_address_address1_validated = $this->removeSpecialSpaceAddress($order_data['billing_address']['address1']);
                $billing_address_address2_validated = $this->removeSpecialSpaceAddress($order_data['billing_address']['address2']);
                $billing_address_phone_no_validated = $this->validating($order_data['billing_address']['phone']);
               
                $order_data['shipping_address']['first_name'] = $shipping_address_first_name_validated;
                $order_data['shipping_address']['last_name'] = $shipping_address_last_name_validated;
                $order_data['shipping_address']['address1'] = $shipping_address_address1_validated;
                $order_data['shipping_address']['address2'] = $shipping_address_address2_validated;
                $order_data['shipping_address']['phone'] = $shipping_address_phone_no_validated;
               
                $order_data['billing_address']['first_name'] = $billing_address_first_name_validated;
                $order_data['billing_address']['last_name'] = $billing_address_last_name_validated;
                $order_data['billing_address']['phone'] = $billing_address_phone_no_validated;
                $order_data['billing_address']['address1'] = $billing_address_address1_validated;
                $order_data['billing_address']['address2'] = $billing_address_address2_validated;
                


                $billing_address_city = Pincode::where('pincode',$order_data['billing_address']['zip'])->value('city');
                $shipping_address_city = Pincode::where('pincode',$order_data['shipping_address']['zip'])->value('city');
                if(!empty($billing_address_city)){
                    $order_data['billing_address']['city'] = $billing_address_city;
                }
                if(!empty($shipping_address_city)){
                    $order_data['shipping_address']['city'] = $shipping_address_city;
                }

                /*//validation for user form fill end
                    Kamakshi Gandhi @ 30 Nov*/

                $var1_data = $success_transaction['receipt']['payment_id'];

                \Log::alert("Successful transaction Payment ID passed in Payu API : ".$var1_data);
                
                $mihpayid = $this->hashMake($var1_data);

                $content = View('sap/order_xml',[
                                //'orderRefNo' => $orderRefNo,
                                'order_data' => $order_data,
                                'order_transaction' => $success_transaction,
                                'mihpayid' => $mihpayid,
                                'order_items' => $order_items,
                                'state_code' => $state_code,
                                'transport_mode' => $transport_mode,
                                'courier_array' => $courier_array,
                                'shipping_charge' => $shipping_charge,
                                //'line_item_tax_line' => $line_item_tax_line,
                                'SAPOrderNo' => $SAPOrderNo,
                                'order_total_price'=>$total_price,
                            ])->render();
                $date_time=date("ymdHi");
                $file_name = $SAPOrderNo. '_' .$date_time;
                
                $if_present_out_side=Storage::disk('sftp')->exists('order/'.$file_name);
                $if_present_in_archive=Storage::disk('sftp')->exists('order/Archive/'.$file_name);
                
                if($if_present_out_side == false && $if_present_in_archive == false)
                {
                    $exists = Storage::disk('sftp')->put('order/'.$file_name.'.xml',$content);

                    // OrderStoreAssign:: where('shopify_order_id',$shopify_order_id)->update(['is_order_generate' => 1]);
                    OrderStoreAssign::where('shopify_order_id', $shopify_order_id)->update(['is_order_generate' => 1, 'payu_payment_id' => $var1_data, 'payu_verifypayment_mihpayid' => $mihpayid]);
                }
               
                $save_detail_nostore = new OrderStoreAssign;
                $items_count_notStore = $save_detail_nostore->where('shopify_order_id',$shopify_order_id)
                                                     ->where(function($query){
                                                        $query->where('store_code', 'No-store');
                                                        $query->orWhere('store_code', NULL);
                                                     })
                                                     ->count();

                $tags = "SAP_ALL";
                if($items_count_notStore > 0){
                   $tags = "SAP_PART";
                }
                $url = '/admin/orders/'.$shopify_order_id.'json';
                $data = [
                        'order' => [
                            'id' => $shopify_order_id,
                            'tags' =>$tags,   
                        ]
                    ];
                $response_tag=$this->putCurl($url,$data);  

            }

        }
    }

    public function hashMake($var1_data)
    {
        \Log::alert("Payu api call for mihpayid ");
        try {
            $og_key = 'fOsf3m';
            $og_command = 'verify_payment';
            // $og_var1 = $var1_data;

            $key = 'fOsf3m|';
            $command = 'verify_payment|';
            $salt = 'G5Pxcb8q3zW6MFf6Gp2TJxDJ6vVZnch8';
            $var1 = $var1_data.'|';
            $hashString = $key . $command . $var1 . $salt;
            $hash = strtolower(hash("sha512", $hashString));

            // dd($hash);
            $data = [
                'key' => $og_key,
                'command' => $og_command,
                'var1' => $var1_data,
                'hash' => $hash
            ];
            
            \Log::alert("Payu API Request Data : ".json_encode($data));

            // Convert the data to a query string
            $queryString = http_build_query($data);
            // dd($queryString);
            $curl = curl_init();

            curl_setopt_array($curl, array(
                CURLOPT_URL => 'https://info.payu.in/merchant/postservice?form=2',
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'POST',
                CURLOPT_POSTFIELDS => $queryString,
                CURLOPT_HTTPHEADER => array(
                    'Content-Type: application/x-www-form-urlencoded'
                ),
            ));

            $response = curl_exec($curl);

            curl_close($curl);

            $responsep = json_decode(stripslashes($response), true);
            \Log::alert("Payu API Response Get : " . json_encode($responsep));

            $output_value = json_decode($response);
            $mihpayid = $output_value->transaction_details->$var1_data->mihpayid;

            // $mihpayid = str_pad($mihpayid, 16, "0", STR_PAD_LEFT);

            return $mihpayid;

        } catch (Exception $e) {
            Log::error('General Enquiry Export Excel :- ' . json_encode($e));
        }
    }

    /*//validation for user form fill start
        Kamakshi Gandhi @ 30 Nov*/

    public function removeSpecialChar($string) {
       $string = preg_replace('/[^A-Za-z \-]/', '', $string); 
       return trim($string) ;
    }

    public function removeSpecialSpace($string) {
       $string = str_replace(' ', '', $string); 
       return preg_replace('/[^A-Za-z\-]/', '', $string); 
    }

    public function removeSpecialSpaceAddress($string){
        $total_char = strlen($string);
        $find_first_char = substr($string,0,1);
        
        if(ctype_alnum($find_first_char)){
            return $string;
            // $remove_special_char = $this->removeSpecialSpace($find_first_char);
            // $remove_special_char = $this->removeSpecialSpaceDigit($find_first_char);
            // return $remove_special_char;
        }else{
            for($i=0;$i<$total_char;$i++){
                $string = substr($string,1,strlen($string));
                $find_first_char_txt = substr($string,0,1);
                if(ctype_alnum($find_first_char_txt)){
                    $string = $this->removeSpecialSpaceAddressLast($string);
                    return $string;
                    break;
                }
            }
        }
    }

    public function removeSpecialSpaceAddressLast($string){
        $total_char = strlen($string);
        $find_first_char = substr($string,$total_char,1);
        
        if(ctype_alnum($find_first_char)){
            return $string;
            // $remove_special_char = $this->removeSpecialSpace($find_first_char);
            // $remove_special_char = $this->removeSpecialSpaceDigit($find_first_char);
            // return $remove_special_char;
        }else{
            $string = preg_replace( '/^\W*(.*?)\W*$/', '$1', $string );
            return $string;

        }
    }

    public function removeSpecialSpaceDigit($string) {
       $string = str_replace(' ', '', $string); 
       return preg_replace('/[\@\.\;\" "\!\#\$\%\^\&\*\(\)\,]+/', '', $string);
    }

    public function validating($phone){
        if(preg_match('/^[0-9]{10}+$/', $phone)) {
            return $phone;
        }else{

            $phone = trim($phone);
            
            if($this->startsWith($phone,"0"))
            {
                $phone = ltrim($phone, '0');
            }

            if(strpos($phone, '+91') !== false) {
                $remove_91 = str_replace('+91', '', $phone);
                $check_number = $this->removeSpecialSpaceDigit($remove_91);
                $make_ten_digit =  $this->makeTenDigit($check_number);
                return $make_ten_digit;
            }else{
                $check_number = $this->removeSpecialSpaceDigit($phone);
                $make_ten_digit =  $this->makeTenDigit($check_number);
                return $make_ten_digit;
            }
        }
    }

    public function makeTenDigit($check_number){
        if(strlen($check_number) > 10){
            $make_10_digit = substr_replace($check_number, '', 10);
            return $make_10_digit;
        }else{
            $make_10_digit = str_pad($check_number, 10, "0", STR_PAD_RIGHT);
            return $make_10_digit;
        }
    }

    function startsWith ($string, $startString)
    {
      $len = strlen($startString);
      return (substr($string, 0, $len) === $startString);
    }
    /*//validation for user form fill end
        Kamakshi Gandhi @ 30 Nov*/
}
