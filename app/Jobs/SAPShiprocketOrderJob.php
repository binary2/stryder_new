<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Models\OrderStoreAssign;
use App\Models\Warehouse;
use App\Models\ShiprocketTokenLog;
use Storage;    
use Config,Queue;
use App\Jobs\SapXmlCreateJob;
use Log;

// class SAPShiprocketOrderJob implements ShouldQueue
class SAPShiprocketOrderJob
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public $shiprocket_array;
    public function __construct($shiprocket_array)
    {
        $this->shiprocket_array = $shiprocket_array;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $shiprocket_array = $this->shiprocket_array;

        $shopify_order_id = $shiprocket_array['order_id'];

        $shiprocket_url = $shiprocket_array['shiprocket_url'];
        $order_id = $shiprocket_array['order_id'];
        $store_codes = $shiprocket_array['store_code'];
        $reference_id = $shiprocket_array['reference_id'];
        //$store_code_count = count($store_codes) - 1;

        foreach ($store_codes as $key => $store_code) {
            // $store_assign_data =  OrderStoreAssign::select('sap_id')->where("store_code",$store_code)->where("shopify_order_id",$shopify_order_id)->get()->toArray();

            //     $sap_id = array_column($store_assign_data,'sap_id');

                //$implode_sap_id = implode(",", $sap_id);
            $order_store_assign_model = new OrderStoreAssign;
            $order_table_name = 'orders';
            $order_item_name = 'order_items';
            $product_name = 'products';
            $order_status_table = 'order_statuses';
            $order_store_assign_table = 'order_store_assign';
            $inventory_management_table = 'inventory_management';

            // get order json
            $get_order_detail = Storage::get('public/order/'.$shopify_order_id.'/order.json');
            $order_data = json_decode($get_order_detail,1);
            $awb_number = "";
            try {
            $message = "";
                $shiprockettoken = ShiprocketTokenLog::orderBy('id', 'Desc')->first();

                order_id_change:
             
                // $order_store_assign_model->setTable($order_store_assign_table);
                $order_item_assign = OrderStoreAssign::where('shopify_order_id', $shopify_order_id)
                                                    ->where('store_code',$store_code)
                                                     ->whereIn('sap_id',$reference_id)
                                                    ->with('getOrderItem')
                                                    ->where('is_awb_generate','1')
                                                    ->get()->toArray();
                
            

                $single_item_assign = head($order_item_assign);

                $line_items = [];
                $subtotal_price = 0;

                $store_detail = Warehouse::where('store_code', $store_code)
                                            ->first();


                foreach ($order_item_assign as $item_key => $item_value) {
                    // create one array for push the order in shiprocket
                    if($item_value['get_order_item']['total_discounts'] > 0){
                        $discount = round($item_value['get_order_item']['total_discounts'] * $item_value['quantity'] / $item_value['get_order_item']['quantity'],2);
        
                    }else{
                        $discount = 0;
                       
                    }
                    $line_item_array = [
                        "name"=> $item_value['get_order_item']['title'],
                        // "sku"=> $item_value['get_order_item']['sku'],
                        "sku"=> $item_value['sap_id'] . "_" .$item_value['get_order_item']['sku'],
                        "units"=> $item_value['quantity'],
                        "selling_price"=> $item_value['get_order_item']['unit_price'],
                        "discount"=> $discount,
                        "tax"=> "",
                        "hsn"=> ""
                    ];
                    $line_items[] = $line_item_array;

                    $subtotal_price += $item_value['get_order_item']['unit_price'] * $item_value['quantity'];
                }
                
                if($store_detail['store_code'] == 2200){
                    $pickup_location = Config::get('constant_array.pick_up_name.2200');
                }else if($store_detail['store_code'] == 2204)
                {
                    $pickup_location = Config::get('constant_array.pick_up_name.2204');
                }else if($store_detail['store_code'] == 2203)
                {
                    $pickup_location = Config::get('constant_array.pick_up_name.2203');
                }
                $order_detail = [
                    "order_id"=> $single_item_assign['shiprocket_id'],
                    "order_date"=> $order_data['created_at'],
                    // "pickup_location"=> 'mulund',
                    
                    "pickup_location" => $pickup_location,
                    "channel_id"=> "",
                    "billing_customer_name"=> $order_data['shipping_address']['name'],
                    "billing_last_name"=> $order_data['shipping_address']['last_name'],
                    "billing_address"=> $order_data['shipping_address']['address1'],
                    "billing_address_2"=> $order_data['shipping_address']['address2'],
                    "billing_city"=> $order_data['shipping_address']['city'],
                    "billing_pincode"=> $order_data['shipping_address']['zip'],
                    "billing_state"=> $order_data['shipping_address']['province'],
                    "billing_country"=> $order_data['shipping_address']['country'],
                    "billing_email"=> $order_data['email'],
                    "billing_phone"=> substr(str_replace(" ","",$order_data['shipping_address']['phone']), -10),
                    "shipping_is_billing"=> true,
                    "shipping_customer_name"=> $order_data['shipping_address']['name'],
                    "shipping_last_name"=> $order_data['shipping_address']['last_name'],
                    "shipping_address"=> $order_data['shipping_address']['address1'],
                    "shipping_address_2"=> $order_data['shipping_address']['address2'],
                    "shipping_city"=> $order_data['shipping_address']['city'],
                    "shipping_pincode"=> $order_data['shipping_address']['zip'],
                    "shipping_country"=> $order_data['shipping_address']['country'],
                    "shipping_state"=> $order_data['shipping_address']['province'],
                    "shipping_email"=> $order_data['email'],
                    "shipping_phone"=> substr(str_replace(" ","",$order_data['shipping_address']['phone']), -10),
                    "order_items"=> $line_items,
                    "payment_method"=> "Prepaid",
                    "shipping_charges"=> 0,
                    "giftwrap_charges"=> 0,
                    "transaction_charges"=> 0,
                    "total_discount"=> 0,
                    "sub_total"=> $subtotal_price,
                    "length"=> 0.5,
                    "breadth"=> 0.5,
                    "height"=> 0.5,
                    "weight"=> 0.5
                ];

                $order_json_data = json_encode($order_detail);

                //Storage::put('public/order/'.$order_data['id'].'/shiprocket_request_'.$store_code.'.json',$order_json_data,'public');

                // push order to shiprocket
                $order_curl = curl_init();
                curl_setopt_array($order_curl, array(
                CURLOPT_URL => $shiprocket_url .'/orders/create/adhoc',
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_POSTFIELDS => $order_json_data,
                CURLOPT_HTTPHEADER => array(
                    "Authorization: Bearer ".$shiprockettoken['token'],
                    "Content-Type: application/json"
                  ),
                ));

                $order_response = curl_exec($order_curl);
                //Storage::put('public/order/'.$order_data['id'].'/shiprocket_response_'.$store_code.'.json',$order_response,'public');
                curl_close($order_curl);

                //update the shiprocket order id and shipment id 
                $orer_decode_response = json_decode($order_response,1);

                $transport_mode = "";
                $logistic_partner = "";
                $courier_array = Config::get('constant_array.courier');
                $transport_mode_config = Config::get('constant_array.transport_mode');

                if(isset($orer_decode_response['status_code']) && $orer_decode_response['status_code'] == 3){
                    $new_shiprocket_id = $single_item_assign['shiprocket_id'] . '_1';

                    $order_item_assign = OrderStoreAssign::where('shopify_order_id', $shopify_order_id)
                                        ->where('store_code',$store_code)
                                        ->whereIn('sap_id',$reference_id)
                                        ->update(['shiprocket_id' => $new_shiprocket_id]);
                    goto order_id_change;
                }
                
                if(isset($orer_decode_response['status_code']) && $orer_decode_response['status_code'] == 1){
                    $store_assign_upadte = $order_store_assign_model->where('shopify_order_id', $shopify_order_id)
                                        ->where('store_code', $store_code)
                                        ->whereIn('sap_id',$reference_id)
                                        ->update(['shiprocket_order_id'=> $orer_decode_response['order_id'],'shiprocket_shipment_id' => $orer_decode_response['shipment_id']]);

                    // check service availibility
                    $service_curl = curl_init();
                    curl_setopt_array($service_curl, array(
                        CURLOPT_URL => $shiprocket_url ."/courier/serviceability/?pickup_postcode=". $store_detail['store_pincode'] . "&delivery_postcode=" . $order_data['shipping_address']['zip'] . "&cod=0&weight=0.5",
                        CURLOPT_RETURNTRANSFER => true,
                        CURLOPT_ENCODING => "",
                        CURLOPT_MAXREDIRS => 10,
                        CURLOPT_TIMEOUT => 30,
                        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                        CURLOPT_CUSTOMREQUEST => "GET",
                        CURLOPT_HTTPHEADER => array(
                        "authorization: Bearer ".$shiprockettoken['token'],
                        ),
                    ));

                    $service_response = curl_exec($service_curl);

                    $service_err = curl_error($service_curl);

                    //Storage::put('public/order/'.$order_data['id'].'/shiprocket_service_response_'.$store_code.'.json',$service_response,'public');
                    curl_close($service_curl);
            
                    if ($service_err) {
                        $message = "Pincode not serviceable";
                
                        $reason = 'Shiprocket service response error for Store code :- ' . $store_code;
                        $email_data = ['order_name' => $order_data['name'], 'reason' => $reason];
                        //$this->sendOrderMail($email_data);
                    } else {
                        $service_decode_response = json_decode($service_response,1);
                        if($service_decode_response['status'] == 200 ){
                            $available_courier_companies = collect($service_decode_response['data']['available_courier_companies']);

                            $first_available_courier_companies = $available_courier_companies->where('courier_company_id',$service_decode_response['data']['recommended_courier_company_id'])->first();

                            if($first_available_courier_companies['is_surface'] == 1){
                                $is_surface = 1;
                                if(array_key_exists(strtoupper("SURFACE"),$transport_mode_config)){
                                    $transport_mode = $transport_mode_config[strtoupper("SURFACE")];
                                }
                            }else{
                                $is_surface = 0;
                                if(array_key_exists(strtoupper("AIR"),$transport_mode_config)){
                                    $transport_mode = $transport_mode_config[strtoupper("AIR")];
                                }
                            }
                            $store_assign_update = $order_store_assign_model->where('shopify_order_id', $shopify_order_id)
                                        ->where('store_code', $store_code)
                                        ->whereIn('sap_id',$reference_id)
                                        ->update(['is_surface'=>$is_surface]);

                            $awb_array = [
                                'shipment_id'   => $orer_decode_response['shipment_id'],
                                'courier_id'    => ''
                            ];
                            $awb_json = json_encode($awb_array);
                           // Storage::put('public/order/'.$order_data['id'].'/shiprocket_awb_request_'.$store_code.'.json',$awb_json,'public');
                            
                            // generate awb and logistic partner
                            $awb_curl = curl_init();
                            curl_setopt_array($awb_curl, array(
                                CURLOPT_URL => $shiprocket_url ."/courier/assign/awb",
                                CURLOPT_RETURNTRANSFER => true,
                                CURLOPT_ENCODING => "",
                                CURLOPT_MAXREDIRS => 10,
                                CURLOPT_TIMEOUT => 30,
                                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                                CURLOPT_CUSTOMREQUEST => "POST",
                                CURLOPT_POSTFIELDS => $awb_json,
                                CURLOPT_HTTPHEADER => array(
                                "authorization: Bearer ".$shiprockettoken['token'],
                                "content-type: application/json",
                                ),
                            ));
                            $awb_response = curl_exec($awb_curl);
                            $awb_err = curl_error($awb_curl);
                           // Storage::put('public/order/'.$order_data['id'].'/shiprocket_awb_response_'.$store_code.'.json',$awb_response,'public');
                            curl_close($awb_curl);
                             
                            if ($awb_err) {

                                $message = "AWB not generated";
                                $reason = 'Shiprocket awb response error for Store code :- ' . $store_code;
                                $email_data = ['order_name' => $order_data['name'], 'reason' => $reason];
                               // $this->sendOrderMail($email_data);
                            } else {
                                $awb_decode_response = json_decode($awb_response,1);
                                

                                // dd($awb_decode_/response);
                                if(isset($awb_decode_response['response']['data'])){
                                    $awb_detail = $awb_decode_response['response']['data'];
                                    $awb_number = $awb_detail['awb_code'];
                                    $message = "AWB generated successfully";
                                    $awb_update = $order_store_assign_model->where('shopify_order_id', $shopify_order_id)
                                                                    ->where('store_code', $store_code)
                                                                    ->whereIn('sap_id',$reference_id)
                                                                    ->update(['awb_number' => $awb_number, 'courier_partner' => $awb_detail['courier_name']]);
                                    if(array_key_exists(strtoupper($awb_detail['courier_name']),$courier_array)){
                                        $logistic_partner = $courier_array[strtoupper($awb_detail['courier_name'])];
                                    }
                                }else{
                                    $message = "AWB not generated.";
                                    $reason = 'Shiprocket awb response error for Store code :- ' . $store_code;
                                    $email_data = ['order_name' => $order_data['name'], 'reason' => $reason];
                                   // $this->sendOrderMail($email_data);                                    
                                }
                            }
                        }else{
                            $message = "Pincode not serviceable.";
                            $reason = 'Shiprocket service response error for Store code :- ' . $store_code;
                            $email_data = ['order_name' => $order_data['name'], 'reason' => $reason];
                           // $this->sendOrderMail($email_data);
                        }
                    }
                }else{
                    $message = "Order not created in shiprocket";
                    $email_data = ['order_name' => $order_data['name'], 'reason' => 'ShipRocket order create response error.'];
                   // $this->sendOrderMail($email_data);
                }
                $response_data =[
                    'message' => $message,
                    'awb_number' => $awb_number,
                    'TransportMode' => $transport_mode,
                    'LogisticsID' => $logistic_partner
                ];
                     
                
                $order_json_data_response = json_encode($response_data);
           
                //Storage::put('public/order/'.$shopify_order_id.'/sap_response_'.$store_code.'_'.date('YmdHis').'.json',$order_json_data_response,'public');

            } catch (\Exception $e) {
                $email_data = ['order_name' => $order_data['name'], 'reason' => 'ShipRocket response error.'];
               // $this->sendOrderMail($email_data);
            }
        }

        $fulfill_array = [];
        foreach ($shiprocket_array['item_status'] as $key => $single_item_status) {

            $statuscode[] = $single_item_status['STATUSCODE'];

            if($single_item_status['STATUSCODE'] == '02'){
                $sap_id = $single_item_status['REFID'];
                
                //$awbNO = $single_item_status['AWBNO'];

                $invoice_data_update_array = [
                'sap_pack_id'=>$single_item_status['INVOICENO'],
                'invoice_no'=>$single_item_status['INVOICENO'],
                'status' => 'Ready for Shipping',
                'order_pickup' => date('Y-m-d H:i:s'),
                //'invoice_transaction_id' => $invoice_data['Transactions']['TransactionID'],
                ];

                $order_store_assign_model = new OrderStoreAssign;
                $order_store_assign_model->setTable($order_store_assign_table);

                $invoice_order_id_update = $order_store_assign_model->where('sap_id',$sap_id)
                                                        ->update($invoice_data_update_array);

                $shipment_query = $order_store_assign_model->where('sap_id',$sap_id);
                $shipment_count = $shipment_query->count();
                
                if($shipment_count > 0){
                    $shipment_id = $shipment_query->first();
                    $store_code = $shipment_id['store_code'];
                    $xml_array['order_id'] = $shipment_id['shopify_order_id'];
                    $xml_array['awb_num'] = $shipment_id['awb_number'];
                }

                if(!isset($fulfill_array[$shipment_id['shopify_item_id']])){
                    $fulfill_array[(string)$shipment_id['awb_number']][] = [
                        'id' => (int)$shipment_id['shopify_item_id'],
                        'quantity'=> (int)$shipment_id['quantity'],
                    ];
                }else{
                    $fulfill_array[$shipment_id['shopify_item_id']]['quantity'] += $shipment_id['quantity'];
                }

                $shipment_req[] = $shipment_id['shiprocket_shipment_id'];


                // $shipment_req[] = [
                //     'shipment_id' => $shipment_id['shiprocket_shipment_id']
                // ];
            }
        }
    
       foreach($fulfill_array as $key => $value){
            $fulfill_array[$key] = array_reduce($value,function (array $carry, array $item) {
                $sku = $item['id'];
                
                if (array_key_exists($sku, $carry)) { 
                    $carry[$sku]['quantity'] += $item['quantity'];
                }else { 
                    $carry[$sku] = $item;
                }
                array_values($carry);
                return $carry;
            },
        
            array()
            );

            $fulfill_array[$key] = array_values($fulfill_array[$key]);

        }
        Log::info("shopify fullfillment array");
        Log::info(json_encode($fulfill_array));
        $shipment_pickup = array_unique($shipment_req);
        if(count($shipment_req) > 0){
            //call the order fulfillment api
            $xml_array['location_id'] = SHOPIFY_LOCATION_ID;
            $xml_array['items'] = $fulfill_array;

            if(isset($xml_array['awb_num']) && !empty($xml_array['awb_num']))
            {
                dispatch(new OrderFulfillmentJob($xml_array));
            }
            $shipment_req_json = [];
            foreach ($shipment_pickup as $shipment_key => $shipment_value) {

                $shipment_req_json = [
                    'shipment_id' => [$shipment_value]
                ];
                $shipment_req_json = json_encode($shipment_req_json);
               // Storage::put('public/order/'.$xml_array['order_id'].'/courier_generate_pickup_request_'.$store_code.'.json',$shipment_req_json,'public');

                $curl = curl_init();
                curl_setopt_array($curl, array(
                    CURLOPT_URL => $shiprocket_url . "/courier/generate/pickup",
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_ENCODING => "",
                    CURLOPT_MAXREDIRS => 10,
                    CURLOPT_TIMEOUT => 30,
                    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                    CURLOPT_CUSTOMREQUEST => "POST",
                    CURLOPT_POSTFIELDS => $shipment_req_json,
                    CURLOPT_HTTPHEADER => array(
                    "authorization: Bearer ".$shiprockettoken['token'],
                    "content-type: application/json",
                    ),
                ));

                $response = curl_exec($curl);

                
               $err = curl_error($curl);
               curl_close($curl);
               // Storage::put('public/order/'.$xml_array['order_id'].'/courier_generate_pickup_response_'.$store_code.'.json',$response,'public');
            }
        }else{
            $email = ['binarynotifications@gmail.com','niraj@binaryic.in','gautam@binaryic.in','amitpatel.binary@gmail.com'];

            $order_data = $order_id;
            $subject = 'Invoice Read for Order Name:- ' . $order_data . " " . date('Y-m-d H:i:s');
            $email_view = "mail.invoice_read";
            $emial_data = [
            'file_name' => $file,
            'shipment_count' => count($shipment_req['shipment_id']),

            ];
            // $this->SendMail($email_view,$subject,$emial_data,$email);
        }

        $awb_generate_data = OrderStoreAssign::where("shopify_order_id",$shopify_order_id)->whereNotNull('awb_number')->get()->toArray();

        $content = View('sap/awb_order_xml',[
        'order_no' => $shiprocket_array['order_no'],
        'order_items' => $awb_generate_data
        ])->render();

        $date_time=date("ymdHi");
        $file_name = $shiprocket_array['order_no'].'_'.$date_time;

        $if_present_out_side=Storage::disk('sftp')->exists('AWB/'.$file_name);
        $if_present_in_archive=Storage::disk('sftp')->exists('AWB/Archive/'.$file_name);
        
        if($if_present_out_side == false && $if_present_in_archive == false)
        {
            $exists = Storage::disk('sftp')->put('AWB/'.$file_name.'.xml',$content);
        }

        $filename = last(explode('\\', $shiprocket_array['single_invoice_file']));
        $final_file_name = last(explode('/', $filename));
        $file = $filename;
        $folder_get_server_path_invoicexml_archive = '/invoicexml/Archive/'.$final_file_name;
        $invoice_archive=Storage::disk('sftp')->move($file, $folder_get_server_path_invoicexml_archive);

    }

    public function return_key_exists_array($single_array)
    {
        unset($single_array['quantity']);
        $lentgh=count($single_array);
        foreach ($this->new_array as $key => $search_array) {
            unset($search_array['quantity']);
            if(count(array_intersect($single_array,$search_array))===$lentgh)
            {
              return $key;
            }
        }
         return "new";
    }
}
