<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Models\OrderStoreAssign;
use App\Models\Order;
Use Storage,Config;

class ReturnOrderXMLJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public $xml_files_generate;
    public function __construct($xml_files_generate)
    {
        //
        $this->xml_files_generate = $xml_files_generate;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //
        $xml_files_generate = $this->xml_files_generate;
        $return_array = $xml_files_generate['return_files_xml'];
        $return_awb = $xml_files_generate['awb'];


        foreach($return_array as $key => $return_file_array) {
            $shopify_order_id = $return_file_array['shopify_order_id'];
            $order_item_id = $return_file_array['line_item_id'];
            $sap_id = $return_file_array['sap_id'];
            $transaction_id_cancel[]=$sap_id;
            $quantity = $return_file_array['quantity'];
            // $quantity = 2;
            $store_code = $return_file_array['store_code'];
            $order_no = $xml_files_generate['order_number'];

            for ($i=1; $i <= $quantity; $i++) {

                //return xml file create  
                $transaction_id = $sap_id;
                $return_file_name = $transaction_id.'_return_'.time();
                $data = OrderStoreAssign::select('awb_number','return_reason','invoice_no')->where('shopify_item_id',$order_item_id)->first();
                $awb = $data['awb_number'];
                $remark = Config::get('returnreason')[$data['return_reason']];
                $invoice_no = $data['invoice_no'];
                // dd($remark);
                $content = View('admin/return_order/return_order_xml',['qty'=>$quantity,'order_no' => $order_no,'item_id'=>$order_item_id,'sap_id'=>$sap_id,'status'=>'08','remark' => $remark,'invoice_no' => $invoice_no,'awb' => $awb,'return_awb'=>$return_awb])->render();
                //Storage::put('public/sap/order_status/return/'.$return_file_name.'.xml',$content,'public');
                $return_file_put = Storage::disk('sftp')->put('invoicexml/update_invoicexml/'.$return_file_name.'.xml',$content);
            }
        }
        //dd($xml_files_generate);
    }
}
