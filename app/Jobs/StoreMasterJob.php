<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Models\StoreMaster;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class StoreMasterJob
//class StoreMasterJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($storemaster_data)
    {
        $this->storemaster_data = $storemaster_data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $storemaster_data  = $this->storemaster_data;
       // print_r($store_master_da)
        $storemaster_id = '';
        $slug=null;
        if (isset($storemaster_data['store_master_id'])) {
            $storemaster_id =$storemaster_data['store_master_id'];
        }

        $slug = Str::slug(trim($storemaster_data['store_slug']));
           
        $storemaster=StoreMaster::firstOrNew(['store_master_id'=>$storemaster_id]);
        $storemaster->fill($storemaster_data);
        $storemaster->store_slug=$slug;
        
        $storemaster->store_code=$storemaster_data['store_code'];
        $storemaster->store_name=$storemaster_data['store_name'];
        $storemaster->store_address=$storemaster_data['store_address'];
        $storemaster->store_city=$storemaster_data['store_city'];
        $storemaster->store_country=$storemaster_data['store_country'];
        $storemaster->store_pincode=$storemaster_data['store_pincode'];
        $storemaster->store_state=$storemaster_data['store_state'];
        $storemaster->store_phone_number=$storemaster_data['store_phone_number'];
        $storemaster->store_email=$storemaster_data['store_email'];
        $storemaster->google_map=$storemaster_data['google_map'];
        $storemaster->store_time="10.30 AM to 9.30 PM";
        $storemaster->google_link=$storemaster_data['google_link'];
        $storemaster->save();
    }
}
