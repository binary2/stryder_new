<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Models\Product;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request as GuzzleHttpRequest;
use Storage;
use App\Models\InventoryManagement;
use DB,PDO,Log;
use App\Http\Traits\ShopifyCurlTrait;

class ShopifyInventoryUpdateJob
// class ShopifyInventoryUpdateJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    use ShopifyCurlTrait;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public $product_data;
    public function __construct($product_inventory_data)
    {
        $this->product_inventory_data = $product_inventory_data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try{

           
            $product_inventory_data = $this->product_inventory_data;

            $inventory_url = '/admin/api/2021-07/inventory_levels/set.json';
            $product_data = Product::where('variant_sku',$product_inventory_data['sku'])->first();

            if($product_data != '')
            {


                //\Log::info('product: ' . $product_inventory_data['sku']."---".$product_inventory_data['quantity']);
               
                //\Log::info("product:",$product_inventory_data['sku'],"---",$product_inventory_data['quantity'],"---",NOW());

                $inventory_data = [
                    "location_id" => SHOPIFY_LOCATION_ID,
                    "inventory_item_id"=>$product_data['variant_inventory_item_id'],
                    "available"=>$product_inventory_data['quantity'],
                ];

                $inventory_update = $this->postcurl('POST',$inventory_url,$inventory_data);

                if ($inventory_update != "Not Found"){
                    $inventory_is_update = InventoryManagement::where('sku',$product_inventory_data['sku'])->update(['is_update' => 1]);  
                }else{
                    $inventory_is_not_update = InventoryManagement::where('sku',$product_inventory_data['sku'])->update(['is_update' => 2]);
                }

               
            }
           
        }catch (\Exception $e) {
            \Log::info($e->getMessage());
            \Log::error($e);
            $email = ['binarynotifications@gmail.com','minakshi@binaryic.in','rushik@binaryic.in','saumil.binary@gmail.com','jigar.patel@binaryic.in','rakesh.patel@binaryic.co.in'];
            $subject = 'Inventory sync error of stryder.com '.date('Y-m-d H:i:s');
            $email_view = "mail.inventory_error";
            $email_data = $e;
            $this->SendMail($email_view,$subject,$email_data,$email);
        }
    }
}
