<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Http\Traits\OrderXmlFileCreateTrait;
use App\Http\Traits\ShopifyCurlTrait;
use App\Models\OrderStoreAssign;
use Illuminate\Support\Facades\Storage;
use Config;

// class SAPOrderReturnXMLJob implements ShouldQueue
class SAPOrderReturnXMLJob
{
    //use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    use ShopifyCurlTrait;
    use OrderXmlFileCreateTrait;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public $shopify_order_id;
    public function __construct($shopify_order_id)
    {
        $this->shopify_order_id = $shopify_order_id;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $shopify_order_id = $this->shopify_order_id;
        
        $order_items_query = OrderStoreAssign::select()
                                        ->where('shopify_order_id',$shopify_order_id)
                                        ->where('store_code','!=',null)
                                        ->where('is_shopify_return', 1);
                                        

        $order_items = $order_items_query->get()->toArray();
        dd($order_items);
        
        // if(!empty($success_transaction)){
        //     if(!empty($order_items)){
        //         $customer_shipping_state = $order_data['shipping_address']['province'];
        //         /*order json value get start*/
        //         $total_price=$this->getTotalPrice($shopify_order_id);
               
        //         $line_items_price=$this->orderLineItemPriceGet($shopify_order_id);

        //         foreach ($order_items as $key => $single_items) {
        //             $order_items[$key]['get_order_item']['unit_price']=$line_items_price[$single_items['get_order_item']['sku']]['price'];
        //             $order_items[$key]['get_order_item']['total_discounts']=$line_items_price[$single_items['get_order_item']['sku']]['total_discounts'];
        //             $order_items[$key]['get_order_item']['quantity']=$line_items_price[$single_items['get_order_item']['sku']]['quantity'];
        //         }
        //         /*order json value get End*/

        //         $line_item_tax_line = [];
        //         foreach ($order_items as $order_key => $order_value) {

        //             $SAPOrderNo = $order_value['order_number'];
        //            // $store_code_state = $order_value['get_store_master']['store_state'];

        //         }
        //         $shipping_charge = 0;
        //         if(isset($order_data['total_shipping_price_set'])){
        //             foreach ($order_data['total_shipping_price_set'] as $shipping_key => $shipping_value) {
        //                 if($shipping_key == 'shop_money'){
        //                     $shipping_charge = $shipping_value['amount'];
        //                 }
        //             }
        //         }
        
        //         $content = View('sap/order_xml',[
        //                         //'orderRefNo' => $orderRefNo,
        //                         'order_data' => $order_data,
        //                         'order_transaction' => $success_transaction,
        //                         'order_items' => $order_items,
        //                         'state_code' => $state_code,
        //                         'transport_mode' => $transport_mode,
        //                         'courier_array' => $courier_array,
        //                         'shipping_charge' => $shipping_charge,
        //                         //'line_item_tax_line' => $line_item_tax_line,
        //                         'SAPOrderNo' => $SAPOrderNo,
        //                         'order_total_price'=>$total_price,
        //                     ])->render();
        //         $date_time=date("Y_m_d_H_i");
        //         $file_name = $shopify_order_id. '_' .$date_time;

        //         $if_present_out_side=Storage::disk('sftp')->exists('order/process_order/'.$file_name);
        //         $if_present_in_archive=Storage::disk('sftp')->exists('order/archive_order/'.$file_name);
                
        //         if($if_present_out_side == false && $if_present_in_archive == false)
        //         {
        //             $exists = Storage::disk('sftp')->put('order/process_order/'.$file_name.'.xml',$content);
        //             OrderStoreAssign:: where('shopify_order_id',$shopify_order_id)->update(['is_order_generate' => 1]);
        //         }


        //         $save_detail_nostore = new OrderStoreAssign;
        //         $items_count_notStore = $save_detail_nostore->where('shopify_order_id',$shopify_order_id)
        //                                              ->where(function($query){
        //                                                 $query->where('store_code', 'No-store');
        //                                                 $query->orWhere('store_code', NULL);
        //                                              })
        //                                              ->count();
        //         $tags = "SAP_ALL";
        //         if($items_count_notStore > 0){
        //            $tags = "SAP_PART";
        //         }
        //         $url = '/admin/orders/'.$shopify_order_id.'json';
        //         $data = [
        //                 'order' => [
        //                     'id' => $shopify_order_id,
        //                     'tags' =>$tags,   
        //                 ]
        //             ];
        //         $response_tag=$this->putCurl($url,$data);  
        //     }

        // }
    }
}
