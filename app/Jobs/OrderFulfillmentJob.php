<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Models\OrderStoreAssign;
use App\Http\Traits\ShopifyCurlTrait;
use Log;

class OrderFulfillmentJob
// class OrderFulfillmentJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    use ShopifyCurlTrait;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($fulfill_array)
    {
        $this->fulfill_array = $fulfill_array;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $fulfill_array = $this->fulfill_array;

        $items_array = [];
        foreach ($fulfill_array['items'] as $awb_key => $single_item) {
            $awb_no = $awb_key;
            foreach ($single_item as $key => $value) {
                $items_array[] = $value;
            }

            $tracking_url = 'https://shiprocket.co/tracking/'.$awb_no;
            $fulfillment = [
                "fulfillment"=> [
                    "location_id"=> $fulfill_array['location_id'],
                    'tracking_url' => $tracking_url,
                    "tracking_number"=> $awb_no,
                    "line_items"=> $items_array,
                ],
            ];

            $url = "/admin/api/2020-10/orders/". $fulfill_array['order_id'] ."/fulfillments.json";
            $decode_response = $this->postCurl('POST',$url,$fulfillment);

            Log::info($awb_no,$decode_response);
            if(isset($decode_response['fulfillment'])){
                OrderStoreAssign::where('awb_number',(string)$awb_no)->update(['fulfillment_id' => (string)$decode_response['fulfillment']['id']]);
            }
        }
        
    }
}
