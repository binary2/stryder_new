<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Jobs\CancelOrderSapXmlCreateJob;
use App\Models\OrderItem;
use App\Models\Order;
use App\Models\OrderStoreAssign;
use Storage;
use Log;
use App\Http\Traits\SendMailTrait;

class CustomerOrderCancelJob 
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    use SendMailTrait;
    
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public $request_data;
    public $order_items_data;
    public function __construct($request_data,$order_items_data)
    {
        //
        $this->request_data = $request_data;
        $this->order_items_data = $order_items_data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //
        $request_data = $this->request_data;
        $order_items_data = $this->order_items_data;
        $shopify_order_id = $request_data['shopify_order_id'];
        $sku = $request_data['line_sku'];
        $cancelled_qty = $request_data['cancelled_qty'];
        $order_number = $request_data['order_number'];

        $update_data = OrderItem::where('shopify_order_id',$shopify_order_id)
                                        ->where('sku',$sku)
                                        ->update([
                                            'cancel_quantity' => $cancelled_qty,
                                            'status' => 'cancel',
                                            'order_cancel' => date('Y-m-d H:i:s')
                        ]);

        $customer_email = Order::where('shopify_order_id',$shopify_order_id)
                        ->select('customer_email')->first();

        Log::info('Customer Email' . $customer_email);

        $order_store_assign_model = new OrderStoreAssign;

        $update_data = $order_store_assign_model->where('shopify_order_id',$shopify_order_id)
                                        ->where('sku',$sku)
                                        ->where('cancel_quantity',0)
                                        ->where('invoice_no',NULL)
                                        ->limit($cancelled_qty)
                                        ->update([
                                            'cancel_quantity' => 1,
                                            'status' => 'cancel',
                                            'order_cancel' => date('Y-m-d H:i:s')
                                        ]);

        $content = View('sap/customer_order_cancel_xml',[
                            'order_items_data' => $order_items_data['order_items'],
                            'order_number' => $order_number,
                        ])->render();

        $date_time=date("ymdHi");
        $file_name = $order_number.'_'.$date_time;
       // $exists = Storage::disk('sftp')->put('cancel/'.$file_name.'.xml',$content);
        $exists = Storage::disk('sftp')->put('cancel/'.$file_name.'.xml',$content);

        try {

            $curl = curl_init();

            curl_setopt_array($curl, array(
                CURLOPT_URL => 'https://stryder-bikes-til.myshopify.com/admin/api/2023-04/orders/' . $shopify_order_id . '/refunds.json',
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'POST',
                CURLOPT_POSTFIELDS => '{"refund":{"notify":true,"note":"wrong size","refund_line_items":[{"line_item_id":' . $request_data['shopify_item_id'] . ',"quantity":' . $request_data['cancelled_qty'] . ',"restock_type":"cancel","location_id":67731980540}]}}',
                CURLOPT_HTTPHEADER => array(
                    'X-Shopify-Access-Token: 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',
                    'Content-Type: application/json',
                    'Cookie: _secure_admin_session_id=9dab923b06d00c1504d082f9c1eabe1e; _secure_admin_session_id_csrf=9dab923b06d00c1504d082f9c1eabe1e'
                ),
            ));

            $response = curl_exec($curl);

            curl_close($curl);
            echo $response;

        } catch (\Exception $e) {
            Log::error($e->getMessage());
        }

        try{
            $to = ['marketing.ludhiana@tatainternational.com'];
            $email = ['charanpreet.kalsi@tatainternational.com','vikas.sharma@tatainternational.com'];
            $subject = 'Order Cancellment Success'.date('Y-m-d H:i:s');
            $email_data = "Cancellation of Order Confirmed<br><br>Order Details are as below<br><br>Order Number :" . $order_number . "<br><br>Item SKU :" . $sku;
            $from = 'stryder.noreply@tatainternational.com';
            $this->sendmail($to,$email,$subject,$email_data,$from);
            Log::notice('Order Cancellment Success Mail Sent Successfully');
        }
        catch (Exception $e) {
            Log::error('General Enquiry Export Excel :- ' .json_encode($e));
        }
    }
}
