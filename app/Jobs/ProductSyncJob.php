<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Http\Traits\ShopifyCurlTrait;
use App\Jobs\SaveProductTwoFiveZeroJob;

//class ProductSyncJob implements ShouldQueue
class ProductSyncJob
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    use ShopifyCurlTrait;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public $product_url;
    public function __construct($product_url)
    {
        $this->product_url = $product_url;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $products = $this->product_url;
        $product_url = $products['product_url'];
        $params = $products['params'];

        $response = $this->getCurlWithHeader($product_url,$params);
      
        $response_body = $response['response_body']; 
          dispatch(new SaveProductTwoFiveZeroJob($response_body));
        
        if(isset($response['response_link'])){

            if(strstr( $response['response_link'], 'next' ) ) {

                $comm_explde = explode(',', $response['response_link']);
                if(isset($comm_explde[1])){
                    $link = $comm_explde[1];
                }else{
                    $link = $comm_explde[0];
                }

                $link_explode = explode('&',$link);
                $final_link = explode('>',$link_explode[1]);
                $token = explode('page_info=',$final_link[0]);
               
                $new_params = ['limit' => 250, 'page_info' => $token[1]];
                $new_products = [
                    'product_url' => $product_url,
                    'type' => $type,
                    'params' => $new_params,
                ];
                \Bus::dispatch(new ProductSyncJob($new_products));
            }
        }
        return;
    }
}
