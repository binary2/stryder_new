<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Models\OrderStoreAssign;
use App\Models\OrderItem;
use App\Models\Order;
use App\Models\ShiprocketTokenLog;
use Storage,File;
use App\Jobs\OrderFulfillmentJob;
use App\Jobs\SAPShiprocketOrderJob;
//use App\Http\Traits\SendMailTrait;
use App\Http\Traits\NewFetchStoreCodeTrait;

class SingleInvoiceXMLReadJob
// class SingleInvoiceXMLReadJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels, NewFetchStoreCodeTrait;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public $file_name;
    public $location_id;
    public $ship_url;

    public function __construct($file_name,$location_id,$ship_url)
    {
        $this->file_name = $file_name;
        $this->location_id = $location_id;
        $this->ship_url = $ship_url;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $single_invoice_file = $this->file_name;
        $location_id = $this->location_id;
        $ship_url = $this->ship_url;
        $shiprockettoken = ShiprocketTokenLog::orderBy('id', 'Desc')->first();
        $filename = last(explode('\\', $single_invoice_file));
        $final_file_name = last(explode('/', $filename));
        
        // $filename = "invoicexml/9620440731.xml";
        $file = $filename;
        // dd($file);
       
        try{
            $xml_invoice_file = Storage::disk('sftp')->get($file);
            $xml_object = simplexml_load_string($xml_invoice_file);
            $xml_file_array = json_decode(json_encode($xml_object),true);
            // dd($xml_file_array);
            
            if(!empty($xml_file_array)){
                $order_id = $xml_file_array['ORDERNO'];
                
                $order_store_assign_table = 'order_store_assign';
               // $transaction_id = $invoice_data['Transactions'];
                $fulfill_array = [];
                $shipment_req['shipment_id'] = [];
                $store_code = "";
                
                if(isset($xml_file_array['ITEMSTATUS'][0])){
                    $item_status = $xml_file_array['ITEMSTATUS'];
                }else{
                    $item_status[] = $xml_file_array['ITEMSTATUS'];
                }
                
                $multiple_awbNo = [];
                $sap_id_update = [];
                foreach ($item_status as $key => $single_item_status_1) {
                    if($single_item_status_1['STATUSCODE'] == '02')
                    {
                        $store_codes[] = $single_item_status_1['PLANT'];
                        $sap_id_update[] = $single_item_status_1['REFID'];
                        $update_data = OrderStoreAssign::where([
                            ['sap_id',$single_item_status_1['REFID']],
                            ['shopify_item_id',$single_item_status_1['ITEMID']],
                            ['store_code',$single_item_status_1['PLANT']],
                            ['is_awb_generate',0],['cancel_quantity',0]])->update(['is_awb_generate'=>'1']);
                        

                    }else if($single_item_status_1['STATUSCODE'] == '05')
                    {
                        $orders_store_assign_query = OrderStoreAssign::where([
                            ['sap_id',$single_item_status_1['REFID']],
                            ['shopify_item_id',$single_item_status_1['ITEMID']],
                            ['is_awb_generate',0],['cancel_quantity',0]]);
                        $get_store_data = $orders_store_assign_query->first();

                        $update_data = $orders_store_assign_query->update(['is_awb_generate'=>'2','previous_store_code' => $single_item_status_1['PLANT']]);
                        $order_data = OrderItem::where('shopify_item_id',$single_item_status_1['ITEMID'])->first();
                        $pincode_arr = ['pincode_val'=>$order_data['state'],'sku'=>$order_data['sku'],'quantity'=>1,'store_code' => $single_item_status_1['PLANT'],'previous_store_code' => $get_store_data['previous_store_code']];
                        $warehouse_code = $this->reAssignStore($pincode_arr);
                        //$warehouse_code = '';

                        if($warehouse_code != null && $warehouse_code != '' && $get_store_data['rehoping_count'] < 3)
                        {
                            $random_number = rand(10,99);
                            $warehouse_codes[] = $warehouse_code;
                            $rehoping_count = $get_store_data['rehoping_count'] + 1;
                            $updateDetails = [
                                'store_code'=>$warehouse_code,
                                'is_order_generate'=>'0',
                                'is_awb_generate'=>'0',
                                'rehoping_count' => $rehoping_count,
                                'shiprocket_id'=>$order_id.'_'.$warehouse_code.'_'.$random_number,
                            ];
                            $update_storecode = OrderStoreAssign::where([
                                ['sap_id',$single_item_status_1['REFID']],
                                ['shopify_item_id',$single_item_status_1['ITEMID']],
                                ['is_awb_generate',2],['cancel_quantity',0]])->update($updateDetails);
                        }else if($warehouse_code == '' || $warehouse_code == null || $get_store_data['rehoping_count'] == 3){

                            $order_data = Order::select('shopify_order_id')->where('order_number',$order_id)->first()->toArray();

                            $order_item_data = OrderItem::where('shopify_order_id',$order_data['shopify_order_id'])
                                        ->where('shopify_item_id',$single_item_status_1['ITEMID'])
                                        ->first()->toArray();
                            $total_cancel_quantity =  $order_item_data['cancel_quantity'] + 1;

                            $update_order_item_data = OrderItem::where('shopify_order_id',$order_data['shopify_order_id'])
                                            ->where('shopify_item_id',$single_item_status_1['ITEMID'])
                                            ->update([
                                                'cancel_quantity' => $total_cancel_quantity,
                                                'status' => 'cancel',
                                                'order_cancel' => date('Y-m-d H:i:s')
                            ]);

                            $order_store_assign_model = new OrderStoreAssign;

                            $update_data = $order_store_assign_model->where('shopify_order_id',$order_data['shopify_order_id'])
                                            ->where('shopify_item_id',$single_item_status_1['ITEMID'])
                                            ->where('store_code',$single_item_status_1['PLANT'])
                                            ->where('sap_id',$single_item_status_1['REFID'])
                                            ->where('cancel_quantity',0)
                                            ->where('invoice_no',NULL)
                                            ->limit(1)
                                            ->update([
                                                'cancel_quantity' => 1,
                                                'status' => 'cancel',
                                                'order_cancel' => date('Y-m-d H:i:s')
                                            ]);
                        
                            $line_item[]  = [
                                'store_code' => $single_item_status_1['PLANT'],
                                'item_id' => $single_item_status_1['ITEMID'],
                                'reference_id' => $single_item_status_1['REFID'],
                            ];
                        }
                    }
                }
                if(isset($store_codes) && !empty($store_codes))
                {
                    $shopify_order_id = OrderStoreAssign::where('order_number',$order_id)->value('shopify_order_id');
                    $shiprocket_array = [
                        'order_id'=> $shopify_order_id,
                        'order_no'=>$order_id,
                        'store_code' => array_unique($store_codes),
                        'shiprocket_url' => SHIPROCKET_API_URL,
                        'item_status' => $item_status,
                        'single_invoice_file' => $single_invoice_file,
                        'reference_id' => $sap_id_update,
                    ];

                    $order_create_data = dispatch(new SAPShiprocketOrderJob($shiprocket_array));

                                        
                }
                if(isset($warehouse_codes) && !empty($warehouse_codes))
                {
                    $unique_store_code = array_unique($warehouse_codes);
                    $shiprocket_url = SHIPROCKET_API_URL;

                    $sap_shiprocket_order_job = [
                        'order_id' => $order_data['shopify_order_id'],
                        'store_code' => $unique_store_code,
                        'shiprocket_url' => $shiprocket_url,
                    ];
                   $order_rehope_data = dispatch(new SapXmlCreateJob($sap_shiprocket_order_job));
                }
                if(isset($line_item) && $line_item != null)
                {
                    $content = View('sap/cancel_order_xml',[
                        //'orderRefNo' => $orderRefNo,
                        'order_number' => $order_id,
                        'order_items' => $line_item
                    ])->render();


                    $date_time=date("ymdHi");
                    $file_name = $order_id.'_'.$date_time;

                    $if_present_out_side=Storage::disk('sftp')->exists('cancel/'.$file_name);
                    $if_present_in_archive=Storage::disk('sftp')->exists('cancel/Archive/'.$file_name);
                    
                    if($if_present_out_side == false && $if_present_in_archive == false)
                    {
                        $exists = Storage::disk('sftp')->put('cancel/'.$file_name.'.xml',$content);
                    }
                }
                
                // $folder_get_server_path_invoicexml_archive = '/invoicexml/Archive/'.$final_file_name;
                // $invoice_archive=Storage::disk('sftp')->move($file, $folder_get_server_path_invoicexml_archive);
               
            }
        
        }catch(Exception $e){
            $email = ['binarynotifications@gmail.com','niraj@binaryic.in','gautam@binaryic.in','amitpatel.binary@gmail.com'];

            $order_data = $order_id;
            $subject = 'Invoice Read for Order Name:- ' . $order_data . " " . date('Y-m-d H:i:s');
            $email_view = "mail.invoice_read";
            $emial_data = [
                'file_name' => $file,
                'shipment_count' => count($shipment_req['shipment_id']),

            ];
           // $this->SendMail($email_view,$subject,$emial_data,$email);
        }
    }
}
