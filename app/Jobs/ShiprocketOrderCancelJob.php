<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Models\OrderStoreAssign;
use App\Models\ShiprocketTokenLog;

//class ShiprocketOrderCancelJob implements ShouldQueue
class ShiprocketOrderCancelJob
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public $request_data;
    public function __construct($request_data)
    {
        //
        $this->request_data = $request_data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //
        $request_data = $this->request_data;
        $shopify_order_id = $request_data['shopify_order_id'];
        $sku = $request_data['line_sku'];
        $cancelled_qty = $request_data['cancelled_qty'];
        $order_no = $request_data['order_number'];
        $shiprocket_url = SHIPROCKET_API_URL;
        $awb_number = OrderStoreAssign::where('shopify_order_id',$shopify_order_id)->first();
        $awb_no= ['awbs'=>[$awb_number['awb_number']]];
        $awb_number = json_encode($awb_no);
        $shiprockettoken = ShiprocketTokenLog::orderBy('id', 'Desc')->first();
       
        $order_curl = curl_init();
        curl_setopt_array($order_curl, array(
            CURLOPT_URL => $shiprocket_url.'/orders/cancel/shipment/awbs',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => $awb_number,
            CURLOPT_HTTPHEADER => array(
                "Authorization: Bearer ".$shiprockettoken['token'],
                "Content-Type: application/json"
            ),
        ));
        $order_response = curl_exec($order_curl);
        dd($order_response);

    }
}
