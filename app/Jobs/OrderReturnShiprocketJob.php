<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Http\Traits\ShiprocketAPITrait;

use App\Models\OrderStoreAssign;
use App\Models\ShiprocketTokenLog;
use App\Models\Warehouse;
use Storage;

// class OrderReturnShiprocketJob implements ShouldQueue
class OrderReturnShiprocketJob 
{
    use ShiprocketAPITrait;
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public $new_item_array;
    public $shopify_order_id; 
    public function __construct($new_item_array,$shopify_order_id)
    {
        //
        $this->new_item_array = $new_item_array;
        $this->shopify_order_id = $shopify_order_id;
    }

    /**
     * Execute the job.
     *
     * @return void
     */

    // $shopify_order_id = $request_data['shopify_order_id'];
    //     $sku = $request_data['sku'];
    //     $return_qty = $request_data['qty'];
        
    public function handle()
    {
        $new_item_array = $this->new_item_array;
        $shopify_order_id = $this->shopify_order_id;
        $return_reason = "return";

        //foreach ($new_item_array as $key => $value) {
            $line_item_id = $new_item_array['unieue_item_id'];
            $return_qty = $new_item_array['qnt_sum'];
            $reference_id = $new_item_array['reference_id'];

            $sku = $new_item_array['unique_sku'];
    
            $order_query = OrderStoreAssign::where('shopify_order_id',$shopify_order_id)
                                      ->whereIn('sku',$sku)
                                      ->whereIn('sap_id',$reference_id);

            $order_count = $order_query->count();

            $shiprocket_url = SHIPROCKET_API_URL;
           
            $get_order_detail = Storage::get('public/order/'.$shopify_order_id.'/order.json');
            $order_data = json_decode($get_order_detail,1);
           
            $return_order_data = $order_query->where('is_delivered',1)
                                                ->where('return_awb_number',null)
                                                ->where('return_quantity',0)
                                                ->with('getOrderItem')
                                                ->take($return_qty)
                                                ->get()->toArray();
    
            $shiprockettoken = ShiprocketTokenLog::orderBy('id', 'Desc')->first();
                
            $final_return_item = array_reduce($return_order_data, function($accumulator, $element){
                $accumulator[$element['store_code']][] = $element;
                return $accumulator;
            });

            foreach ($final_return_item as $store_key => $store_wise_value) {
    
                $line_items = [];
                $return_files_xml = [];
                $subtotal_price = 0;

                $return_store_detail = Warehouse::where('store_code',$store_key)->first();
                
                $return_store_code = $return_store_detail['store_code'];
                    
                $return_shiprocket_id = 'R2_'. $order_data['order_number'];
                foreach ($store_wise_value as $return_item_key => $return_item_value) {
                    if($return_item_key == 0){
                        $return_shiprocket_id .= '_'. $return_item_value['order_store_id']; 
                    }
                    $return_file_array = [
                        'line_item_id' => $return_item_value['shopify_item_id'],
                        'sap_id' => $return_item_value['sap_id'],
                        'shopify_order_id' => $shopify_order_id,
                        'quantity' => $return_item_value['quantity'],
                        'store_code' => $store_key
                    ];
                    $return_files_xml[] = $return_file_array;
                    if($return_item_value['get_order_item']['total_discounts'] > 0){
                        $discount = round($return_item_value['get_order_item']['total_discounts'] * $return_item_value['quantity'] / $return_item_value['get_order_item']['quantity'],2);
                    }else{
                        $discount = 0;
                    }
                    $line_item_array = [
                        "name"=> $return_item_value['get_order_item']['title'],
                        // "sku"=> $return_item_value['get_order_item']['sku'],
                        "sku"=> $return_item_value['sap_id']. '_' . $return_item_value['get_order_item']['sku'],
                        "units"=> $return_item_value['quantity'],
                        "selling_price"=> $return_item_value['get_order_item']['unit_price'],
                        "discount"=> $discount,
                        "tax"=> "",
                        "hsn"=> ""
                    ];
                    $line_items[] = $line_item_array;
                    $subtotal_price += $return_item_value['get_order_item']['unit_price'] * $return_item_value['quantity'];
                }
                $address = $order_data['shipping_address']['address1']. ' ' .$order_data['shipping_address']['address2'];

                $order_detail = [
                    "order_id" => $return_shiprocket_id,
                    "order_date" => $order_data['created_at'],
                    "channel_id" => "2737164",
                    "pickup_customer_name" => $order_data['shipping_address']['first_name'],
                    "pickup_last_name" => $order_data['shipping_address']['last_name'],
                    "pickup_address" => $address,
                    "pickup_city" => $order_data['shipping_address']['city'],
                    "pickup_state" => $order_data['shipping_address']['province'],
                    "pickup_country" => $order_data['shipping_address']['country'],
                    "pickup_pincode" => $order_data['shipping_address']['zip'],
                    "pickup_email" => $order_data['email'],
                    "pickup_phone" => substr(str_replace(" ","",$order_data['shipping_address']['phone']), -10),
                    'pickup_location_id' => "797251",
                    "shipping_customer_name" => $return_store_detail['store_name'],
                    "shipping_last_name" => $return_store_detail['store_name'],
                    "shipping_address" => $return_store_detail['store_address'],
                    "shipping_address_2" => $return_store_code,
                    "shipping_city" => $return_store_detail['store_city'],
                    "shipping_pincode" => $return_store_detail['store_pincode'],
                    "shipping_country" => "India",
                    "shipping_state" => $return_store_detail['store_state'],
                    "shipping_email" => $return_store_detail['store_email'],
                    "shipping_phone" => substr(str_replace(" ","",$return_store_detail['store_phone_number']), -10),
                    "order_items" => $line_items,
                    "payment_method" => "Prepaid",
                    "shipping_charges" => 0,
                    "giftwrap_charges" => 0,
                    "transaction_charges" => 0,
                    "total_discount" => 0,
                    "sub_total" => $subtotal_price,
                    "length" => 0.5,
                    "breadth" => 0.5,
                    "height" => 0.5,
                    "weight" => 0.5,
                ];
                // dd($order_detail);
                $order_json_data = json_encode($order_detail);
                // dd($order_json_data);
                
                Storage::put('public/order/'.$order_data['id'].'/shiprocket_request_return_'.$store_key.'.json',$order_json_data,'public');
                $order_curl = curl_init();
                curl_setopt_array($order_curl, array(
                    CURLOPT_URL => $shiprocket_url .'/orders/create/return',
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_ENCODING => "",
                    CURLOPT_MAXREDIRS => 10,
                    CURLOPT_TIMEOUT => 0,
                    CURLOPT_FOLLOWLOCATION => true,
                    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                    CURLOPT_CUSTOMREQUEST => "POST",
                    CURLOPT_POSTFIELDS => $order_json_data,
                    CURLOPT_HTTPHEADER => array(
                        "Authorization: Bearer ".$shiprockettoken['token'],
                        "Content-Type: application/json"
                    ),
                ));
                $order_response = curl_exec($order_curl);
                Storage::put('public/order/'.$order_data['id'].'/shiprocket_response_return_'.$store_key.'.json',$order_response,'public');

                curl_close($order_curl);

                $order_json_decode = json_decode($order_response,1);
                
                if(isset($order_json_decode['order_id']))
                {
                     $return_shiprocket_order_id = $order_json_decode['order_id'];
                     $return_shiprocket_shipment_id = $order_json_decode['shipment_id'];
                }
                else
                {
                    $get_shiprocket_detail=OrderStoreAssign::select('shiprocket_order_id','shiprocket_shipment_id')
                                                ->where('shopify_order_id',$shopify_order_id)
                                                ->whereIn('sku',$sku)
                                                ->whereIn('sap_id',$reference_id)
                                                ->where('return_awb_number',null)
                                                ->first(); 


                    $return_shiprocket_order_id = $get_shiprocket_detail['shiprocket_order_id'];
                    $return_shiprocket_shipment_id = $get_shiprocket_detail['shiprocket_shipment_id'];

                }
                $order_assign_table_update = OrderStoreAssign::where('shopify_order_id',$shopify_order_id)
                                    ->whereIn('sku',$sku)
                                    ->whereIn('sap_id',$reference_id)
                                    ->where('store_code',$store_key)
                                    ->where('is_delivered',1)
                                    ->where('return_awb_number',null)
                                    ->limit(count($store_wise_value))
                                    ->update([
                                        'return_shiprocket_id' => $return_shiprocket_id,
                                        'return_store_code' => $return_store_code,
                                        'return_shiprocket_shipment_id' => $return_shiprocket_shipment_id,
                                        'return_shiprocket_order_id' => $return_shiprocket_order_id,
                                        "return_reason"=>$return_reason
                                    ]);
                $check_service = curl_init();
                curl_setopt_array($check_service, array(
                    CURLOPT_URL => $shiprocket_url .'/courier/serviceability/?order_id='.$return_shiprocket_order_id.'&is_return=1&pickup_postcode='.$return_store_detail["store_pincode"].'&delivery_postcode='.$order_data['billing_address']['zip'],
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_ENCODING => "",
                    CURLOPT_MAXREDIRS => 10,
                    CURLOPT_TIMEOUT => 0,
                    CURLOPT_FOLLOWLOCATION => true,
                    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                    CURLOPT_CUSTOMREQUEST => "GET",
                    CURLOPT_HTTPHEADER => array(
                        "Authorization: Bearer ".$shiprockettoken['token'],
                        "Content-Type: application/json"
                    ),
                ));
                $response = curl_exec($check_service);
                $json_decode = json_decode($response);
                curl_close($check_service);
                $awb_generate_data = [
                    'shipment_id' => $return_shiprocket_shipment_id,
                    'is_return  ' => 1
                ];
                $awb_generate_data_json = json_encode($awb_generate_data);
                Storage::put('public/order/'.$order_data['id'].'/shiprocket_awb_request_return_'.$store_key.'.json',$awb_generate_data_json,'public');

                $return_shipment_awb_generate = curl_init();
                curl_setopt_array($return_shipment_awb_generate, array(
                    CURLOPT_URL => $shiprocket_url .'/courier/assign/awb',
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_ENCODING => "",
                    CURLOPT_MAXREDIRS => 10,
                    CURLOPT_TIMEOUT => 0,
                    CURLOPT_FOLLOWLOCATION => true,
                    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                    CURLOPT_CUSTOMREQUEST => "POST",
                    CURLOPT_POSTFIELDS => $awb_generate_data_json,
                    CURLOPT_HTTPHEADER => array(
                        "Authorization: Bearer ".$shiprockettoken['token'],
                        "Content-Type: application/json"
                    ),
                ));
                $return_shipment_awb_generate_response = curl_exec($return_shipment_awb_generate);
                Storage::put('public/order/'.$order_data['id'].'/shiprocket_awb_response_return_'.$store_key.'.json',$return_shipment_awb_generate_response,'public');


                $awb_decode_response = json_decode($return_shipment_awb_generate_response,1);
                
                if(isset($awb_decode_response['response']['data'])){
                    $awb_detail = $awb_decode_response['response']['data'];
                    $return_awb_number = $awb_detail['awb_code'];
                    $order_assign_table_update = OrderStoreAssign::where('shopify_order_id',$shopify_order_id)
                                            ->whereIn('sku',$sku)
                                            ->whereIn('sap_id',$reference_id)
                                            ->where('store_code',$store_key)
                                            ->where('is_delivered',1)
                                            ->where('return_awb_number',null)
                                            ->limit(count($store_wise_value))
                                            ->update([
                                                'return_awb_number' => $return_awb_number,
                                                'return_quantity' => 1,
                                                'status' => 'RETURN INITIATED',
                                                'order_return' => date('Y-m-d H:i:s')
                                            ]);

                    $shipment_req = [
                        'shipment_id' => [$awb_detail['shipment_id']]
                    ];
                    $pickup_request_data = [
                        'shipment_req'       => $shipment_req,
                        'ship_url'           => $shiprocket_url,
                        'shiprockettoken'    => $shiprockettoken['token'],
                        'order_id'           => $order_data['id'],
                        'request_file_path'  => 'public/order/'.$order_data['id'].'/courier_generate_return_order_pickup_request_'.time().'.json',
                        'response_file_path' => 'public/order/'.$order_data['id'].'/courier_generate_return_order_pickup_response_'.time().'.json',
                    ];
                    $pickup_response_data = $this->pickupRequest($pickup_request_data);
                    
                    //dispatch(new ReturnOrderXMLJob($xml_files_generate));

                    //ReturnOrders::where('shopify_order_id',$shopify_order_id)->where('sku',$sku)->update(['is_approved'=>1]);
                    //return response()->json(['success'=>true]);
                }
            }
       // }

        $return_order_create_data = dispatch(new SAPOrderReturnXMLJob($shopify_order_id));
    }
}