<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Storage;

class SingleOrderXMLReadJob 
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public $file_name;
    public function __construct($file_name)
    {
        //
        $this->file_name = $file_name;
    }

    /**
     * Execute the job.
     *
     * @return void
     */

    public function handle()
    {
        //\
        // /$order_data = [];
        $order_data = array();
        try{
            $single_invoice_file = $this->file_name;
            $filename = last(explode('\\', $single_invoice_file));
            $final_file_name = last(explode('/', $filename));
            $file_path = storage_path().'/app/order/cancel_order/store_cancel/';
            $file = $filename;
            // dd($final_file_name);
            $xml_order_file = file_get_contents($file);
            $xml_object = simplexml_load_string($xml_order_file);
            $xml_file_array = json_decode(json_encode($xml_object),true);
            
            if(!empty($xml_file_array)){
                $main_item_data = [];
                $order_data= $xml_file_array['ITEMSTATUS'];

                $order_no = $xml_file_array['ORDERNO'];

                if(!isset($order_data['ITEMID'])){
                    foreach($order_data as $order_key => $order_value){
                        $main_item_data[] = $order_value;
                    }
                } else {
                    $main_item_data[] = $order_data;
                }
               // dd($main_item_data);
                // if(!empty($main_item_data)){

                //     foreach($order_data as $order_key => $order_detail){
                //         //dd($order_detail);
                //     }
                // }
                
                $content = View('sap/store_cancel_order_xml',['order_no'=>$order_no,'item_data'=>$main_item_data])->render();
                // dd($content);
                // $exists = $content->storeAs($file_path, $final_file_name);
                $exists = Storage::disk('local')->put('order/cancel_order/store_cancel/archive/'.$final_file_name,$content);
                if($exists){
                    unlink($file_path.$final_file_name);
                }
            }
        } catch(Exception $e){

        }
    }
}
