<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Jobs\SaveProductDataJob;

//class SaveProductTwoFiveZeroJob implements ShouldQueue
class SaveProductTwoFiveZeroJob
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public $response;  
    public function __construct($response)
    {
        $this->response = $response;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $response = $this->response;
        
        if(isset($response['products']))
        {  
            foreach ($response['products'] as $product_key => $product_value) {
                $product_data = [
                    'product_value' => $product_value,
                ];
                dispatch(new SaveProductDataJob($product_data));
            }
        }
    }
}
