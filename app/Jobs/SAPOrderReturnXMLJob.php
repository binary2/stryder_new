<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Http\Traits\OrderXmlFileCreateTrait;
use App\Http\Traits\ShopifyCurlTrait;
use App\Models\OrderStoreAssign;
use Illuminate\Support\Facades\Storage;
use Config;

// class SAPOrderReturnXMLJob implements ShouldQueue
class SAPOrderReturnXMLJob
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    use ShopifyCurlTrait;
    use OrderXmlFileCreateTrait;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public $shopify_order_id;
    public function __construct($shopify_order_id)
    {
        $this->shopify_order_id = $shopify_order_id;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $shopify_order_id = $this->shopify_order_id;
        $order_no = OrderStoreAssign::select('order_number')->where('shopify_order_id',$shopify_order_id)->first();
         $order_items_query = OrderStoreAssign::select('shopify_item_id','sap_id','store_code','awb_number','return_awb_number','return_remark','sap_pack_id')
                                        ->where('shopify_order_id',$shopify_order_id)
                                        ->where('store_code','!=',null)
                                        ->where('is_shopify_return', '1');
                                        

        $order_items = $order_items_query->get()->toArray();
       // $random_number = rand(1000000000,9999999999);
        
        
        $content = View('sap/shopify_order_return_xml',[
                        
                        'order_no' => $order_no,
                        'order_items' => $order_items,
                    //    'random_number' => $random_number

                    ])->render();
        $date_time=date("ymdHi");
        $file_name = $order_no['order_number'].'_'.$date_time;
        // dd($file_name);
        $if_present_out_side=Storage::disk('sftp')->put('return/'.$file_name.'.xml',$content);
        // dd($if_present_out_side);
        $update_data = $order_items_query->update(['is_shopify_return'=>'2']);
        //$if_present_in_archive=Storage::disk('sftp')->exists('return/archive_return/'.$file_name);
                
                
    }
}
