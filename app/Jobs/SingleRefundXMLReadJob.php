<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Models\OrderStoreAssign;
use App\Models\OrderItem;
use App\Models\ShiprocketTokenLog;
use Storage,File;

// class SingleRefundXMLReadJob implements ShouldQueue
class SingleRefundXMLReadJob 
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public $file_name;
    public $location_id;
    public $ship_url;

    public function __construct($file_name,$location_id,$ship_url)
    {
        $this->file_name = $file_name;
        $this->location_id = $location_id;
        $this->ship_url = $ship_url;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $single_invoice_file = $this->file_name;
        $location_id = $this->location_id;
        $ship_url = $this->ship_url;

        $shiprockettoken = ShiprocketTokenLog::orderBy('id', 'Desc')->first();
        $filename = last(explode('\\', $single_invoice_file));
        $final_file_name = last(explode('/', $filename));
        $file = $filename;

        try{

            $xml_refund_file = Storage::disk('sftp')->get($file);
            $xml_object = simplexml_load_string($xml_refund_file);
            $xml_file_array = json_decode(json_encode($xml_object),true);

            if(!empty($xml_file_array)){

                $order_id = $xml_file_array['ORDERNO'];
                $item_status[] = $xml_file_array['REFSTATUS'];
                
                foreach ($item_status as $key => $single_item_status_1) {
                    
                    if($single_item_status_1['STATUSCODE'] == '12'){

                        $update_data = OrderStoreAssign::where('sap_id',$single_item_status_1['REFID'])->where('is_refund',null)->where('shopify_item_id',$single_item_status_1['ITEMID'])->where('store_code',$single_item_status_1['PLANT'])->first();
                        if(isset($update_data)){

                            $refund_date = $single_item_status_1["REFUNDDATE"].' '.$single_item_status_1["REFUNDTIME"];
                            
                            $update_data->update(['is_refund'=>'1','refund_amount'=>$single_item_status_1['REFUNDAMT'],'refund_date'=>$refund_date]);
                        }
                    }
                }

                // $date_time=date("Y_m_d_H_i");
                // $file_name = $order_id. '_' .$date_time;

                // $if_present_out_side=Storage::disk('local')->exists('update_refund/'.$file_name);
                // $if_present_in_archive=Storage::disk('local')->exists('archive_refund/'.$file_name);
                
                // if($if_present_out_side == false && $if_present_in_archive == false)
                // {
                //     $exists = Storage::disk('local')->put('update_refund/'.$file_name.'.xml',$content);
                // }
                

                $folder_get_server_path_refundxml_archive = '/refund/Archive/'.$final_file_name;
                $invoice_archive=Storage::disk('sftp')->move($file, $folder_get_server_path_refundxml_archive);
            }
        }catch(Exception $e){

        }
    }
}
