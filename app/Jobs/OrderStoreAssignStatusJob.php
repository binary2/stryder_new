<?php
namespace App\Jobs;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Models\OrderStoreAssignStatus;
use App\Models\OrderStoreAssign;

class OrderStoreAssignStatusJob
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $test_store_assign;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($test_store_assign)
    {
        $this->test_store_assign = $test_store_assign;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $test_store_assign = $this->test_store_assign;
        $note = '';

        $order_store_assign_table = $test_store_assign['order_store_assign_table'];
        $order_store_assign_status_table = $test_store_assign['order_store_assign_status_table'];

        $order_store_assign_data = new OrderStoreAssign;
        $order_store_assign_data->setTable($order_store_assign_table);
        $order_store_assign = OrderStoreAssign::where(['shopify_order_id'=>$test_store_assign['shopify_order_id'],'shopify_item_id'=>$test_store_assign['shopify_item_id']])
                            ->first();
        if($test_store_assign['status'] == 'reassign'){
            
            $note = $test_store_assign['status'].'- old_store_value('.$order_store_assign['store_code'].')';
        }
        
        $order_store_assign_status = new OrderStoreAssignStatus;
        $order_store_assign_status->setTable($order_store_assign_status_table);
        $order_store_assign_status->order_store_assign_id = $test_store_assign['order_store_assign_id'];
        $order_store_assign_status->shopify_order_id = $test_store_assign['shopify_order_id'];
        $order_store_assign_status->shopify_item_id =$test_store_assign['shopify_item_id'];
        $order_store_assign_status->sku = $test_store_assign['sku'];
        $order_store_assign_status->store_code = $test_store_assign['store_code'];
        $order_store_assign_status->quantity =$test_store_assign['quantity'];
        $order_store_assign_status->status = $test_store_assign['status'];
        $order_store_assign_status->note = $note;
        $order_store_assign_status->save();
        
        return;
    }
}
