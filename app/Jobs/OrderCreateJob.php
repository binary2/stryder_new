<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Models\Order;
use App\Models\OrderItem;
use App\Models\OrderStoreAssign;
use App\Models\Pincode;
use Storage;
use App\Jobs\SapXmlCreateJob;
use App\Jobs\SAPShiprocketOrderJob;
use App\Http\Traits\NewFetchStoreCodeTrait;
use Config,Queue;
use App\Http\Traits\ShopifyCurlTrait;
use App\Models\State;
use Log;
use App\Http\Traits\SendMailTrait;

//class OrderCreateJob implements ShouldQueue
 class OrderCreateJob
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels,NewFetchStoreCodeTrait;
    use ShopifyCurlTrait;
    use SendMailTrait;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public $order_api_data;

    public function __construct($order_api_data)
    {
        $this->order_api_data = $order_api_data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $order_api_data = $this->order_api_data;
        $order_count_data = Order::where('shopify_order_id',(string)$order_api_data['id'])->count();
        $pincode = $order_api_data['shipping_address']['zip'];
        $state = $order_api_data['shipping_address']['province'];
        //$discount_code = $order_api_data['discount_applications'];
        if(isset($order_api_data['discount_applications'][0])){
            //$discount_code = $order_api_data['discount_applications'][0]['code'];
            $discount_code = NULL;
        }else{
            $discount_code = NULL;
        }
        if($order_count_data == 0){
            $order_detail = new Order;
            $order_detail->shopify_order_id = $order_api_data['id'];
            $order_detail->customer_name = $order_api_data['customer']['default_address']['name'];
            $order_detail->customer_email = $order_api_data['email'];
            if(isset($order_api_data['shipping_address'])){
                $order_detail->phone_number = str_replace(' ','',$order_api_data['shipping_address']['phone']);
            }
            $order_detail->note = $order_api_data['note'];
            $order_detail->total_price = $order_api_data['total_price'];
            $order_detail->subtotal_price = $order_api_data['subtotal_price'];
            $order_detail->total_tax = $order_api_data['total_tax'];
            $order_detail->financial_status = $order_api_data['financial_status'];
            $order_detail->total_discounts = $order_api_data['total_discounts'];
            $order_detail->discount_code = $discount_code;
            $order_detail->shipping_charge = 0;
            $order_detail->name = $order_api_data['name'];
            $order_detail->fulfillment_status = $order_api_data['fulfillment_status'];
            $order_detail->order_number = $order_api_data['order_number'];
            $order_detail->save();
        }else{
            $order_detail = Order::where('shopify_order_id',(string)$order_api_data['id'])->first();
        }
        Storage::put('public/order/'.$order_api_data['id'].'/order.json',json_encode($order_api_data),'public');

        $trnsaction_url= "/admin/orders/".$order_api_data['id']."/transactions.json";

        $response_trnsaction=$this->getCurl($trnsaction_url);
    
        Storage::put('public/order/'.$order_api_data['id'].'/transactions.json',json_encode($response_trnsaction),'public');


        foreach ($order_api_data['line_items'] as $order_variant_key => $order_variant_value){
            
            // $discount_array = $order_variant_value['discount_allocations'];
            // $discount = array_sum(array_column($discount_array,'amount'));

            $discount = 0.00;
            if(isset($order_variant_value['discount_allocations'])){
                if(count($order_variant_value['discount_allocations']) > 0){
                    foreach ($order_variant_value['discount_allocations'] as $dis_key => $dis_value) {
                        $discount += $dis_value['amount'];
                    }
                }
            }

            $order_variant_data = new OrderItem;
            $order_variant_data->order_id = $order_detail['id'];
            $order_variant_data->shopify_item_id =  (string)$order_variant_value['id'];    
            $order_variant_data->shopify_order_id = (string)$order_api_data['id'];
            //$order_variant_data->name = $order_api_data['name'];
            $order_variant_data->variant_id = (string)$order_variant_value['variant_id'];
            $order_variant_data->title = $order_variant_value['title'];
            $order_variant_data->quantity = $order_variant_value['quantity'];
            $order_variant_data->sku = $order_variant_value['sku'];
            $order_variant_data->variant_title = $order_variant_value['variant_title'];
            $order_variant_data->fulfillment_service = $order_variant_value['fulfillment_service'];
            $order_variant_data->product_id = (string)$order_variant_value['product_id'];
            $order_variant_data->gift_card = $order_variant_value['gift_card'];
            $order_variant_data->price = $order_variant_value['price'];
            $order_variant_data->unit_price = $order_variant_value['price'];

            $order_variant_data->compare_price = 0;
            $order_variant_data->fulfillment_status = $order_variant_value['fulfillment_status'];
            $order_variant_data->total_discounts = $discount;
            $order_variant_data->discount_code = $discount_code;
            $order_variant_data->state = $state;
            $order_variant_data->save();

            $checked_data = $this->checkStoreAssignCount($order_api_data['id'],$order_variant_value['sku']);
            $warehouse_code =  STORE_CODE;

            $new_pincode_arr = ['pincode_val'=>$state,'sku'=>$order_variant_value['sku'],'quantity'=>$order_variant_value['quantity']];
            $all_warehouse_code = $this->checkALlInventory($new_pincode_arr);
            \Log::alert("remaining_save_qty".$checked_data['remaining_save_qty']);
            if($all_warehouse_code != null){

                \Log::alert("checkALlInventory store code-order id ".$all_warehouse_code.'-'.$order_api_data['id']);

                if($checked_data['remaining_save_qty'] > 0){
                    for ($i=1; $i <= $checked_data['remaining_save_qty']; $i++) {
                        $pincode_arr = ['pincode_val'=>$state,'sku'=>$order_variant_value['sku'],'quantity'=>1];
                       
                        $save_detail = new OrderStoreAssign;
                        //$save_detail->setTable($order_store_assign_table);
                        $save_detail->sku = $order_variant_value['sku'];                        
                        $save_detail->order_number = $order_api_data['order_number'];
                        $save_detail->order_received = date('Y-m-d H:i:s');
                        $save_detail->quantity = 1;
                        $save_detail->total_quantity = $order_variant_value['quantity'];
                        $save_detail->shopify_item_id = $order_variant_value['id'];
                        $save_detail->shopify_order_id = $order_api_data['id'];
                        $save_detail->title = $order_variant_value['title'];
                        $save_detail->store_code = $all_warehouse_code;
                        if($all_warehouse_code != null || $all_warehouse_code != ""){
                            $save_detail->shiprocket_id = $order_api_data['order_number'].'_'.$all_warehouse_code;
                        } 
                        //$save_detail->store_code = $warehouse_code;
                        $save_detail->is_order_generate = 0;
                        $save_detail->save();
                        $sap_id=intval($save_detail['order_store_id']);
                        $main_sap_id = '1'.str_pad($sap_id,9,'0',STR_PAD_LEFT);
                        $save_detail_update = OrderStoreAssign::where('order_store_id',$save_detail['order_store_id'])->update(['sap_id'=>$main_sap_id]);
                        $store_code[] =  $save_detail['store_code'];
                    }
                }
            }else{
                if($checked_data['remaining_save_qty'] > 0){
                    for ($i=1; $i <= $checked_data['remaining_save_qty']; $i++) {
                        $pincode_arr = ['pincode_val'=>$state,'sku'=>$order_variant_value['sku'],'quantity'=>1];
                        $warehouse_code = $this->checkState($pincode_arr);

                        \Log::alert("checkState store code-order id ".$warehouse_code.'-'.$order_api_data['id']);

                        if($warehouse_code != "")
                        {
                        $save_detail = new OrderStoreAssign;
                        //$save_detail->setTable($order_store_assign_table);
                        $save_detail->sku = $order_variant_value['sku'];
                        
                        $save_detail->order_number = $order_api_data['order_number'];
                        $save_detail->order_received = date('Y-m-d H:i:s');
                        $save_detail->quantity = 1;
                        $save_detail->total_quantity = $order_variant_value['quantity'];
                        $save_detail->shopify_item_id = $order_variant_value['id'];
                        $save_detail->shopify_order_id = $order_api_data['id'];
                        $save_detail->title = $order_variant_value['title'];
                        $save_detail->store_code = $warehouse_code;
                        if($warehouse_code != null || $warehouse_code != ""){
                            $save_detail->shiprocket_id = $order_api_data['order_number'].'_'.$warehouse_code;
                        } 
                        //$save_detail->store_code = $warehouse_code;
                        $save_detail->is_order_generate = 0;
                        $save_detail->save();
                        $sap_id=intval($save_detail['order_store_id']);
                        $main_sap_id = '1'.str_pad($sap_id,9,'0',STR_PAD_LEFT);
                        $save_detail_update = OrderStoreAssign::where('order_store_id',$save_detail['order_store_id'])->update(['sap_id'=>$main_sap_id]);
                        $store_code[] =  $save_detail['store_code'];
                       }
                    }
                }
            }
            // if($checked_data['remaining_save_qty'] > 0){
            //     for ($i=1; $i <= $checked_data['remaining_save_qty']; $i++) {
            //         $pincode_arr = ['pincode_val'=>$state,'sku'=>$order_variant_value['sku'],'quantity'=>1];
            //         $warehouse_code = $this->checkState($pincode_arr);
            //         $save_detail = new OrderStoreAssign;
            //         //$save_detail->setTable($order_store_assign_table);
            //         $save_detail->sku = $order_variant_value['sku'];
                    
            //         $save_detail->order_number = $order_api_data['order_number'];
            //         $save_detail->order_received = date('Y-m-d H:i:s');
            //         $save_detail->quantity = 1;
            //         $save_detail->total_quantity = $order_variant_value['quantity'];
            //         $save_detail->shopify_item_id = $order_variant_value['id'];
            //         $save_detail->shopify_order_id = $order_api_data['id'];
            //         $save_detail->title = $order_variant_value['title'];
            //         $save_detail->store_code = $warehouse_code;
            //         if($warehouse_code != null || $warehouse_code != ""){
            //             $save_detail->shiprocket_id = $order_api_data['order_number'].'_'.$warehouse_code;
            //         } 
            //         //$save_detail->store_code = $warehouse_code;
            //         $save_detail->is_order_generate = 0;
            //         $save_detail->save();
            //         $sap_id=intval($save_detail['order_store_id']);
            //         $main_sap_id = '1'.str_pad($sap_id,9,'0',STR_PAD_LEFT);
            //         $save_detail_update = OrderStoreAssign::where('order_store_id',$save_detail['order_store_id'])->update(['sap_id'=>$main_sap_id]);
            //         $store_code[] =  $save_detail['store_code'];
            //     }
            // }
        }

        if(!empty($store_code))
        {
        $unique_store_code = array_unique($store_code);
        $shiprocket_url = SHIPROCKET_API_URL;
        
        $sap_shiprocket_order_job = [
            'order_id' => $order_api_data['id'],
            'store_code' => $unique_store_code,
            'shiprocket_url' => $shiprocket_url,
        ];

        //dd($order_create_data);
        
        \Log::alert("Data passed in SapXmlCreateJob : ".json_encode($sap_shiprocket_order_job));
        $order_create_data = dispatch(new SapXmlCreateJob($sap_shiprocket_order_job));
        }
        else
        {
            // $email = ['binarynotifications@gmail.com','minakshi@binaryic.in','rushik@binaryic.in','saumil.binary@gmail.com'];
            // $subject = 'Inventory sync error of stryder.com '.date('Y-m-d H:i:s');
            // $email_view = "Create Order Error";
            // $email_data = "Error in Order Creation in ".$order_api_data['id'];

            $to = ['binarynotifications@gmail.com'];
            $email = ['minakshi@binaryic.in','rushik@binaryic.in','saumil.binary@gmail.com'];
            $subject = 'Inventory sync error of stryder.com '.date('Y-m-d H:i:s');
            $email_data = "Error in Order Creation in ".$order_api_data['id'];
            $from = 'stryder.noreply@tatainternational.com';
            $this->sendmail($to,$email,$subject,$email_data,$from);

            // $this->SendMail($email_view,$subject,$email_data,$email);

            \Log::alert("Store Code Not Found for shopify order id ".$order_api_data['id']);
        }
            
    }
}
