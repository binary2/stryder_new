<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Storage;

class CancelOrderSapXmlCreateJob 
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //
            // $content = View('sap/order_xml',[
            //                     //'orderRefNo' => $orderRefNo,
            //                     'order_data' => $order_data,
            //                     'order_transaction' => $success_transaction,
            //                     'order_items' => $order_items,
            //                     'state_code' => $state_code,
            //                     'transport_mode' => $transport_mode,
            //                     'courier_array' => $courier_array,
            //                     'shipping_charge' => $shipping_charge,
            //                     //'line_item_tax_line' => $line_item_tax_line,
            //                     'SAPOrderNo' => $SAPOrderNo,
            //                     'order_total_price'=>$total_price,
            //                 ])->render();
            $content = View('sap/cancel_order_xml')->render();
            $file_name = '123456789'. '_' . time();
            $exists = Storage::disk('local')->put('order/cancel_order/'.$file_name.'.xml',$content);
    }
}
