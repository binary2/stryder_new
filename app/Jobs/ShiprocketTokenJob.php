<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Models\ShiprocketTokenLog;

//class ShiprocketTokenJob
class ShiprocketTokenJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    protected $token_array;
    public function __construct($token_array)
    {
        $this->token_array = $token_array;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $token_array = $this->token_array;

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => $token_array['ship_url'] . "/auth/login?",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => $token_array['creadential_array'],
        ));

        $response = curl_exec($curl);

        curl_close($curl);

        $response = json_decode($response);

        /**
         * undocumented constant
         **/

        // save token to database
        $token_save = new ShiprocketTokenLog();
        $token_save->token = $response->token;
        $token_save->save();
    }
}
