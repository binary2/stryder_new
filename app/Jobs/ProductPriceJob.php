<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Models\Product;
use App\Models\ProductPrice;
use App\Http\Traits\ShopifyCurlTrait;

class ProductPriceJob /*implements ShouldQueue*/
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    use ShopifyCurlTrait;

    /**
     * Create a new job instance.
     *
     * @return void
     */

    protected $productsData;
    public function __construct($productsData)
    {
        //
        $this->productsData = $productsData;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //
        $product_data = $this->productsData;
        try{
            $id = $product_data['id'];

            $sku = $product_data['sku'];
            $variant_id = Product::where('variant_sku',$sku)->first();
            if(!empty($variant_id)){
                $variant_id = $variant_id['shopify_variant_id'];

                $url = '/admin/api/2019-10/variants/'.$variant_id.'.json';
                $variant = [
                    'variant'=> [
                        'id' => $variant_id,
                        'price' => $product_data['price'],
                        'compare_at_price' => $product_data['selling_price'],
                    ]
                ];

                \Log::info('Price update request variant payload', $variant);
                $type = 'Westside';
                $main_response = $this->putCurl($url,$variant);

                $result_arr = json_decode(json_encode($main_response['body']), true);
                ProductPrice::where('id',$id)->update(['is_update'=>'1']);
            }else{
                ProductPrice::where('id',$id)->update(['is_update'=>'2']);
            }
        }catch(Exception $e){

            Log::error('ProductPriceJob Job :- ' .json_encode($e));
            //ProductPrice::where('id',$product_data['id'])->update(['is_update'=>'2']);
        }
    }
}
