<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Models\ShiprocketTokenLog;
use App\Jobs\SingleRefundXMLReadJob;
use Storage;
use File;
// class RefundXMLReadJob implements ShouldQueue
class RefundXMLReadJob
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($xml_array)
    {
        $this->xml_array = $xml_array;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $xml_array = $this->xml_array;
        $ship_url = $xml_array['shiprocket_url'];
        $location_id = $xml_array['location_id'];
        $folder_get_server_path = '/refund/';
        $all_refund_file = Storage::disk('sftp')->files($folder_get_server_path);
        
        $shiprockettoken = ShiprocketTokenLog::orderBy('id', 'Desc')->first();
        foreach ($all_refund_file as $all_refund_file_key => $single_refund_file){

            dispatch(new SingleRefundXMLReadJob($single_refund_file,$location_id,$ship_url));
        }

        return response()->json(['sucssess'=>true]);
    }
}
