<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Models\Product;

//class SaveProductDataJob implements ShouldQueue
class SaveProductDataJob
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public $product_value;
    public function __construct($product_value)
    {
        $this->product_value = $product_value;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $products = $this->product_value;
    
        $product_value = $products['product_value'];

        \Storage::put('public/products/'.$product_value['id'].'.json',json_encode($product_value));

        $shopify_product_id = (string)$product_value['id'];
        
        foreach ($product_value['variants'] as $product_variant_key => $product_variant_value) 
        {
            $shopify_variant_id = (string)$product_variant_value['id'];
            $variant_data = [
                'shopify_product_id' => $shopify_product_id,
                'title'              => $product_value['title'],
                // 'body_html'          => $product_value['body_html'],
                'vendor'             => $product_value['vendor'],
                'product_type'       => $product_value['product_type'],
                'handle'             => $product_value['handle'],
                'tags'               => $product_value['tags'],
                'published_scope'    => $product_value['published_scope'],
                'shopify_variant_id' => $shopify_variant_id,
                'variant_title' => $product_variant_value['title'],
                'variant_price' => $product_variant_value['price'],
                'variant_sku' => $product_variant_value['sku'],
                'variant_position' => $product_variant_value['position'],
                'variant_inventory_policy' => $product_variant_value['inventory_policy'],
                'variant_compare_at_price' => $product_variant_value['compare_at_price'],
                'variant_fulfillment_service' => $product_variant_value['fulfillment_service'],
                'variant_inventory_management' => $product_variant_value['inventory_management'],
                'variant_taxable' => $product_variant_value['taxable'],
                'variant_barcode' => $product_variant_value['barcode'],
                'variant_inventory_item_id' => $product_variant_value['inventory_item_id'],
                'variant_inventory_quantity' => $product_variant_value['inventory_quantity'],
                // 'variant_old_inventory_quantity' => $product_variant_value['old_inventory_quantity'],
                'variant_requires_shipping' => $product_variant_value['requires_shipping'],
                // 'single_variant_json'  => $product_variant_value,
                // 'product_json' => $product_value
            ];

            $product_variant_data = Product::firstOrNew(['shopify_variant_id'=>$shopify_variant_id]);
        
            $product_variant_data->fill($variant_data);
            $product_variant_data->save();
        }
    }
}
