<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Models\OrderStoreAssign;
use App\Models\ShiprocketTokenLog;
use Storage;
use File;
use App\Jobs\SingleInvoiceXMLReadJob;

class InvoiceXMLReadJob 
// class InvoiceXMLReadJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($xml_array)
    {
        $this->xml_array = $xml_array;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $xml_array = $this->xml_array;
        $ship_url = $xml_array['shiprocket_url'];
        $location_id = $xml_array['location_id'];
        $folder_get_server_path = '/invoicexml/';
        $all_invoice_file = Storage::disk('sftp')->files($folder_get_server_path);
       
        $shiprockettoken = ShiprocketTokenLog::orderBy('id', 'Desc')->first();
        foreach ($all_invoice_file as $all_invoice_file_key => $single_invoice_file) 
        {
            // $filename = last(explode('\\', $single_invoice_file));
            // $final_file_name = last(explode('/', $filename));
            // $file = $filename;
            // $xml_invoice_file = file_get_contents($xml_invoice_file_path.$file);
            // $xml_object = simplexml_load_string($xml_invoice_file);
            // $xml_file_array = json_decode(json_encode($xml_object),true);
            dispatch(new SingleInvoiceXMLReadJob($single_invoice_file,$location_id,$ship_url));
        }
        
        return response()->json(['sucssess'=>true]);
                                                        
    }
}
