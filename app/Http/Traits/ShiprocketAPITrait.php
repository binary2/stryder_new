<?php

namespace App\Http\Traits;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;
use Guzzle\Http\Exception\ClientErrorResponseException;
use App\Models\Shop;
use Illuminate\Support\Facades\Storage;

trait ShiprocketAPITrait {

	public function pickupRequest($data)
    {
        $shipment_req_json = json_encode($data['shipment_req']);
        Storage::put($data['request_file_path'],$shipment_req_json,'public');
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $data['ship_url'] . "/courier/generate/pickup",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => $shipment_req_json,
            CURLOPT_HTTPHEADER => array(
                "authorization: Bearer ".$data['shiprockettoken'],
                "content-type: application/json",
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);
        if ($err) {
            \Log::info("cURL Error For pickupRequest Order Number:" . $data['order_id']);
            \Log::info("cURL Error For pickupRequest :" . $err);
        }
        curl_close($curl);
        Storage::put($data['response_file_path'],$response,'public');
        return json_decode($response,1);
    }
}