<?php

namespace App\Http\Traits;

use Illuminate\Support\Facades\Mail;
// use Mail;
use Carbon\Carbon;
// use App\Helpers\MailHelp;
use Log;

trait SendMailTrait
{
    // public function sendmail($to, $cc, $subject, $msg)
    // {
    // 	$subject = $subject . ' : ' . Carbon::now()->format('d-m-Y H:i:s A');
    // 	$msg = $msg . '<br><br><br>Automated emails generated automatically for your convenience.<br><br>Thank you<br><b>STRYDER CYCLE PRIVATE LIMITED</b><br>If you have any questions, get in touch with us<br>Toll free number : 1800 123 2660 •Email: customer.care@tatainternational.com.';
    // 	Mail::html($msg, function ($message) use ($to, $cc, $subject) {
    // 		$message->to($to)->cc($cc)->subject($subject);
    // 	});

    // 	if (Mail::flushMacros() != null) {
    // 		if (count(Mail::flushMacros()) > 0) {
    // 			Log::error('Error in sending mail');
    // 		}
    // 	} else {
    // 		Log::info('Sent Mail Success');
    // 	}
    // }

    // public function sendmail($to, $cc, $subject, $msg, $attach = null){
    //     // dd('in function');
    //     $subject = $subject. ' : '.Carbon::now()->format('d-m-Y H:i:s A');
    //     $msg = $msg. '<br><br><br>Automated emails generated automatically for your convenience.<br><br>Thank you<br><b style="color: green;">BINARY WEB SOLUTIONS INDIA PVT LTD</b><br><b>Vision :</b> To Identify Potential of an organisation and Empower their purpose by Intuition, Innovation and Implementation.';
    //     Mail::html($msg, function ($message) use ($to, $cc, $subject, $attach){
    //             $message->to($to)->cc($cc)->subject( $subject );
    //             if ($attach) {
    //                 $message->attach($attach);
    //             }

    //     });
    //     if( count(Mail::failures()) > 0 ) {
    //        Log::error('Error in sending mail');
    //     } else {
    //         Log::info('Sent Mail Success');
    //     }
    // }

    public function sendMail($to, $cc, $subject, $msg, $from)
    {
        $subject = $subject . ' : ' . Carbon::now()->format('d-m-Y H:i:s A');
        $msg = $msg . '<br><br><br>Automated emails generated automatically for your convenience.<br><br>Thank you<br><b>STRYDER CYCLE PRIVATE LIMITED</b><br>If you have any questions, get in touch with us<br>Toll free number: 1800 123 2660 • Email: customer.care@tatainternational.com.';

        Mail::send([], [], function ($message) use ($to, $cc, $subject, $msg, $from) {
            $message->from($from);
            $message->to($to)->cc($cc)->subject($subject);
            $message->setBody($msg, 'text/html');
        });

        if (count(Mail::failures()) > 0) {
            Log::error('Error in sending mail');
        } else {
            Log::info('Sent Mail Success');
        }
    }

    public function SendMailTrait($email_view, $subject, $email_data, $email, $bcc = null)
    {
        Mail::send($email_view, ['email_data' => $email_data], function ($message) use ($email, $subject, $email_data, $email_view, $bcc) {

            $message->to($email);
            if (!empty($bcc)) {
                $message->bcc($bcc);
            }
            $message->subject($subject);
        });
    }
}
