<?php

namespace App\Http\Traits;
//use App\Models\InventoryManagement;
//use App\Models\StoreMaster;
use App\Models\Pincode;
use App\Models\OrderStoreAssign;
use App\Models\OrderItem;
use App\Models\Order;
use App\Models\Warehouse;
use App\Models\InventoryManagement;
use App\Models\State;
use DB,Storage;
//use App\Jobs\OrderStoreAssignStatusJob;
use Mail;

trait NewFetchStoreCodeTrait {

     public function checkStoreAssignCount($shopify_order_id,$sku){
        $get_order_detail = Storage::get('public/order/' . $shopify_order_id . '/order.json');
        $final_order_data = json_decode($get_order_detail,1);
        $order_total_qty = array_sum(array_column($final_order_data['line_items'], 'quantity'));

        $sku_data = collect($final_order_data['line_items'])->where('sku',$sku)->first();
        $sku_quantity = $sku_data['quantity'];

        $order_store_assign_model = new OrderStoreAssign;
       // $order_store_assign_model->setTable($order_store_assign_table);
        $saved_qty_data_query = $order_store_assign_model->select('shopify_order_id','quantity')
                                                ->where('sku',$sku)
                                                ->where('shopify_order_id',$shopify_order_id);
        $saved_qty_data_count = $saved_qty_data_query->count();
        $saved_qty = 0;
        if($saved_qty_data_count > 0){
            $saved_qty_data = $saved_qty_data_query->get()->toArray();
            $saved_qty = array_sum(array_column($saved_qty_data, 'quantity'));
        }

        return [
            'order_total_qty' => $order_total_qty,
            'saved_qty' => $saved_qty,
            'sku_quantity' => $sku_quantity,
            'remaining_save_qty' => $sku_quantity - $saved_qty
        ];
    }

    public function getLatesetBinaryId()
    {
        $order_store_assign_model = new OrderStoreAssign;
        //$order_store_assign_model->setTable($order_store_assign_table);
        $data = 1 + $order_store_assign_model->max('binary_int_id');
        $binary_id = '1'.str_pad($data,9,'0',STR_PAD_LEFT);
       
        
        return [
            'binary_id' => $binary_id,
            'binary_int_id' => $data
        ];
    }

    public function checkPincode($pincode_arr){
        $pincode = $pincode_arr['pincode_val'];
        $sku = $pincode_arr['sku'];
        $qty = $pincode_arr['quantity'];
        $all_warehouse_code = Pincode::where('pincode',$pincode)->get()->toArray();
       // $priority_1 = $all_warehouse_code[0]['warehouse_code_1'];
       // $priority_2 = $all_warehouse_code[0]['warehouse_code_2'];
       // $priority_3 = $all_warehouse_code[0]['warehouse_code_3'];
        
        $warehouse_code='';
        if ($all_warehouse_code != '') {


            $priority_1 = $all_warehouse_code[0]['warehouse_code_1'];
            $priority_2 = $all_warehouse_code[0]['warehouse_code_2'];
            $priority_3 = $all_warehouse_code[0]['warehouse_code_3'];

            $warehouse_qty_priority_1 = InventoryManagement::select('store_code','quantity')->where('sku',$sku)->where('store_code',$priority_1)->first();
            $warehouse_qty_priority_2 = InventoryManagement::select('store_code','quantity')->where('sku',$sku)->where('store_code',$priority_2)->first();
            $warehouse_qty_priority_3 = InventoryManagement::select('store_code','quantity')->where('sku',$sku)->where('store_code',$priority_3)->first();
            if(isset($warehouse_qty_priority_1) && $warehouse_qty_priority_1['quantity'] != 0 && $warehouse_qty_priority_1['quantity'] > 0)
            {
                $warehouse_code = $warehouse_qty_priority_1['store_code'];
                $update_quantity =  $warehouse_qty_priority_1['quantity'] - $qty;
                InventoryManagement::where('sku',$sku)->where('store_code',$warehouse_code)->update(['quantity'=>$update_quantity]);

            }elseif(isset($warehouse_qty_priority_2) && $warehouse_qty_priority_2['quantity'] != 0 && $warehouse_qty_priority_2['quantity'] > 0){
                $warehouse_code = $warehouse_qty_priority_2['store_code'];
                $update_quantity =  $warehouse_qty_priority_2['quantity'] - $qty;
                InventoryManagement::where('sku',$sku)->where('store_code',$warehouse_code)->update(['quantity'=>$update_quantity]);
                
            }elseif(isset($warehouse_qty_priority_3) && $warehouse_qty_priority_3['quantity'] != 0 && $warehouse_qty_priority_3['quantity'] > 0){
                $warehouse_code = $warehouse_qty_priority_3['store_code'];
                $update_quantity =  $warehouse_qty_priority_3['quantity'] - $qty;
                InventoryManagement::where('sku',$sku)->where('store_code',$warehouse_code)->update(['quantity'=>$update_quantity]);
            }

        }
        return $warehouse_code;
    }

    public function checkALlInventory($pincode_arr)
    {
        $pincode = $pincode_arr['pincode_val'];
        $sku = $pincode_arr['sku'];
        $qty = $pincode_arr['quantity'];

        $all_warehouse_code = State::where('destination',$pincode)->get()->toArray();
        //$priority_1 = $all_warehouse_code[0]['priority_1'];
        //$priority_2 = $all_warehouse_code[0]['priority_2'];
        //$priority_3 = $all_warehouse_code[0]['priority_3'];
        $warehouse_code='';
        if ($all_warehouse_code != '') {

            $priority_1 = $all_warehouse_code[0]['priority_1'];
            $priority_2 = $all_warehouse_code[0]['priority_2'];
            $priority_3 = $all_warehouse_code[0]['priority_3'];

            $warehouse_qty_priority_1 = InventoryManagement::select('store_code','quantity')->where('sku',$sku)->where('store_code',$priority_1)->first();
            $warehouse_qty_priority_2 = InventoryManagement::select('store_code','quantity')->where('sku',$sku)->where('store_code',$priority_2)->first();
            
            $warehouse_qty_priority_3 = InventoryManagement::select('store_code','quantity')->where('sku',$sku)->where('store_code',$priority_3)->first();
            
            if(isset($warehouse_qty_priority_1) && $warehouse_qty_priority_1['quantity'] != 0 && $warehouse_qty_priority_1['quantity'] >= $qty)
            {
                \Log::notice("warehouse_qty_priority_1 Code ");
                $warehouse_code = $warehouse_qty_priority_1['store_code'];
                $update_quantity =  $warehouse_qty_priority_1['quantity'] - $qty;
                InventoryManagement::where('sku',$sku)->where('store_code',$warehouse_code)->update(['quantity'=>$update_quantity]);

            }elseif(isset($warehouse_qty_priority_2) && $warehouse_qty_priority_2['quantity'] != 0 && $warehouse_qty_priority_2['quantity'] >= $qty){
                \Log::notice("warehouse_qty_priority_2 Code ");
                $warehouse_code = $warehouse_qty_priority_2['store_code'];
                $update_quantity =  $warehouse_qty_priority_2['quantity'] - $qty;
                InventoryManagement::where('sku',$sku)->where('store_code',$warehouse_code)->update(['quantity'=>$update_quantity]);
                
            }elseif(isset($warehouse_qty_priority_3) && $warehouse_qty_priority_3['quantity'] != 0 && $warehouse_qty_priority_3['quantity'] >= $qty){
                \Log::notice("warehouse_qty_priority_3 Code ");
                $warehouse_code = $warehouse_qty_priority_3['store_code'];
                $update_quantity =  $warehouse_qty_priority_3['quantity'] - $qty;
                InventoryManagement::where('sku',$sku)->where('store_code',$warehouse_code)->update(['quantity'=>$update_quantity]);
            }

        }
        // dd($warehouse_code);
        \Log::notice("Final Warehouse Code ".$warehouse_code);
        return $warehouse_code;
    }

    public function checkState($pincode_arr){
        $pincode = $pincode_arr['pincode_val'];
        $sku = $pincode_arr['sku'];
        $qty = $pincode_arr['quantity'];

        $all_warehouse_code = State::where('destination',$pincode)->get()->toArray();
        //$priority_1 = $all_warehouse_code[0]['priority_1'];
        //$priority_2 = $all_warehouse_code[0]['priority_2'];
        //$priority_3 = $all_warehouse_code[0]['priority_3'];
        $warehouse_code='';
        if ($all_warehouse_code != '') {

            $priority_1 = $all_warehouse_code[0]['priority_1'];
            $priority_2 = $all_warehouse_code[0]['priority_2'];
            $priority_3 = $all_warehouse_code[0]['priority_3'];

            $warehouse_qty_priority_1 = InventoryManagement::select('store_code','quantity')->where('sku',$sku)->where('store_code',$priority_1)->first();
            $warehouse_qty_priority_2 = InventoryManagement::select('store_code','quantity')->where('sku',$sku)->where('store_code',$priority_2)->first();
            
            $warehouse_qty_priority_3 = InventoryManagement::select('store_code','quantity')->where('sku',$sku)->where('store_code',$priority_3)->first();
            
            if(isset($warehouse_qty_priority_1) && $warehouse_qty_priority_1['quantity'] != 0 && $warehouse_qty_priority_1['quantity'] > 0)
            {
                $warehouse_code = $warehouse_qty_priority_1['store_code'];
                $update_quantity =  $warehouse_qty_priority_1['quantity'] - $qty;
                InventoryManagement::where('sku',$sku)->where('store_code',$warehouse_code)->update(['quantity'=>$update_quantity]);

            }elseif(isset($warehouse_qty_priority_2) && $warehouse_qty_priority_2['quantity'] != 0 && $warehouse_qty_priority_2['quantity'] > 0){
                $warehouse_code = $warehouse_qty_priority_2['store_code'];
                $update_quantity =  $warehouse_qty_priority_2['quantity'] - $qty;
                InventoryManagement::where('sku',$sku)->where('store_code',$warehouse_code)->update(['quantity'=>$update_quantity]);
                
            }elseif(isset($warehouse_qty_priority_3) && $warehouse_qty_priority_3['quantity'] != 0 && $warehouse_qty_priority_3['quantity'] > 0){
                $warehouse_code = $warehouse_qty_priority_3['store_code'];
                $update_quantity =  $warehouse_qty_priority_3['quantity'] - $qty;
                InventoryManagement::where('sku',$sku)->where('store_code',$warehouse_code)->update(['quantity'=>$update_quantity]);
            }

        }
        // dd($warehouse_code);
        return $warehouse_code;
    }

    public function reAssignStore($pincode_arr){
        $pincode = $pincode_arr['pincode_val'];
        $sku = $pincode_arr['sku'];
        $qty = $pincode_arr['quantity'];
        $store_code = $pincode_arr['store_code'];
        $previous_store_code = $pincode_arr['previous_store_code'];
        //dd($store_code);
        $all_warehouse_code = State::where('destination',$pincode)->get()->toArray();
        //$priority_1 = $all_warehouse_code[0]['priority_1'];
        //$priority_2 = $all_warehouse_code[0]['priority_2'];
        //$priority_3 = $all_warehouse_code[0]['priority_3'];
        $warehouse_code='';
        if ($all_warehouse_code != '') {

            $priority_1 = $all_warehouse_code[0]['priority_1'];
            $priority_2 = $all_warehouse_code[0]['priority_2'];
            $priority_3 = $all_warehouse_code[0]['priority_3'];

            $warehouse_qty_priority_1 = InventoryManagement::select('store_code','quantity')->where('sku',$sku)->where('store_code',$priority_1)->first();
       
            $warehouse_qty_priority_2 = InventoryManagement::select('store_code','quantity')->where('sku',$sku)->where('store_code',$priority_2)->first();
           

            $warehouse_qty_priority_3 = InventoryManagement::select('store_code','quantity')->where('sku',$sku)->where('store_code',$priority_3)->first();

             if(isset($warehouse_qty_priority_1) && $warehouse_qty_priority_1['quantity'] != 0 && $warehouse_qty_priority_1['quantity'] > 0 && $warehouse_qty_priority_1['store_code'] != $store_code && $warehouse_qty_priority_1['store_code'] != $previous_store_code)
            {
                $warehouse_code = $warehouse_qty_priority_1['store_code'];
                $update_quantity =  $warehouse_qty_priority_1['quantity'] - $qty;
                InventoryManagement::where('sku',$sku)->where('store_code',$warehouse_code)->update(['quantity'=>$update_quantity]);

            }elseif(isset($warehouse_qty_priority_2) && $warehouse_qty_priority_2['quantity'] != 0 && $warehouse_qty_priority_2['quantity'] > 0 && $warehouse_qty_priority_2['store_code'] != $store_code && $warehouse_qty_priority_2['store_code'] != $previous_store_code){
                $warehouse_code = $warehouse_qty_priority_2['store_code'];
                $update_quantity =  $warehouse_qty_priority_2['quantity'] - $qty;
                InventoryManagement::where('sku',$sku)->where('store_code',$warehouse_code)->update(['quantity'=>$update_quantity]);
                
            }elseif(isset($warehouse_qty_priority_3) && $warehouse_qty_priority_3['quantity'] != 0 && $warehouse_qty_priority_3['quantity'] > 0 && $warehouse_qty_priority_3['store_code'] != $store_code && $warehouse_qty_priority_3['store_code'] != $previous_store_code){
                $warehouse_code = $warehouse_qty_priority_3['store_code'];
                $update_quantity =  $warehouse_qty_priority_3['quantity'] - $qty;
                InventoryManagement::where('sku',$sku)->where('store_code',$warehouse_code)->update(['quantity'=>$update_quantity]);
            }

        }

        return $warehouse_code;
    }
}
