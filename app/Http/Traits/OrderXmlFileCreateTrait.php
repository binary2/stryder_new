<?php
namespace App\Http\Traits;
use Storage;
use App\Models\OrderStoreAssign;

trait OrderXmlFileCreateTrait {
	public function getTotalPrice($shopify_order_id){
        $get_order_detail = Storage::get('public/order/'.$shopify_order_id.'/order.json');
        $order_data = json_decode($get_order_detail,1);

        return $order_data['total_price'];
    }
    
    public function orderLineItemPriceGet($shopify_order_id){
       $get_order_detail = Storage::get('public/order/'.$shopify_order_id.'/order.json');
       $order_data = json_decode($get_order_detail,1);
       $items_price=[];
       foreach ($order_data['line_items'] as $item_key => $single_item) {
        
          $items_price_total=[
              "price"=>$single_item['price'],
              "total_discounts"=>$single_item['total_discount'],
              "quantity"=>$single_item['quantity']
          ];
          
          $items_price[$single_item['sku']]=$items_price_total;
       }
       return $items_price; 
    }
}