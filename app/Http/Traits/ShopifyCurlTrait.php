<?php

namespace App\Http\Traits;

use App\Models\User;

trait ShopifyCurlTrait
{
    public function getCurltest($url){
        $shop_data = User::select('name', 'password')
                                    ->where('name','stryder-bikes-til.myshopify.com')->first();
           
        $response = $shop_data->api()->rest('GET', $url);
        $response = json_encode($response['body']);
        $response = json_decode($response, true);
        return $response;
    }

    public function getCurl($url){

        $shop_data = User::where('name','stryder-bikes-til.myshopify.com')->first();
        $response_data = $shop_data->api()->rest('GET',$url);
        $response = $response_data['body'];
        return $response;  
    }

    public function getCurlWithHeader($url,$params)
    {
        $shop_data = User::where('name','stryder-bikes-til.myshopify.com')->first();
        $response_data = $shop_data->api()->rest('GET',$url, $params);
        if (isset($response_data['response'])) {
            $response_link = $response_data['response']->getHeaders();
            if (isset($response_link['Link']['0'])) {
                $response_link = $response_link['Link']['0'];
            }
        }
        $response = json_decode(json_encode($response_data['body']), true);
    
        return [
            'response_body' => $response,
            'response_link' => $response_link,
        ];
    }

    public function putCurl($url, $data)
    {
        $shop_data = User::select('name', 'password')
                                ->where('name','stryder-bikes-til.myshopify.com')->first();
   
        $response = $shop_data->api()->rest('PUT', $url, $data);
        $response = json_encode($response['body']);
        $response = json_decode($response, true);
        return $response;
    }

     public function postcurl($method,$url,$inventory_array)
    {
        $shop_data = User::where('name','stryder-bikes-til.myshopify.com')->first();
        $response_data = $shop_data->api()->rest($method,$url,$inventory_array);
        $response = json_decode(json_encode($response_data['body']), true);
        return $response;
    }
}