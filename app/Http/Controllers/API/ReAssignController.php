<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Validator;
use App\Models\OrderStoreAssign;
use App\Models\Order;
use App\Models\ShiprocketTokenLog;
use App\Models\ShiprocketToken;
use Storage;
use App\Jobs\SAPShiprocketOrderJob;

class ReAssignController extends Controller
{
    public function lineItemAssignOrder(Request $request)
    {
        \Log::notice(json_encode($request->all()));
       
        $header = $request->header('Authorization');
        \Log::debug('token is =>'.$header);
        $check_token = ShiprocketToken::where('token',$header)->first();
        
        // if($check_token == null){
        //     \Log::debug('token is invalid =>'.$header);
        //     return response()->json(['message'=>'Unauthorized'],401);
        // }

        $reassign_data = $request->all();
        $shiprocket_url = SHIPROCKET_API_URL;
       
        $validator = Validator::make($request->all(), [
            'order_no' => 'required',
            'store_code' => 'required',
            'article' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors()], 401);            
        }

        $name = $reassign_data['order_no'];
        $order_table_name = 'orders';
        $article = explode(',', $reassign_data['article']);
        $type_no = substr($article[0], 0, 1);
        $order_store_assign_table = 'order_store_assign';

        $name = "#" . $name;
        $order_model = new Order;
        $order_model->setTable($order_table_name);
        $order_get = $order_model->where('name',$name)
                            ->first();
        $order_store_assign_model = new OrderStoreAssign;
        $order_store_assign_model->setTable($order_store_assign_table);
        $check_already = $order_store_assign_model->where('sap_id',$article[0])->first()->toArray();
       
        if(empty($check_already['awb_number']))
        {
            $order_json_data_request = json_encode($reassign_data);
            Storage::put('public/order/'.$order_get['shopify_order_id'].'/sap_request_'.$reassign_data['store_code'].'_'.date('YmdHis').'.json',$order_json_data_request,'public');

            $sap_shiprocket_order_job = [
                'order_id' => $order_get['shopify_order_id'],
                'store_code' => $reassign_data['store_code'],
                'sap_id' => $article,
                'shiprocket_url' => $shiprocket_url,
            ];
           
            $order_create_data = $this->dispatch(new SAPShiprocketOrderJob($sap_shiprocket_order_job));
            
            $response_data = [
                'success' => true,
                'message' => $order_create_data['message'],
                'data' => $order_create_data['awb_number'],
                'TransportMode' => $order_create_data['TransportMode'],
                'LogisticsID' => $order_create_data['LogisticsID']
            ];
        }
        else
        {
           $transport_mode_config = Config::get('constant_array.transport_mode');
           $courier_array = Config::get('constant_array.courier');
           $logistic_partner=null;

           if($check_already['is_surface'] == 1){
               if(array_key_exists(strtoupper("SURFACE"),$transport_mode_config)){
                   $transport_mode = $transport_mode_config[strtoupper("SURFACE")];
               }
           }else{
               if(array_key_exists(strtoupper("AIR"),$transport_mode_config)){
                   $transport_mode = $transport_mode_config[strtoupper("AIR")];
               }
           } 

           if(array_key_exists(strtoupper($check_already['courier_partner']),$courier_array)){
               $logistic_partner = $courier_array[strtoupper($check_already['courier_partner'])];
           }
           
           $response_data = [
               'success' => true,
               'message' => "Alreday AWB generated successfully",
               'data' => $check_already['awb_number'],
               'TransportMode' =>$transport_mode,
               'LogisticsID' => $logistic_partner
           ]; 

        }
       
        $order_json_data_response = json_encode($response_data);
       
        Storage::put('public/order/'.$order_get['shopify_order_id'].'/sap_response_'.$reassign_data['store_code'].'_'.date('YmdHis').'.json',$order_json_data_response,'public');

        return response()->json([
            'success' => true,
            'message' => $response_data['message'],
            'data' => $response_data['data'],
            'TransportMode' => $response_data['TransportMode'],
            'LogisticsID' => $response_data['LogisticsID']
        ],200);
    }
}
