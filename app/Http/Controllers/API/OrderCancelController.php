<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Order;
use App\Models\OrderItem;
use App\Models\OrderStoreAssign;
use App\Models\ReturnOrders;
use App\Jobs\CustomerOrderCancelJob;
use App\Jobs\ShiprocketOrderCancelJob;
use Storage,DB,Mail;
use App\Http\Traits\SendMailTrait;

class OrderCancelController extends Controller
{
    //
    use SendMailTrait;
    public function checkStatus(Request $request){
        $shopify_order_id = $request['shopify_order_id'];
        $order_query = OrderItem::where('shopify_order_id',$shopify_order_id);
        $order_count = $order_query->count();
        $picked_up = false;
        $success = true;
        $return = false;
        $delivered = false;
        $order_data = array();
        $qty_sku_wise = array();

        if($order_count > 0){

            $order_store_assign = new OrderStoreAssign;
            $order_data = OrderStoreAssign::where('shopify_order_id',$shopify_order_id)->get()->toArray();
        
            foreach($order_data as $order_key => $order_value){
                $final_data_quantity_sku_query = OrderStoreAssign::select(\DB::raw('SUM(`quantity`) as total_quantity'),'delivery_date','is_shopify_return')
                                                                ->where('shopify_order_id',$shopify_order_id)
                                                                ->where('shopify_item_id',$order_value['shopify_item_id']);  


                 $final_data_cancel_sku_query = OrderStoreAssign::select(\DB::raw('SUM(`quantity`) - SUM(`cancel_quantity`) as final_quantity'))
                                                                ->where('shopify_order_id',$shopify_order_id)
                                                                 ->where('shopify_item_id',$order_value['shopify_item_id'])
                                                                 ->where('sku',$order_value['sku'])
                                                                 ->where('invoice_no',NULL);

                $final_data_return_sku_query = OrderStoreAssign::select(\DB::raw('SUM(`quantity`) - SUM(`return_quantity`) as final_quantity'),'delivery_date','order_store_id')
                                                            ->where('shopify_order_id',$shopify_order_id)
                                                            ->where('is_delivered',"1")
                                                            ->where('sku',$order_value['sku']);

               
                $final_data_cancel_sku_count = $final_data_cancel_sku_query->count();

                $final_data_return_sku_count = $final_data_return_sku_query->count();
                $final_data_quantity_sku_count = $final_data_quantity_sku_query->count();

                $final_data_cancel_sku = 0;
                $final_data_quantity_sku = 0;
                $final_data_return_sku = 0;

                if($final_data_quantity_sku_count > 0){
                    $final_data_quantity_sku_data = $final_data_quantity_sku_query->first()->toArray();
                    $final_data_quantity_sku = $final_data_quantity_sku_data['total_quantity'];
                }
              
             
                if($final_data_return_sku_count > 0){
                    $final_data_return_sku_data = $final_data_return_sku_query->first()->toArray();
                    $final_data_return_sku = $final_data_return_sku_data['final_quantity'];
                    $qty_sku_wise[$order_value['shopify_item_id']]['delivery_date'] = $final_data_return_sku_data['delivery_date'];
                }
                if($final_data_cancel_sku_count > 0){
                    $final_data_cancel_sku_data = $final_data_cancel_sku_query->first()->toArray();
                    $final_data_cancel_sku = $final_data_cancel_sku_data['final_quantity'];

                }
                $qty_sku_wise[$order_value['shopify_item_id']]['total_qty'] = $final_data_quantity_sku;
                $qty_sku_wise[$order_value['shopify_item_id']]['cancel_qty'] = $final_data_cancel_sku;
                $qty_sku_wise[$order_value['shopify_item_id']]['return_qty'] = $final_data_return_sku;
               
                
                
                $qty_sku_wise[$order_value['shopify_item_id']]['is_shopify_return'] = $final_data_quantity_sku_data['is_shopify_return'];
                if(!empty($order_value['invoice_no'])){
                    $picked_up = true;
                }

                if(!empty($order_value['is_delivered']) && $order_value['is_delivered'] == 1){
                    $delivered = true;
                }
            }
        }
      
        $orders_table_model = new Order;
        // $orders_table_model->setTable($orders_table);
        $order_return_query = $orders_table_model->where('shopify_order_id',$shopify_order_id)->value('is_return');

        if($order_return_query == 1){
            $return = true;
        }
        return response()->json(['success' => true, 'order_data' => $order_data, 'qty_sku_wise' => $qty_sku_wise,'picked_up' => $picked_up,'return'=>$return,'delivered' => $delivered]);
    }
    public function lineItemAssignOrder(Request $request){
        $request_data = $request->all();
        $shopify_order_id = $request_data['shopify_order_id'];
        $order_query = OrderItem::where('shopify_order_id',$shopify_order_id)
                                      ->where('sku',$request_data['line_sku']);
        
        $order_count = $order_query->count();

        $order_store_assign_model = new OrderStoreAssign;
        $order_data = $order_store_assign_model->where('shopify_order_id',$shopify_order_id)
                                        ->with('getOrderItem')
                                        ->where('sku',$request_data['line_sku'])
                                        ->where('invoice_no',NULL)
                                        ->where('cancel_quantity',0)
                                        ->take($request_data['cancelled_qty'])
                                        ->get()->toArray(); 
        $order_items = [];

        foreach ($order_data as $key => $single_order_data) {
            $order_number = $single_order_data['order_number'];
            $order_items[] = [
                "item_id" => $single_order_data['shopify_item_id'],
                "reference_id" => $single_order_data['sap_id'],
                "store_code" => $single_order_data['store_code'],

            ];
        } 

        if($order_count > 0){
            $order_item_data = $order_query->first()->toArray();
    
            if($request_data['type'] == 'cancel'){
                $line_item_id = (int)$order_item_data['shopify_item_id'];

                $total_cancel_quantity =  $order_item_data['cancel_quantity'] + (int)$request_data['cancelled_qty'];

                $total_discount_quantity_wise = $order_item_data['total_discounts'] / $order_item_data['quantity'];

                $request_data['shopify_item_id'] = $line_item_id;
                $request_data['total_cancel_quantity'] =$total_cancel_quantity;
                $request_data['total_discount_quantity_wise'] =$total_discount_quantity_wise;
                $order_items_data['order_items'] = $order_items;
               // dispatch(new ShiprocketOrderCancelJob($request_data));
                dispatch(new CustomerOrderCancelJob($request_data,$order_items_data));
            } else if($request_data['type'] == 'return'){
                $shopify_order_id = $request_data['shopify_order_id'];
                $sku = $request_data['line_sku'];
                $customer_remarks = $request_data['remarks'];
                //$return_qty = $request_data['cancelled_qty'];
                //$return_reason = $request_data['return_reason'];

                $order_store_assign_model = new OrderStoreAssign;
                $order_data = $order_store_assign_model->where('shopify_order_id',$shopify_order_id)
                                        ->where('sku',$sku)
                                        ->update(['return_remark' => $customer_remarks]); 
                $order_item = OrderItem::where('shopify_order_id',$shopify_order_id)->where('sku',$sku)->first();
        
                $order_json = Storage::get('public/order/'.$shopify_order_id.'/order.json','public');
                $order_array = json_decode($order_json ,true);

                $customer_email = $order_array['customer']['email'];
                $customer_name = ucfirst(($order_array['customer']['first_name'])) . ' ' .ucfirst(($order_array['customer']['last_name']));
                $shipping_address = ucfirst(($order_array['shipping_address']['address1'])) . ' ' .ucfirst(($order_array['shipping_address']['address2']));
                $zip = $order_array['shipping_address']['zip'];
                $order_number = $order_array['name'];

                $body_arr = [
                    'title' => $order_item['title'],
                    'sku' => $sku,
                    'quantity' => $order_item['quantity'],
                    'remarks' => $customer_remarks,
                    'price' => $order_item['price'],
                    'email'=>$customer_email,
                    'customer_name'=>$customer_name,
                    'shipping_address'=>$shipping_address,
                    'zip' => $zip,
                    'state' => $order_array['shipping_address']['province'],
                    'city'=>$order_array['shipping_address']['city'],
                    'phone'=>$order_array['shipping_address']['phone'],
                    'order_number'=>$order_number,
                    'total_discounts'=>$order_item['total_discounts']
                ];
                
                try {
                    // $email_ids = ['customer.care@tatainternational.com','marketing.ludhiana@tatainternational.com','tilldh.logistic@tatainternational.com','dipali.binary@gmail.com'];
                    // $mail = array('body' => $body_arr);
                    // $send = Mail::send('mail_send.return_order_mail_send', $mail, function($message) use ($mail,$email_ids) {
                    //     $message->to($email_ids)
                    //             ->subject('order return');
                    //     $message->from('stryder.noreply@tatainternational.com');
                    // });

                    $to = $customer_email;
                    $email = ['customer.care@tatainternational.com','marketing.ludhiana@tatainternational.com','tilldh.logistic@tatainternational.com'];
                    $subject = 'Order Cancel Confirmation Email '.date('Y-m-d H:i:s');
                    $email_data = $body_arr;
                    $this->sendMail($to,$email,$subject,$email_data);

                    // $to = $customer_email;
                    // $email = ['customer.care@tatainternational.com','marketing.ludhiana@tatainternational.com','tilldh.logistic@tatainternational.com'];
                    // $subject = 'Order Cancel Confirmation Email '.date('Y-m-d H:i:s');
                    // $email_data = $body_arr;
                    // $from_email = 'stryder.noreply@tatainternational.com';
                    // $from_name = 'Stryder';
                    // $this->sendMail($to, $email, $subject, $email_data, $from_email, $from_name);

                    
                    return response()->json(['success'=>true,'message'=>'Successfully'],200);
                    
                } catch (\Exception $e){
                    return response()->json($e,500);
                }

                // $order_return_store = new ReturnOrders;
                // $order_return_store->shopify_order_id = $shopify_order_id;
                // $order_return_store->sku = $sku;
                // $order_return_store->title = $order_item['title'];
                // $order_return_store->total_qty = $order_item['quantity'];
                // $order_return_store->return_qty = $return_qty;
                // $order_return_store->return_reason = $return_reason;
                // $order_return_store->save();
            }

        }
        return response()->json(['success'=>true, 'type' => $request_data['type']],200);
    }
}
