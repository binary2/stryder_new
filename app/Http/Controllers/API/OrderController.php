<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Jobs\OrderCreateJob;
use App\Models\OrderStoreAssign;
use Config,Storage,Log;
use App\Jobs\OrderStoreAssignStatusJob;
use App\Jobs\SAPOrderReturnXMLJob;
use App\Jobs\OrderReturnShiprocketJob;
use App\Http\Traits\ShopifyCurlTrait;

class OrderController extends Controller
//class OrderController
{
    use ShopifyCurlTrait;

    public function createOrder(Request $request){
        $order_data = $request->all();
       
        $response = dispatch(new OrderCreateJob($order_data));
        //$this->dispatch(new OrderCreateJob($order_data));
        return response()->json(['success'=>true,'message'=>'order created successfully'],200);
    }

    public function shipRocketStatus(Request $request){

        \Log::info(json_encode($request->all()));
        $order_id = $request['order_id'];
        $awb_number = $request['awb'];
        $current_status = strtoupper($request['current_status']);
        $order_store_assign_table = 'order_store_assign';

        if($current_status == SHIPROCKET_RETURN_PICKED_UP){
            $order_store_assign_model_picked = new OrderStoreAssign;
            $order_store_assign_model_picked->setTable($order_store_assign_table);
            $shopify_order_query = $order_store_assign_model_picked->where('return_awb_number',$awb_number)->first();
            $transaction_id_cancel=$order_store_assign_model_picked->select('sap_id')->where('return_awb_number',$awb_number)->pluck('sap_id');
            $customer_cancel_job_send_mail=[
                 'sap_id'=>$transaction_id_cancel,
                 'shopify_order_id' => $shopify_order_query['shopify_order_id'],
                 'website'=>$type,
                 'cancelled_by' => 'Picked_Up',
                 'restock_type'=>"return"
                 
            ];

        }else{

            $order_store_assign_modal = new OrderStoreAssign;
            $shopify_order_query = OrderStoreAssign::with('getOrder')
            ->where('awb_number',$awb_number);

            $order_store_assign = OrderStoreAssign::where('awb_number',$awb_number)->where('is_shopify_return',null)->get()->toArray();
           // dd($order_store_assign);
            $shopify_order_data_count = $shopify_order_query->count();
            if($shopify_order_data_count > 0){
                $shopify_order_name_data = $shopify_order_query->first();

                $shopify_order_name = $shopify_order_name_data['getOrder'];
                $shopify_order_name_explode = explode('#',$shopify_order_name['name']);

                $order_name = $shopify_order_name_explode[1];
                $order_status_array = [];
                foreach ($order_store_assign as $key => $value) {

                    $fulfillment_id = $value['fulfillment_id'];
                    $store_code = $value['store_code'];
                    if($current_status == 'DELIVERED'){
                        $return_status = Config::get('constant_array.order_status.DELIVERED');
                        $event = 'delivered';
                        $delivered_date = date('Y-m-d');
                        $order_store_assign_modal->where('awb_number',$awb_number)
                        ->where('cancel_quantity',0)
                        ->update([
                            'is_delivered' => 1,
                            'delivery_date' => $delivered_date,
                            'status' => 'Delivered'
                        ]);
                    }else if($current_status == 'RETURN INITIATED'){
                        $return_status = Config::get('constant_array.order_status.RETURN INITIATED');
                    }/*else if($current_status == 'RETURN PICK UP'){
                        $return_status = Config::get('constant_array.order_status.RETURN PICK UP');     
                    }*/else if($current_status == 'RETURN DELIVERED'){
                        $return_status = Config::get('constant_array.order_status.RETURN DELIVERED');     
                    }else if($current_status == 'REFUND INITIATED'){
                        $return_status = Config::get('constant_array.order_status.REFUND INITIATED');     
                    }else if($current_status == "PICKED UP"){
                        $return_status = Config::get('constant_array.order_status.PICKED UP');
                        $order_store_assign_modal->where('awb_number',$awb_number)
                        ->update([
                            'is_shiprocket_picked_up' => 1,
                            'order_picked_up' => date('Y-m-d H:i:s'),
                            'status' => 'PICKED UP'
                        ]);
                    }else if($current_status == "RTO INITIATED" || $current_status == "RTO IN TRANSIT"){
                        
                        $transaction_id_cancel=$order_store_assign_modal->where('awb_number',$awb_number)->whereNull('rto_initiated')->get(); 
                        
                        if(count($transaction_id_cancel))
                        {
                            $return_status = Config::get('constant_array.order_status.RTO INITIATED');
                            $order_store_assign_modal->where('awb_number',$awb_number)
                                                     ->update([
                                                        'rto_initiated' => date('Y-m-d H:i:s'),
                                                        'status' => 'Return Initiated'
                                                    ]);
                           
                            $transaction_id_cancel=$order_store_assign_modal->select('sap_id')->where('awb_number',$awb_number)->pluck('sap_id');  
                            // $customer_cancel_job_send_mail=[
                            //      'sap_id'=>$transaction_id_cancel,
                            //      'shopify_order_id' => $shopify_order_name['shopify_order_id'],
                            //      'website'=>$type,
                            //      'cancelled_by' => 'RTO',
                            //      'restock_type'=>"return"
                                 
                            // ];
                            // dispatch(new WSCanceInShopifyJob($customer_cancel_job_send_mail));                         
                        }

                    }else if($current_status == "RTO DELIVERED"){
                        $order_store_assign_modal->where('awb_number',$awb_number)
                                                 ->update([
                                                    'rto_delivered' => date('Y-m-d H:i:s')
                                                ]);
                    }

                    if(!empty($return_status)){
                        for ($i=1; $i <= $value['quantity']; $i++) {
                        if(!empty($value['sap_id'])){
                            $transaction_id = $value['sap_id'];
                        }else{
                            $transaction_id = $order_value['shopify_item_id'].'_'.$order_value['store_code'].'_'.$i;
                        }

                        $order_status_array[] = [
                            "transaction_id" => $transaction_id,
                            "status" => $return_status,
                            'shopify_item_id' => $value['shopify_item_id'],
                            "store_code" => $value['store_code'],
                            "awb_number"=>$value['awb_number'],
                            "invoice_number"=>$value['sap_pack_id']
                        ];

                        }
                    }
                    
                    $store_assign_status = ['order_store_assign_id'=> $value['order_store_id'],'shopify_order_id'=>$value['shopify_order_id'] ,'shopify_item_id' => $value['shopify_item_id'], 'sku'=> $value['sku'],'store_code'=> $value['store_code'], 'quantity'=> $value['quantity'],'status'=> $current_status,'order_store_assign_table' => 'order_store_assign','order_store_assign_status_table' => 'order_store_assign_status'];

                   // dispatch(new OrderStoreAssignStatusJob($store_assign_status));
                    if(isset($event)){
                        $fulfillment = "/admin/api/2020-10/orders/".$value['shopify_order_id']."/fulfillments/".$fulfillment_id ."/events.json";
                        $events = [
                            'event'=>[
                            'status'=>$event
                        ]
                    ];
                    $fulfillment_data = $this->postcurl('POST',$fulfillment,$events);
                    }
                }

                $date_time=date("ymdHi");
                if($order_status_array != [] ){
                    $content = View('sap/order_status_xml',['order_name' => $order_name,'order_status_array'=>$order_status_array])->render();
            
                    Storage::disk('sftp')->put('order_status/'.$order_name.'_'.$date_time.'.xml',$content,'public');

                    //\Storage::put('public/'.$order_name.'_'.$date_time.'.xml',$content,'public');
                }
            }
 
            return response()->json(['success'=>true]);
        }
    }

    public function createRefund(Request $request){

        \Log::warning(json_encode($request->all()));
        $shopify_order_id = $request['order_id'];

        // $request_data = $request['refunds'];

        $return_line_item = $request['refund_line_items'];
        \Log::info('shopify return request');
        \Log::info($return_line_item);
        $line_item_id = [];
        $sku = [];
        $reference_id = [];
        foreach ($return_line_item as $single_line_item_key => $single_line_item_value) {
            $new_line_item_id = $single_line_item_value['line_item_id'];
            $line_item_id[] = $single_line_item_value['line_item_id'];
            $qty = $single_line_item_value['quantity'];
            $total_qty_sku_wise[] = $single_line_item_value['quantity'];
            $restock_type = $single_line_item_value['restock_type'];
            $sku[] = $single_line_item_value['line_item']['sku'];

            if($restock_type == "return"){

                $order_store_assign_data = OrderStoreAssign::where('shopify_order_id',$shopify_order_id)
                                                                ->where('shopify_item_id',$new_line_item_id)
                                                                ->where('return_awb_number',null)
                                                                ->where('is_shopify_return',null)
                                                                ->where('is_delivered',1)
                                                                ->take($qty)->get()->toArray();

                
                foreach($order_store_assign_data as $order_value){
    
                    if($order_value['status'] == "Delivered"){
                        
                        $reference_id[]= $order_value['sap_id'];
                        $update_data = OrderStoreAssign::where('shopify_order_id',$shopify_order_id)
                                                            ->where('shopify_item_id',$new_line_item_id)
                                                            ->where('is_shopify_return',null)
                                                            ->where('return_awb_number',null)
                                                            ->where('is_delivered',1)
                                                            ->first()->update(['is_shopify_return'=>'1']);
                        
                    }
                }
            }
        }
      
        $unieue_item_id = array_unique($line_item_id);
        $unique_sku = array_unique($sku);

        $qnt_sum =array_sum($total_qty_sku_wise);

        $new_item_array = ['unieue_item_id' =>$unieue_item_id , 'unique_sku' =>$unique_sku , 'qnt_sum' => $qnt_sum , 'reference_id'=> $reference_id];
        if($reference_id != []){
            $return_order_shiprocket = dispatch(new OrderReturnShiprocketJob($new_item_array,$shopify_order_id));
        }
        return response()->json(['success'=>true]);
    }
}
