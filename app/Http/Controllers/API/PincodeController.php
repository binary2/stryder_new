<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\ShiprocketPincodeResponse;
use App\Models\ShiprocketTokenLog;
use Config;

class PincodeController extends Controller
{
    public function pincodeServicable(Request $request)
    {
        $request_data = $request->all();
        
        $shiprockettoken = ShiprocketTokenLog::orderBy('id', 'Desc')->first();

        $shiprocket_url = SHIPROCKET_API_URL;
        $url = $shiprocket_url ."/courier/serviceability/?pickup_postcode=400080&cod=0&weight=0.5&delivery_postcode=".$request_data['pincode']; 
        
        $service_curl = curl_init();
        curl_setopt_array($service_curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
            "authorization: Bearer ".$shiprockettoken['token'],
            ),
        ));

        $service_response = curl_exec($service_curl);
        $service_err = curl_error($service_curl);
        curl_close($service_curl);
        
        // if(isset($request_data['checkemail']))
        // {  
        //     $save_info=new ShiprocketPincodeResponse();
        //     $save_info->email=$request_data['checkemail'];
        //     $save_info->pincode=$request_data['pincode'];
        //     $save_info->pincode_request=$url;
        //     $save_info->pincode_response=$service_response;
        //     $save_info->save();
        // }

        if ($service_err) {
            return response()->json(['success'=>false]);
        } else {
            $service_decode_response = json_decode($service_response,1);

            if(isset($service_decode_response['status']) && $service_decode_response['status'] == 200){
                return response()->json(['success'=>true]);
            }else{
                return response()->json(['success'=>false]);
            }
        }
    }
}
