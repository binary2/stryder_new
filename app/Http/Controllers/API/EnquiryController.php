<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\GeneralEnquiry;
use App\Models\DealershipEnquiry;
use App\Models\Pincode;
use Log;
use App\Http\Requests\EnquiryRequest;

class EnquiryController extends Controller
{
    // public function generalEnquiry(EnquiryRequest $request)
    // {
    //     $request_data = $request->all();
       
    //     try{
    //         $general_enquiry = GeneralEnquiry::firstOrNew(['phone_number'=>$request_data['phone_number']]);
    //         $general_enquiry->name = $request_data['name'];
    //         $general_enquiry->phone_number = $request_data['phone_number'];
    //         $general_enquiry->email = $request_data['email'];
    //         $general_enquiry->state_name = $request_data['state'];
    //         $general_enquiry->city = $request_data['city'];
    //         $general_enquiry->pincode = $request_data['pincode'];
    //         $general_enquiry->comment = $request_data['comment'];
    //         $general_enquiry->save();

    //         $dueDAte = date("Y-m-d");

    //         $general_enquiry = [
    //                 "comment"=>$request_data['comment'],
    //                 "state"=>$request_data['state'],
    //                 "city"=>$request_data['city'],
    //                 "pin_code"=>$request_data['pincode']
    //         ];


    //     $PDATA = [
    //         "title" => "General Enquiry",
    //         "ticket_details" => "General Enquiry",
    //         "due_date" => $dueDAte,
    //         "customer_name"=>$request_data['name'],
    //         "phone"=>$request_data['phone_number'],
    //         "email_id"=>$request_data['email'],
    //        "general_enquiry"=>json_encode($general_enquiry)
    //     ];

    //     $curl = curl_init();

    //     curl_setopt_array($curl, array(
    //       CURLOPT_URL => GENERALENQUIRY_API,
    //       CURLOPT_RETURNTRANSFER => true,
    //       CURLOPT_ENCODING => '',
    //       CURLOPT_MAXREDIRS => 10,
    //       CURLOPT_TIMEOUT => 0,
    //       CURLOPT_FOLLOWLOCATION => true,
    //       CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    //       CURLOPT_CUSTOMREQUEST => 'POST',
    //       CURLOPT_POSTFIELDS =>json_encode($PDATA),
    //       CURLOPT_HTTPHEADER => array(
    //         'Authorization: Basic NDZyMTFnNnl0N2tnOXVodzE0MDhqODA1MWZtdGJxdXN1cXBseTA4OWpqODN5cGZhaTc=',
    //         'Content-Type: application/json',
    //         'Cookie: JSESSIONID=58DA12575EEF84623502E07C6BC1EC03; _KAPTURECRM_SESSION='
    //       ),
    //     ));
            
    //         $response = curl_exec($curl);
            
    //         curl_close($curl);

    //         return response(['success'=>true,'message'=>'Your Data Succesfully saved']);
    //     }catch(\Exception $e){
    //         return response()->json($e);
    //     }
    // }

    public function generalEnquiry(EnquiryRequest $request){
        $request_data = $request->all();
       
        try{
            $general_enquiry = GeneralEnquiry::firstOrNew(['phone_number'=>$request_data['phone_number']]);
            $general_enquiry->name = $request_data['name'];
            $general_enquiry->phone_number = $request_data['phone_number'];
            $general_enquiry->email = $request_data['email'];
            $general_enquiry->state_name = $request_data['state'];
            $general_enquiry->pincode = $request_data['pincode'];
            $general_enquiry->comment = $request_data['comment'];
            $general_enquiry->save();

            return response(['success'=>true,'message'=>'Your Data Succesfully saved']);
        }catch(\Exception $e){
            return response()->json($e);
        }
    }

    // public function dealershipEnquiry(EnquiryRequest $request)
    // {
      
    //     $request_data = $request->all();
        
    //     try{
    //         $general_enquiry = DealershipEnquiry::firstOrNew(['phone_number'=>$request_data['phone_number']]);
    //         $general_enquiry->name = $request_data['name'];
    //         $general_enquiry->phone_number = $request_data['phone_number'];
    //         $general_enquiry->email = $request_data['email'];
    //         $general_enquiry->entity_name = $request_data['entity_name'];
    //         $general_enquiry->state_name = $request_data['state'];
    //         $general_enquiry->city = $request_data['city'];
    //         $general_enquiry->pincode = $request_data['pincode'];
    //         $general_enquiry->comment = $request_data['comment'];
    //         $general_enquiry->save();

    //         $dueDAte = date("Y-m-d");

    //         $dealership_enquiry = [
    //             "comment"=>$request_data['comment'],
    //                 "state"=>$request_data['state'],
    //                 "city"=>$request_data['city'],
    //                 "pin_code"=>$request_data['pincode'],
    //                 "entity_name"=>$request_data['entity_name']
    //         ];


    //     $PDATA = [
    //         "title" => "Dealership Enquiry",
    //         "ticket_details" => "Dealership Enquiry",
    //         "due_date" => $dueDAte,
    //         "customer_name"=>$request_data['name'],
    //         "phone"=>$request_data['phone_number'],
    //         "email_id"=>$request_data['email'],
    //        "dealership_enquiry"=>json_encode($general_enquiry)
    //     ];

    //     $curl = curl_init();

    //     curl_setopt_array($curl, array(
    //       CURLOPT_URL => DEALERSHIP_ENQUIRY_API,
    //       CURLOPT_RETURNTRANSFER => true,
    //       CURLOPT_ENCODING => '',
    //       CURLOPT_MAXREDIRS => 10,
    //       CURLOPT_TIMEOUT => 0,
    //       CURLOPT_FOLLOWLOCATION => true,
    //       CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    //       CURLOPT_CUSTOMREQUEST => 'POST',
    //       CURLOPT_POSTFIELDS =>json_encode($PDATA),
    //       CURLOPT_HTTPHEADER => array(
    //         'Authorization: Basic YTJzdDA0N2Z5c3UxeDFnYTNnbm9udXhobjY3dHI5dmJ6a2VzdnF0d3BqMG85Z2p3MG8=',
    //         'Content-Type: application/json',
    //         'Cookie: JSESSIONID=53CA881FDA1D564D0C331FFD32C6AC2B; _KAPTURECRM_SESSION='
    //       ),
    //     ));

    //     $response = curl_exec($curl);

    //     curl_close($curl);
            
    //         //print_r($general_enquiry);
    //         return response(['success'=>true,'message'=>'Your Data Succesfully saved']);
    //     }catch(\Exception $e){
    //         return rcurl_setopt_array($curl, array(
    //       CURLOPT_URL => DEALERSHIP_ENQUIRY_API,
    //       CURLOPT_RETURNTRANSFER => true,
    //       CURLOPT_ENCODING => '',
    //       CURLOPT_MAXREDIRS => 10,
    //       CURLOPT_TIMEOUT => 0,
    //       CURLOPT_FOLLOWLOCATION => true,
    //       CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    //       CURLOPT_CUSTOMREQUEST => 'POST',
    //       CURLOPT_POSTFIELDS =>json_encode($PDATA),
    //       CURLOPT_HTTPHEADER => array(
    //         'Authorization: Basic YTJzdDA0N2Z5c3UxeDFnYTNnbm9udXhobjY3dHI5dmJ6a2VzdnF0d3BqMG85Z2p3MG8=',
    //         'Content-Type: application/json',
    //         'C
                    
    //                 //print_r($general_enquiry);
    //                 return response(['success'=>true,'message'=>'Your Data Succesfully saved']);
    //             }catch(\Exception $e){
    //                 return rcurl_setopt_array($curl, array(
    //       CURLOPT_URL => DEALERSHIP_ENQUIRY_API,
    //       CURLOPT_RETURNTRANSFER => true,
    //       CURLOPT_ENCODING => '',
    //       CURLOPT_MAXREDIRS => 10,
    //       CURLOPT_TIMEOUT => 0,
    //       CURLOPT_FOLLOWLOCATION => true,
    //       CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    //       CURLOPT_CUSTOMREQUEST => 'POST',
    //       CURLOPT_POSTFIELDS =>json_encode($PDATA),
    //       CURLOPT_HTTPHEADER => array(
    //         'Authorization: Basic YTJzdDA0N2Z5c3UxeDFnYTNnbm9udXhobjY3dHI5dmJ6a2VzdnF0d3BqMG85Z2p3MG8=',
    //         'Content-Type: application/json',
    //         'Cesponse()->json($e);
    //             }
       
    // }

    public function dealershipEnquiry(EnquiryRequest $request)
    {
      
        $request_data = $request->all();
        
        try{
            $general_enquiry = DealershipEnquiry::firstOrNew(['phone_number'=>$request_data['phone_number']]);
            $general_enquiry->name = $request_data['name'];
            $general_enquiry->phone_number = $request_data['phone_number'];
            $general_enquiry->email = $request_data['email'];
            $general_enquiry->entity_name = $request_data['entity_name'];
            $general_enquiry->state_name = $request_data['state'];
            $general_enquiry->pincode = $request_data['pincode'];
            $general_enquiry->comment = $request_data['comment'];
            $general_enquiry->save();
            
            //print_r($general_enquiry);
            return response(['success'=>true,'message'=>'Your Data Succesfully saved']);
        }catch(\Exception $e){
            return response()->json($e);
        }
       
    }

    public function getstateandcity(Request $request)
    {
      
        $request_data = $request->all();
       
        $pincode = $request_data['zip'];
        
        try{
            
            $pincodedata = Pincode::where('pincode',$pincode)->first()->toArray();

            return response()->json(array('success' => true, 'data' => $pincodedata));
           
           // return response(['success'=>true,'data'=>$pincodedata,'city'=>$pincodedata['city'],'state'=>$pincodedata['state_name']]);
        }catch(\Exception $e){
            return response()->json($e);
        }
       
    }
}
