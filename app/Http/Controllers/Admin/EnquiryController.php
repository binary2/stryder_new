<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\GeneralEnquiry;
use App\Models\DealershipEnquiry;
use App\Exports\GeneralEnquiryExport;
use App\Exports\DealershipEnquiryExport;
use Excel;
use App\Models\Order;
use App\Http\Traits\SendMailTrait;
use Illuminate\Support\Facades\Mail;
// use Mail;
use Illuminate\Mail\Message;

class EnquiryController extends Controller
{
    use SendMailTrait;
    public function dealershipEnquiryindex(Request $request)
    {
        if ($request->ajax()) {

            $where_str = '1 = ?';
            $where_params = [1];            

            if ($request->has('sSearch')) {
                $search = $request->get('sSearch');
                $where_str .= " and (phone_number like \"%{$search}%\""
                . " or pincode like \"%{$search}%\""
                . " or email like \"%{$search}%\""
                . " or name like \"%{$search}%\""
                . " or state_name like \"%{$search}%\""
                . " or entity_name like \"%{$search}%\""
                . ")";
            }

            if ($request->get('yoy')[0] != "") {

                $date_range_str = head($request->get('yoy'));
                $date_range_arr = explode(" - ", $date_range_str);
                // dd($date_range_arr);
                
                $range_from = date('Y-m-d', strtotime($date_range_arr[0]));
                $range_from = str_replace('/', '-', $range_from);
                // dd($range_from_date);
                $range_to = date('Y-m-d', strtotime($date_range_arr[1]));
                $range_to = str_replace('/', '-', $range_to);

                $range_condition = " AND (DATE_FORMAT(created_at,'%Y-%m-%d') BETWEEN '$range_from' AND '$range_to') ";
                $where_str .= $range_condition;
            }

            $columns = ['created_at','name','phone_number','email','entity_name','state_name','pincode','comment'];
            
            $cartbuster_list = DealershipEnquiry::select($columns)
            ->whereRaw($where_str, $where_params)->orderBy('id', 'desc'); 

            $cartbuster_list_count = DealershipEnquiry::select($columns)
            ->whereRaw($where_str, $where_params)->count();     

            if($request->get('iDisplayStart') != '' && $request->get('iDisplayLength') != ''){
                $cartbuster_list = $cartbuster_list->take($request->input('iDisplayLength'))
                ->skip($request->input('iDisplayStart'));
            }          

            if($request->input('iSortCol_0')){
                $sql_order='';
                for ( $i = 0; $i < $request->input('iSortingCols'); $i++ )
                {
                    $column = $columns[$request->input('iSortCol_' . $i)];
                    if(false !== ($index = strpos($column, ' as '))){
                        $column = substr($column, 0, $index);
                    }
                    $cartbuster_list = $cartbuster_list->orderBy($column,$request->input('sSortDir_'.$i));   
                }
            }

            $cartbuster_list = $cartbuster_list->get()->toArray();

            $response['iTotalDisplayRecords'] = $cartbuster_list_count;
            $response['iTotalRecords'] = $cartbuster_list_count;
            $response['sEcho'] = intval($request->input('sEcho'));
            $response['aaData'] = $cartbuster_list;

            return $response;
        }
        return view('admin.enquiry.dealership');
    }

    public function generalEnquiryindex(Request $request)
    {
        if ($request->ajax()) {

            $where_str = '1 = ?';
            $where_params = [1];            

            if ($request->has('sSearch')) {
                $search = $request->get('sSearch');
                $where_str .= " and (phone_number like \"%{$search}%\""
                . " or pincode like \"%{$search}%\""
                . " or email like \"%{$search}%\""
                . " or name like \"%{$search}%\""
                . " or state_name like \"%{$search}%\""
                . ")";
            }

            if ($request->get('yoy')[0] != "") {

                $date_range_str = head($request->get('yoy'));
                $date_range_arr = explode(" - ", $date_range_str);
                // dd($date_range_arr);
                
                $range_from = date('Y-m-d', strtotime($date_range_arr[0]));
                $range_from = str_replace('/', '-', $range_from);
                // dd($range_from_date);
                $range_to = date('Y-m-d', strtotime($date_range_arr[1]));
                $range_to = str_replace('/', '-', $range_to);

                $range_condition = " AND (DATE_FORMAT(created_at,'%Y-%m-%d') BETWEEN '$range_from' AND '$range_to') ";
                $where_str .= $range_condition;
            }

            $columns = ['created_at','name','phone_number','email','state_name','pincode','comment'];

            $cartbuster_list = GeneralEnquiry::select($columns)
            ->whereRaw($where_str, $where_params)->orderBy('id', 'desc'); 

            $cartbuster_list_count = GeneralEnquiry::select($columns)
            ->whereRaw($where_str, $where_params)->count();     

            if($request->get('iDisplayStart') != '' && $request->get('iDisplayLength') != ''){
                $cartbuster_list = $cartbuster_list->take($request->input('iDisplayLength'))
                ->skip($request->input('iDisplayStart'));
            }          

            if($request->input('iSortCol_0')){
                $sql_order='';
                for ( $i = 0; $i < $request->input('iSortingCols'); $i++ )
                {
                    $column = $columns[$request->input('iSortCol_' . $i)];
                    if(false !== ($index = strpos($column, ' as '))){
                        $column = substr($column, 0, $index);
                    }
                    $cartbuster_list = $cartbuster_list->orderBy($column,$request->input('sSortDir_'.$i));   
                }
            }

            $cartbuster_list = $cartbuster_list->get()->toArray();

            $response['iTotalDisplayRecords'] = $cartbuster_list_count;
            $response['iTotalRecords'] = $cartbuster_list_count;
            $response['sEcho'] = intval($request->input('sEcho'));
            $response['aaData'] = $cartbuster_list;

            return $response;
        }
        return view('admin.enquiry.general');
    }

    public function exportgeneralenquiryExcel(Request $request){
        try{
            $sheet_name = 'general_enquiry_detail_' . date('Y_m_d_H_i_s') . '.csv';   
            $excel_array = $request->all();

            return Excel::download(new GeneralEnquiryExport($excel_array), $sheet_name);
        }
        catch (Exception $e) {
            Log::error('General Enquiry Export Excel :- ' .json_encode($e));
        }
    }

    public function exportdealershipenquiryExcel(Request $request){
        try{
            $sheet_name = 'dealership_enquiry_bikes_detail_' . date('Y_m_d_H_i_s') . '.csv';   
            $excel_array = $request->all();

            return Excel::download(new DealershipEnquiryExport($excel_array), $sheet_name);
        }
        catch (Exception $e) {
            Log::error('Dealership Enquiry bike Export Excel :- ' .json_encode($e));
        }
    }


    public function checkingtestmail(){
        try{
            $to = ['itsupport.tilinfral3@tatainternational.com'];
            $email = ['vivek@binaryic.co.in','kamatvivek2000@gmail.com','stryder.noreply@tatainternational.com'];
            $subject = 'Order Cancellment Success'.date('Y-m-d H:i:s');
            $email_data = "Cancellation of Order Confirmed<br><br>Order Details are as below<br><br>Order Number :";
            $from = 'stryder.noreply@tatainternational.com';
            $this->sendmail($to,$email,$subject,$email_data,$from);
        }
        catch (Exception $e) {
            Log::error('General Enquiry Export Excel :- ' .json_encode($e));
        }

        // Mail::send([], [], function (Message $message) {
        //     $message->to('vivekkamat.binary@gmail.com')
        //         ->subject('Order Cancellment Success'.date('Y-m-d H:i:s'))
        //         ->setBody('Cancellation of Order Confirmed<br><br>Order Details are as below<br><br>Order Number :', 'text/plain')
        //         ->setFrom('stryder.noreply@tatainternational.com', 'Stryder');

        //         $headers = $message->getHeaders();
        //             if ($headers !== null) {
        //                 $headers->addTextHeader('X-Mailer', 'Laravel Mailer');
        //                 $headers->addTextHeader('X-Priority', '1');
        //                 $headers->addTextHeader('X-MSMail-Priority', 'High');
        //                 $headers->addTextHeader('Importance', 'High');
        //             }
        // });
    }
}
