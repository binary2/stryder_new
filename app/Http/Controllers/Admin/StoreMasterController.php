<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\StoreMaster;
use App\Http\Requests\StoreMasterRequest;
use App\Events\StoreMasterEvent;
use Event;
use App\Jobs\StoreMasterJob;

class StoreMasterController extends Controller
{
    public function create(){

        return view('admin.storemaster.create');
    }

    public function index(Request $request) {

        if($request->ajax()){

            $where_str    = "1 = ?";

            $where_params = array(1); 

            $type = $request->get('type');

            if (!empty($request->input('sSearch'))){

                $search     = $request->input('sSearch');

                $where_str .= " and (store_code like \"%{$search}%\""

                . "or store_name like \"%{$search}%\""

                . "or store_address like \"%{$search}%\""

                . "or store_city like \"%{$search}%\""

                . "or store_pincode like \"%{$search}%\""

                . "or store_state like \"%{$search}%\""

                . "or store_latitude like \"%{$search}%\""

                . "or store_longitude like \"%{$search}%\""

                . "or store_phone_number like \"%{$search}%\""

               // . "or store_email like \"%{$search}%\""

                //. "or store_zone like \"%{$search}%\""

                . ")";

            }                                            

            $columns = ['store_master_id','store_code','store_name','store_address','store_city','store_pincode','store_state','store_latitude','store_longitude','store_phone_number','store_time','updated_at'];

            $categorie_columns_count = StoreMaster::select($columns)

            ->whereRaw($where_str, $where_params)

            ->count();

            $categorie_list = StoreMaster::select($columns)

            ->whereRaw($where_str, $where_params);


            if($request->get('iDisplayStart') != '' && $request->get('iDisplayLength') != ''){

                $categorie_list = $categorie_list->take($request->input('iDisplayLength'))

                ->skip($request->input('iDisplayStart'));

            }          

            if($request->input('iSortCol_0')){

                $sql_order='';

                for ( $i = 0; $i < $request->input('iSortingCols'); $i++ ){

                    $column = $columns[$request->input('iSortCol_' . $i)];

                    if(false !== ($index = strpos($column, ' as '))){

                        $column = substr($column, 0, $index);

                    }

                    $categorie_list = $categorie_list->orderBy($column,$request->input('sSortDir_'.$i));   

                }

            } 

            $categorie_list = $categorie_list->get();

            $response['iTotalDisplayRecords'] = $categorie_columns_count;

            $response['iTotalRecords'] = $categorie_columns_count;

            $response['sEcho'] = intval($request->input('sEcho'));

            $response['aaData'] = $categorie_list->toArray();



            return $response;

        }

        return view("admin.storemaster.index");

    }

    public function store(StoreMasterRequest $request){

        $storemaster_data = $request->all();
        
        $storemaster_data = $this->Dispatch(new StoreMasterJob($storemaster_data));
        //event(new StoreMasterEvent($storemaster_data));         

        return response()->json(['success'=>true]);

    }

    public function show($store_master_id){

        $storemaster_data = StoreMaster::find($store_master_id);

        return view('admin.storemaster.show',compact('storemaster_data'));

    }

    public function update(StoreMasterRequest $request, $store_master_id){

        $storemaster_data = $request->all();

        $storemaster_data['store_master_id'] =$store_master_id;
        $storemaster_data = $this->Dispatch(new StoreMasterJob($storemaster_data));
        //event(new StoreMasterEvent($storemaster_data));   



        return response()->json(['success'=>true]);

    }
     public function destroy(Request $request){

        if($request->ajax()){
            if(is_array($request->id)){ 
                $id=$request->id; 

                $len=count($id);
                for($i=0; $i <$len; $i++) { 
                    StoreMaster::where('store_master_id',$id[$i])->delete();

                }

            }else{

                StoreMaster::where('store_master_id',$request->id)->delete();
            }

            return response()->json(["success"=>true]);

        }

    }

}
