<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Registration;
use Storage;
use Excel;
use App\Exports\RegisterYourBikeExport;

class RegisterBikeController extends Controller
{
    public function RegisterYourBikeindex(Request $request){
        if ($request->ajax()) {
        
            $where_str = '1 = ?';
            $where_params = [1];            

            if ($request->has('sSearch')) {
                $search = $request->get('sSearch');
                $where_str .= " and (phone like \"%{$search}%\""
                . " or bike_type like \"%{$search}%\""
                . " or retail_name like \"%{$search}%\""
                . " or phone like \"%{$search}%\""
                . " or state like \"%{$search}%\""
                . " or pincode like \"%{$search}%\""
                . " or email like \"%{$search}%\""
                . " or retail_name like \"%{$search}%\""
                . ")";
            }

            if ($request->get('yoy')[0] != "") {

                $date_range_str = head($request->get('yoy'));
                $date_range_arr = explode(" - ", $date_range_str);
                // dd($date_range_arr);
                
                $range_from = date('Y-m-d', strtotime($date_range_arr[0]));
                $range_from = str_replace('/', '-', $range_from);
                // dd($range_from_date);
                $range_to = date('Y-m-d', strtotime($date_range_arr[1]));
                $range_to = str_replace('/', '-', $range_to);

                $range_condition = " AND (DATE_FORMAT(created_at,'%Y-%m-%d') BETWEEN '$range_from' AND '$range_to') ";
                $where_str .= $range_condition;
            }

            $columns = ['created_at','bike_type','date_of_purchase','Motor_no','frame_number','retail_name','retail_city','product_name','product_price','personal_retail_name','email','gender','age','phone','state','city','address','pincode','comment','id'];

            $cartbuster_list = Registration::select($columns)
            ->whereRaw($where_str, $where_params)->orderBy('id', 'desc'); 

            $cartbuster_list_count = Registration::select($columns)
            ->whereRaw($where_str, $where_params)->count();     

            if($request->get('iDisplayStart') != '' && $request->get('iDisplayLength') != ''){
                $cartbuster_list = $cartbuster_list->take($request->input('iDisplayLength'))
                ->skip($request->input('iDisplayStart'));
            }          

            if($request->input('iSortCol_0')){
                $sql_order='';
                for ( $i = 0; $i < $request->input('iSortingCols'); $i++ )
                {
                    $column = $columns[$request->input('iSortCol_' . $i)];
                    if(false !== ($index = strpos($column, ' as '))){
                        $column = substr($column, 0, $index);
                    }
                    $cartbuster_list = $cartbuster_list->orderBy($column,$request->input('sSortDir_'.$i));   
                }
            }

            $cartbuster_list = $cartbuster_list->get()->toArray();

            $response['iTotalDisplayRecords'] = $cartbuster_list_count;
            $response['iTotalRecords'] = $cartbuster_list_count;
            $response['sEcho'] = intval($request->input('sEcho'));
            $response['aaData'] = $cartbuster_list;

            return $response;
        }
        return view('admin.register_your_bike.index');
    }

    public function InvoiceDownload($id)
    {
        $invoice_data =  Registration::find($id);
        // dd($invoice_data['invoice_file']);
        $file = storage_path().'/public/invoice/'.$id.'/'.$invoice_data['invoice_file'];

        $headers = array(
            'Content-Type: image/png',
        );

        return response()->download($file,$invoice_data['invoice_file'], $headers);

    }
    public function exportExcel(Request $request){
        try{
            $sheet_name = 'register_your_bikes_detail_' . date('Y_m_d_H_i_s') . '.csv';   
            $excel_array = $request->all();

            return Excel::download(new RegisterYourBikeExport($excel_array), $sheet_name);
        }
        catch (Exception $e) {
            Log::error('Register your bike Export Excel :- ' .json_encode($e));
        }
    }
}
