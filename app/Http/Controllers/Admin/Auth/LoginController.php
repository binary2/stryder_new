<?php

namespace App\Http\Controllers\Admin\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;
use Session;

class LoginController extends Controller
{
    use AuthenticatesUsers;

    protected $redirectTo = '/dealership/enquiry/index';
    protected $maxAttempts = 5;
    protected $decayMinutes = 5;

    protected function redirectTo()
    {
        if (Auth::guard() == 'admin') {
            return '/dealership/enquiry/index';
        }
        return '/admin/login';
    }

    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function showLoginForm()
    {
        if (Session::get('loginornot') == 1) {
            return redirect()->route('dealership.enquiry.index');
        } else {
            return view('admin.login', ['url' => 'admin']);
        }
    }

    protected function guard()
    {
        return Auth::guard('admin');
    }

    public function login(Request $request)
    {
        $this->validate($request, [
            'email' => 'required',
            'password' => 'required',
        ]);

        $credentials = $request->only('email', 'password');
        $remember = $request->has('remember');

        if ($this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);
            return $this->sendLockoutResponse($request);
        }

        if (Auth::guard('admin')->attempt($credentials, $remember)) {
            $this->clearLoginAttempts($request);
            Session::put('loginornot', 1);
            return $this->sendLoginResponse($request);
        }

        $this->incrementLoginAttempts($request);
        if ($this->hasTooManyLoginAttempts($request)) {
            $this->sendLockoutResponse($request);
        }

        throw ValidationException::withMessages([
            $this->username() => [trans('auth.failed')],
        ]);
    }

    public function logout()
    {
        Session::flush();
        Auth::guard('admin')->logout();
        return redirect()->route('admin.login')->with('success', 'Logout successfully.')->with('message_type', 'success');
    }
}
