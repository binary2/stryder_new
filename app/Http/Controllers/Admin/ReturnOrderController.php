<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\OrderItem;
use App\Models\OrderStoreAssign;
use App\Models\ShiprocketTokenLog;
use App\Models\StoreMaster;
use App\Models\ReturnOrders;
use App\Http\Traits\ShiprocketAPITrait;
use App\Jobs\ReturnOrderXMLJob;
use Storage;

class ReturnOrderController extends Controller
{
    //
    use ShiprocketAPITrait;
    public function index(Request $request) {
            if ($request->ajax()) {

            $where_str = '1 = ?';
            $where_params = [1];            

            if ($request->has('sSearch')) {
                $search = $request->get('sSearch');
                $where_str .= " and (title like \"%{$search}%\""
                . " or sku like \"%{$search}%\""
                . ")";
            }

            $columns = ['return_order_id','title','sku','total_qty','return_qty','return_reason','shopify_order_id'];

            $order_list = ReturnOrders::select($columns)
            ->whereRaw($where_str, $where_params); 

            $order_list_count = ReturnOrders::select($columns)
            ->whereRaw($where_str, $where_params)->count();     

            if($request->get('iDisplayStart') != '' && $request->get('iDisplayLength') != ''){
                $order_list = $order_list->take($request->input('iDisplayLength'))
                ->skip($request->input('iDisplayStart'));
            }          

            if($request->input('iSortCol_0')){
                $sql_order='';
                for ( $i = 0; $i < $request->input('iSortingCols'); $i++ )
                {
                    $column = $columns[$request->input('iSortCol_' . $i)];
                    if(false !== ($index = strpos($column, ' as '))){
                        $column = substr($column, 0, $index);
                    }
                    $order_list = $order_list->orderBy($column,$request->input('sSortDir_'.$i));   
                }
            }

            $order_list = $order_list->get()->toArray();

            $response['iTotalDisplayRecords'] = $order_list_count;
            $response['iTotalRecords'] = $order_list_count;
            $response['sEcho'] = intval($request->input('sEcho'));
            $response['aaData'] = $order_list;

            return $response;
        }
        
        return view('admin.return_order.index');
    }

    public function orderStatus(Request $request){

        $request_data = $request->all();
        $type = $request_data['type'];
        $shopify_order_id = $request_data['shopify_order_id'];
        $sku = $request_data['sku'];
        $return_qty = $request_data['qty'];
        $return_reason = $request_data['return_reason'];
        
        if($type == "approve"){
            $order_query = OrderStoreAssign::where('shopify_order_id',$shopify_order_id)
                                      ->where('sku',$sku);

            $order_count = $order_query->count();

            $shiprocket_url = SHIPROCKET_API_URL;
            //$web_url = SHOPIFY_API_URL;
            $get_order_detail = Storage::get('public/order/'.$shopify_order_id.'/order.json');
            $order_data = json_decode($get_order_detail,1);
            $return_order_data = $order_query->where('is_delivered',1)
                                                ->where('return_quantity',0)
                                                ->with('getOrderItem')
                                                ->take($return_qty)
                                                ->get()->toArray();
                                            
            $shiprockettoken = ShiprocketTokenLog::orderBy('id', 'Desc')->first();
                
            $final_return_item = array_reduce($return_order_data, function($accumulator, $element){
                $accumulator[$element['store_code']][] = $element;
                return $accumulator;
            });
            foreach ($final_return_item as $store_key => $store_wise_value) {

                $line_items = [];
                $return_files_xml = [];
                $subtotal_price = 0;

                $return_store_detail = StoreMaster::where('store_code',$store_key)->first();
                // dd($return_store_detail);
                $return_store_code = $return_store_detail['store_code'];
                    
                $return_shiprocket_id = 'R2_'. $order_data['order_number'];
                foreach ($store_wise_value as $return_item_key => $return_item_value) {
                    if($return_item_key == 0){
                        $return_shiprocket_id .= '_'. $return_item_value['order_store_id']; 
                    }
                    $return_file_array = [
                        'line_item_id' => $return_item_value['shopify_item_id'],
                        'sap_id' => $return_item_value['sap_id'],
                        'shopify_order_id' => $shopify_order_id,
                        'quantity' => $return_item_value['quantity'],
                        'store_code' => $store_key
                    ];
                    $return_files_xml[] = $return_file_array;
                    if($return_item_value['get_order_item']['total_discounts'] > 0){
                        $discount = round($return_item_value['get_order_item']['total_discounts'] * $return_item_value['quantity'] / $return_item_value['get_order_item']['quantity'],2);
                    }else{
                        $discount = 0;
                    }
                    $line_item_array = [
                        "name"=> $return_item_value['get_order_item']['title'],
                        // "sku"=> $return_item_value['get_order_item']['sku'],
                        "sku"=> $return_item_value['sap_id']. '_' . $return_item_value['get_order_item']['sku'],
                        "units"=> $return_item_value['quantity'],
                        "selling_price"=> $return_item_value['get_order_item']['unit_price'],
                        "discount"=> $discount,
                        "tax"=> "",
                        "hsn"=> $return_reason
                    ];
                    $line_items[] = $line_item_array;
                    $subtotal_price += $return_item_value['get_order_item']['unit_price'] * $return_item_value['quantity'];
                }
                $address = $order_data['shipping_address']['address1']. ' ' .$order_data['shipping_address']['address2'];

                $order_detail = [
                    "order_id" => $return_shiprocket_id,
                    "order_date" => $order_data['created_at'],
                    "channel_id" => "2737164",
                    "pickup_customer_name" => $order_data['shipping_address']['first_name'],
                    "pickup_last_name" => $order_data['shipping_address']['last_name'],
                    "pickup_address" => $address,
                    "pickup_city" => $order_data['shipping_address']['city'],
                    "pickup_state" => $order_data['shipping_address']['province'],
                    "pickup_country" => $order_data['shipping_address']['country'],
                    "pickup_pincode" => $order_data['shipping_address']['zip'],
                    "pickup_email" => $order_data['email'],
                    "pickup_phone" => substr(str_replace(" ","",$order_data['shipping_address']['phone']), -10),
                    'pickup_location_id' => "797251",
                    "shipping_customer_name" => $return_store_detail['store_name'],
                    "shipping_last_name" => $return_store_detail['store_name'],
                    "shipping_address" => $return_store_detail['store_address'],
                    "shipping_address_2" => $return_store_code,
                    "shipping_city" => $return_store_detail['store_city'],
                    "shipping_pincode" => $return_store_detail['store_pincode'],
                    "shipping_country" => $return_store_detail['store_country'],
                    "shipping_state" => $return_store_detail['store_state'],
                    "shipping_email" => $return_store_detail['store_email'],
                    "shipping_phone" => substr(str_replace(" ","",$return_store_detail['store_phone_number']), -10),
                    "order_items" => $line_items,
                    "payment_method" => "Prepaid",
                    "shipping_charges" => 0,
                    "giftwrap_charges" => 0,
                    "transaction_charges" => 0,
                    "total_discount" => 0,
                    "sub_total" => $subtotal_price,
                    "length" => 0.5,
                    "breadth" => 0.5,
                    "height" => 0.5,
                    "weight" => 0.5,
                ];

                $order_json_data = json_encode($order_detail);
        
                Storage::put('public/order/'.$order_data['id'].'/shiprocket_request_return_'.$store_key.'.json',$order_json_data,'public');
                $order_curl = curl_init();
                curl_setopt_array($order_curl, array(
                    CURLOPT_URL => $shiprocket_url .'/orders/create/return',
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_ENCODING => "",
                    CURLOPT_MAXREDIRS => 10,
                    CURLOPT_TIMEOUT => 0,
                    CURLOPT_FOLLOWLOCATION => true,
                    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                    CURLOPT_CUSTOMREQUEST => "POST",
                    CURLOPT_POSTFIELDS => $order_json_data,
                    CURLOPT_HTTPHEADER => array(
                        "Authorization: Bearer ".$shiprockettoken['token'],
                        "Content-Type: application/json"
                    ),
                ));
                $order_response = curl_exec($order_curl);
                Storage::put('public/order/'.$order_data['id'].'/shiprocket_response_return_'.$store_key.'.json',$order_response,'public');

                curl_close($order_curl);
// //                 $order_response = '{
// //   "order_id": 221900238,
// //   "shipment_id": 221382298,
// //   "status": "RETURN PENDING",
// //   "status_code": 21,
// //   "company_name": "Stryder Cycles Pvt Ltd"
// // }';
                $order_json_decode = json_decode($order_response,1);
                   
                if(isset($order_json_decode['order_id']))
                {
                     $return_shiprocket_order_id = $order_json_decode['order_id'];
                     $return_shiprocket_shipment_id = $order_json_decode['shipment_id'];
                }
                else
                {
                    $get_shiprocket_detail=OrderStoreAssign::select('shiprocket_order_id','shiprocket_shipment_id')->where('shopify_order_id',$shopify_order_id)
                    ->where('sku',$sku)->first(); 


                    $return_shiprocket_order_id = $get_shiprocket_detail['shiprocket_order_id'];
                    $return_shiprocket_shipment_id = $get_shiprocket_detail['shiprocket_shipment_id'];

                }
                $order_assign_table_update = OrderStoreAssign::where('shopify_order_id',$shopify_order_id)
                                    ->where('sku',$sku)
                                    ->where('store_code',$store_key)
                                    ->where('is_delivered',1)
                                    ->limit(count($store_wise_value))
                                    ->update([
                                        'return_shiprocket_id' => $return_shiprocket_id,
                                        'return_store_code' => $return_store_code,
                                        'return_shiprocket_shipment_id' => $return_shiprocket_shipment_id,
                                        'return_shiprocket_order_id' => $return_shiprocket_order_id,
                                        "return_reason"=>$return_reason
                                    ]);
                $check_service = curl_init();
                curl_setopt_array($check_service, array(
                    CURLOPT_URL => $shiprocket_url .'/courier/serviceability/?order_id='.$return_shiprocket_order_id.'&is_return=1&pickup_postcode='.$return_store_detail["store_pincode"].'&delivery_postcode='.$order_data['billing_address']['zip'],
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_ENCODING => "",
                    CURLOPT_MAXREDIRS => 10,
                    CURLOPT_TIMEOUT => 0,
                    CURLOPT_FOLLOWLOCATION => true,
                    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                    CURLOPT_CUSTOMREQUEST => "GET",
                    CURLOPT_HTTPHEADER => array(
                        "Authorization: Bearer ".$shiprockettoken['token'],
                        "Content-Type: application/json"
                    ),
                ));
                $response = curl_exec($check_service);
                $json_decode = json_decode($response);
                curl_close($check_service);
                $awb_generate_data = [
                    'shipment_id' => $return_shiprocket_shipment_id,
                    'is_return  ' => 1
                ];
                $awb_generate_data_json = json_encode($awb_generate_data);
                Storage::put('public/order/'.$order_data['id'].'/shiprocket_awb_request_return_'.$store_key.'.json',$awb_generate_data_json,'public');

                $return_shipment_awb_generate = curl_init();
                curl_setopt_array($return_shipment_awb_generate, array(
                    CURLOPT_URL => $shiprocket_url .'/courier/assign/awb',
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_ENCODING => "",
                    CURLOPT_MAXREDIRS => 10,
                    CURLOPT_TIMEOUT => 0,
                    CURLOPT_FOLLOWLOCATION => true,
                    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                    CURLOPT_CUSTOMREQUEST => "POST",
                    CURLOPT_POSTFIELDS => $awb_generate_data_json,
                    CURLOPT_HTTPHEADER => array(
                        "Authorization: Bearer ".$shiprockettoken['token'],
                        "Content-Type: application/json"
                    ),
                ));
                $return_shipment_awb_generate_response = curl_exec($return_shipment_awb_generate);
                Storage::put('public/order/'.$order_data['id'].'/shiprocket_awb_response_return_'.$store_key.'.json',$return_shipment_awb_generate_response,'public');

//                 
                // $return_shipment_awb_generate_response = '{"awb_assign_status":1,"response":{"data":{"courier_company_id":6,"awb_code":"D40112785","cod":0,"order_id":219223221,"shipment_id":218707994,"awb_code_status":1,"assigned_date_time":{"date":"2022-06-01 16:25:02.265025","timezone_type":3,"timezone":"Asia\/Kolkata"},"applied_weight":0.5,"company_id":2434217,"courier_name":"DTDC Surface","child_courier_name":null,"routing_code":"","rto_routing_code":"","invoice_no":"Retail00015","transporter_id":"88AAACD8017H1ZX","transporter_name":"","shipped_by":{"shipper_company_name":"Stryder Cycle Pvt Ltd","shipper_address_1":"Property No  1838, Plot No B3 8 , A 4, Focal Point","shipper_address_2":"Near Flipkart Warehouse","shipper_city":"Ludhiana","shipper_state":"Punjab","shipper_country":"India","shipper_postcode":"141010","shipper_first_mile_activated":0,"shipper_phone":"8283838689","lat":null,"long":null,"shipper_email":"marketing.stryder@tatainternational.com","rto_company_name":"Stryder Cycle Pvt Ltd","rto_address_1":"Property No  1838, Plot No B3 8 , A 4, Focal Point","rto_address_2":"Near Flipkart Warehouse","rto_city":"Ludhiana","rto_state":"Punjab","rto_country":"India","rto_postcode":"141010","rto_phone":"8283838689","rto_email":"marketing.stryder@tatainternational.com"}}}}';
                $awb_decode_response = json_decode($return_shipment_awb_generate_response,1);
               
                if(isset($awb_decode_response['response']['data'])){
                    $awb_detail = $awb_decode_response['response']['data'];
                    $return_awb_number = $awb_detail['awb_code'];
                    $order_assign_table_update = OrderStoreAssign::where('shopify_order_id',$shopify_order_id)
                                            ->where('sku',$sku)
                                            ->where('store_code',$store_key)
                                            ->where('is_delivered',1)
                                            ->limit(count($store_wise_value))
                                            ->update([
                                                'return_awb_number' => $return_awb_number,
                                                'return_quantity' => 1,
                                                'status' => 'RETURN INITIATED',
                                                'order_return' => date('Y-m-d H:i:s')
                                            ]);

                    $shipment_req = [
                        'shipment_id' => [$awb_detail['shipment_id']]
                    ];
                    $pickup_request_data = [
                        'shipment_req'       => $shipment_req,
                        'ship_url'           => $shiprocket_url,
                        'shiprockettoken'    => $shiprockettoken['token'],
                        'order_id'           => $order_data['id'],
                        'request_file_path'  => 'public/order/'.$order_data['id'].'/courier_generate_return_order_pickup_request_'.time().'.json',
                        'response_file_path' => 'public/order/'.$order_data['id'].'/courier_generate_return_order_pickup_response_'.time().'.json',
                    ];
                    $pickup_response_data = $this->pickupRequest($pickup_request_data);
                  
                    $xml_files_generate = ['return_files_xml' => $return_files_xml, 'awb' => $awb_detail['awb_code'],'order_number' => $order_data['order_number']];
                    dispatch(new ReturnOrderXMLJob($xml_files_generate));

                    ReturnOrders::where('shopify_order_id',$shopify_order_id)->where('sku',$sku)->update(['is_approved'=>1]);
                    return response()->json(['success'=>true]);
                }
            }
        } else {
            $data = ReturnOrders::where('shopify_order_id',$shopify_order_id)->where('sku',$sku)->update(['is_approved'=>2]);
            return response()->json(['success'=>true]);
        }

    }
}
