<?php

namespace App\Http\Controllers\Custom;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use App\Models\Registration;
// use Storage;
use Illuminate\Support\Facades\Storage;
use App\Exports\RegisterYourBikeExport;
use App\Http\Requests\RegisterYourBikeRequest;

class RegisterController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index(RegisterYourBikeRequest $request)
    {

        $invoice_file = $request->file('myfile');
        $originalFileName = $invoice_file->getClientOriginalName();
        $filepart = pathinfo($originalFileName);
        $ext = $filepart['extension'];
        $fname = $filepart['filename'];
        // $extension = pathinfo($originalFileName, PATHINFO_EXTENSION);
        $fileName = $fname.'_'.date('Y-m-d-H-i-s').'.'.$ext;

        $registration = new Registration;
        $registration->bike_type = $request['bike_type'];
        $registration->date_of_purchase = $request['date_of_purchase'];
        $registration->retail_name = $request['retail_name'];
        $registration->retail_city =  $request['retail_city'];
        $registration->product_name = $request['product_name'];
        $registration->product_price = $request['product_price'];
        $registration->personal_retail_name = $request['personal_retail_name'];
        $registration->email = $request['email'] ;
        $registration->gender = $request['gender'];
        $registration->age = $request['age'];
        $registration->phone = $request['number'];
        $registration->state = $request['state'];
        $registration->city = $request['city'];
        $registration->address = $request['address'];
        $registration->pincode = $request['pincode'];
        $registration->comment = $request['comment'];
        $registration->invoice_file = $fileName;
        if($request['Motor_no'] != null && $request['frame_number'] != null){
            $registration->Motor_no = $request['Motor_no'];
            $registration->frame_number = $request['frame_number'];
        }else{
            $registration->Motor_no = '-';
            $registration->frame_number = '-';
        }
        $registration-> save();
       
        $id = $registration->id;
        $name = $originalFileName.'_'.$id;
        $response = Storage::makeDirectory('public/invoice/'.$id);
        $path = $invoice_file->storeAs('public/invoice/'.$id.'/', $fileName);

        return response(['success'=>true,'message'=>'Your Data Succesfully saved']);
           
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        return view('register::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
        return view('register::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
        return view('register::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy($id)
    {
        //
    }

    public function exportExcel(Request $request){
        try{
            $sheet_name = 'register_your_bikes_detail_' . date('Y_m_d_H_i_s') . '.csv';   
            $excel_array = $request->all();

            return Excel::download(new RegisterYourBikeExport($excel_array), $sheet_name);
        }
        catch (Exception $e) {
            Log::error('Register your bike Export Excel :- ' .json_encode($e));
        }
    }
}
