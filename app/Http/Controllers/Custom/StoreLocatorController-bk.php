<?php

namespace App\Http\Controllers\Custom;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\StoreMaster;


class StoreLocatorController extends Controller
{

    public function listingNew($state_name = null,$city_name = null)
    {
        $where_str    = "1 = ?";
        $where_str_city = "1 = ?";
        $where_params = array(0); 
        $city_list = [];
        $selected_city_name = '';
        if(!empty($state_name)){
            $where_params = array(1); 
            $where_str .= " and LOWER(REPLACE(store_state,' ','')) = '$state_name'";
        }
        // dd($state_name);
        if(!empty($city_name)){
            $where_params = array(1); 
            $where_str_city .= " and LOWER(REPLACE(store_city,' ','')) = '$city_name'";
        }

        
        // $store_state=StoreMaster::select('store_state')->distinct('store_state')->OrderBy('store_state')->whereRaw($where_str, $where_params)->pluck('store_state','store_state')->toArray();
        $store_state=StoreMaster::distinct('store_state')->OrderBy('store_state')->pluck('store_state','store_state')->toArray();

        
        $selected_state_name = StoreMaster::whereRaw($where_str, $where_params)->value('store_state');
        if(!empty($state_name)){
            $city_list = StoreMaster::distinct('store_city')->OrderBy('store_city')->whereRaw($where_str, $where_params)->pluck('store_city','store_city')->toArray();

            if(!empty($city_name)){
                $selected_city_name = StoreMaster::whereRaw($where_str_city, $where_params)->value('store_city');
            }
        }

        if(count($store_state) > 1){
            if(!empty($store_state))
            {
                $store_state=array_unique($store_state);
                
                $store_state =[''=>'Select State']+$store_state;
            }
            else
            {
                $store_state =[''=>'Select State']; 
            }
        }

  
        $get_store_name_url = "https://admin3157.gocolors.com/new_go_colors_india/public/getstore-name";
        
        // dd($get_store_name_url);
        $get_store_data_url = "https://admin3157.gocolors.com/new_go_colors_india/public/getstore-data";
        $get_store_all_url = "https://admin3157.gocolors.com/new_go_colors_india/public/" . 'getstore-all';
        $get_store_cutome = "https://test.gocolors.in/apps/s/storelocators/";
        
        $file_view = 'custom.storelocator';
        
        $html = view($file_view,compact('store_state','get_store_name_url','get_store_data_url','get_store_all_url','city_name','get_store_cutome','state_name','selected_state_name','city_list','selected_city_name'))->render();
        // $html = "Westside";
        
        return response($html)->withHeaders(['Content-Type' => 'application/liquid']);
        // return $html;
    }

    public function getStoreName(Request $request)
    {  
        $render=null;
        $StoreName=null;
        $StoreName=$request['store_state'];

        
        if(!empty($StoreName))
        {
        $store_city=StoreMaster::select('store_city')->distinct('store_city')->OrderBy('store_city')->where('store_state',$request['store_state'])->pluck('store_city','store_city')->toArray();
        $store_count=StoreMaster::select('store_master_id')->where('store_state',$request['store_state'])->count();
        $Store_data=StoreMaster::where('store_state',$request['store_state'])->get()->toArray();
        }
        else
        {

        $store_city=StoreMaster::select('store_city')->distinct('store_city')->OrderBy('store_city')->pluck('store_city','store_city')->toArray();
        $store_count=StoreMaster::select('store_master_id')->count();
        $Store_data=StoreMaster::get()->toArray();
        }

        if(!empty($Store_data))
        {
            $detail_url = "https://test.gocolors.in/apps/s/storelocators";
            $render_file = 'custom.render';
            
            $render=view($render_file,compact('store_count','Store_data','StoreName','detail_url'))->render();
        }
        if(!empty($store_city))
        { 
            $store_city = array_unique($store_city);
        }
        else
        {
            return response()->json(['success'=>false,'msg'=>'Not Found']);
        }

        return response()->json(['success'=>true,'data'=>$store_city,'render'=>$render]);
    }

    public function getStoreDetail(Request $request)
    {

        $StoreName=null;
        $where_str = "1 = ?";
        $StoreName=$request['store_city'];

        $where_str_city = "1 = ?";
        $where_str_city .= " AND LOWER(REPLACE(store_city,' ','')) = '$StoreName'";
        
        $where_params = array(1); 

        $store_count = StoreMaster::select('store_master_id')->whereRaw($where_str_city, $where_params)->count();

        

        $Store_data=StoreMaster::whereRaw($where_str_city, $where_params)->get()->toArray();
       
        if(!empty($Store_data))
        {
            $detail_url = "https://test.gocolors.in/apps/s/storelocators";
            $render_file = 'custom.render';
            $render=view($render_file,compact('store_count','Store_data','StoreName','detail_url'))->render();
            // return $render;
            // $render->render();

            return response()->json(['success'=>true,'data'=>$render]);
        }
        else
        {
            return response()->json(['success'=>false,'msg'=>'No Use Found']);
        }
    }

    public function getStoreDetailAll(Request $request)
    {
        $store_count=StoreMaster::select('store_master_id')->count();
        $Store_data=StoreMaster::paginate(15);
        $render = "No Record Found";
        
        if(!empty($Store_data))
        {
            $detail_url = "https://test.gocolors.in/apps/s/storelocators";
            $file_name = 'custom.webview.render';
            $render=view($file_name,compact('store_count','Store_data','detail_url'))->render();
        }
        return response()->json(['success'=>true,'data'=>$render]);
    }

     public function MoreDetail(Request $request,$state,$city,$slug)
    {

        $detail_url = "https://test.gocolors.in/apps/s/storelocators";
        $go_back = "https://test.gocolors.in/apps/s/storelocators";
        $file_name = 'custom.webview.render';
        $detail_file = 'custom.detail';
       
        $StoreMaster_data=StoreMaster::where('store_slug',$slug)->get()->first();

        $where_str    = "1 = ?";
        $where_params = array(1); 

        if(!empty($state)){
            $where_str .= " and LOWER(REPLACE(store_state,' ','')) = '$state'";
        }
 
        $Store_data=StoreMaster::OrderBy('store_state')->whereRaw($where_str, $where_params)->where('store_master_id','!=',$StoreMaster_data['store_master_id'])->get()->toArray();
        $store_count = count($Store_data);
        $StoreName = $state;

        // echo "<pre>";
        // print_r($Store_data);
        // exit();
        $other_city_html = view($file_name,compact('store_count','Store_data','StoreName','detail_url'))->render();

        // $desktop_store_gallery=PhotoGallery::select('images')->where('store_id',$StoreMaster_data['store_master_id'])->where('image_type','desktop')->get()->toArray();
        // $mobile_store_gallery=PhotoGallery::select('images')->where('store_id',$StoreMaster_data['store_master_id'])->where('image_type','mobile')->get()->toArray();

        // if(empty($desktop_store_gallery))
        // {
        //     $desktop_store_gallery=PhotoGallery::select('images')->where('store_id','0')->where('image_type','desktop')->get();
        // }
        // if(empty($mobile_store_gallery))
        // {
        //     $mobile_store_gallery=PhotoGallery::select('images')->where('store_id','0')->where('image_type','mobile_')->get();
        // }

        $html = view($detail_file,compact('StoreMaster_data','go_back','detail_url','other_city_html','state','city','slug'))->render();
        return $html;
        // return response($html)->withHeaders(['Content-Type' => 'application/liquid']);
    }
   
}
