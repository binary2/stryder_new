<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

class RegisterYourBikeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {
        if($request['bike_type'] == 'Regular Bikes'){
            $rules= [
                'bike_type'=>'required',
                'date_of_purchase'=>'required',
                'retail_name'=>'required',
                'retail_city'=>'required',
                'product_name'=>'required',
                'product_price'=>'required|regex:/^([0-9\s\-\+\(\)]*)$/',
                'personal_retail_name'=>'required',
                'email'=>'required|max:50|email|regex:/[^@]+@[^\.]+\..+/',
                'number'=>'required|regex:/^([0-9\s\-\+\(\)]*)$/|max:10|min:10',
                'age'=>'required',
                'state'=>'required',
                'city'=>'required',
                'address'=>'required',
                'pincode'=>'required|regex:/^([0-9\s\-\+\(\)]*)$/|max:6|min:6',
                'myfile'=>'required|max:2048|mimes:jpg,jpeg,pdf',
                'gender'=>'required',
                'comment'=>'required',
                'checkbox'=>'accepted',
            ];
        }else{
            $rules= [
                'bike_type'=>'required',
                'date_of_purchase'=>'required',
                'retail_name'=>'required',
                'retail_city'=>'required',
                'product_name'=>'required',
                'product_price'=>'required|regex:/^([0-9\s\-\+\(\)]*)$/',
                'personal_retail_name'=>'required',
                'email'=>'required|max:50|email|regex:/[^@]+@[^\.]+\..+/',
                'number'=>'required|regex:/^([0-9\s\-\+\(\)]*)$/|max:10|min:10',
                'age'=>'required',
                'state'=>'required',
                'city'=>'required',
                'address'=>'required',
                'pincode'=>'required|regex:/^([0-9\s\-\+\(\)]*)$/|max:6|min:6',
                'myfile'=>'required|max:2048|mimes:jpg,jpeg,pdf',
                'gender'=>'required',
                'comment'=>'required',
                'checkbox'=>'accepted',
                'Motor_no'=>'required',
                'frame_number'=>'required',
            ];
        }
        
        return $rules;
    }

    public function messages(){
        $messages = [
            'bike_type.required'=>'Please Select your bike type',
            'address.required'=>'Please enter your Address',
            'date_of_purchase.required'=>'Please select product purchase date',
            'retail_name.required'=>'Please enter retail name',
            'retail_city.required'=>'Please enter retail city',
            'city.required'=>'Please enter your city',
            'state.required'=>'Please enter your state',
            'product_price.required'=>'Please enter your Product Price',
            'product_price.regex'=>'The Product Price Must be Numeric',
            'product_name.required'=>'Please enter your Product Name',
            'personal_retail_name.required'=>'Please enter Personal Retail Name',
            'number.required'=>'Please enter your Mobile Number',
            'number.regex'=>'The Mobile Number Must be Numeric',
            'number.max'=>'The Mobile Number Must be 10 digits',
            'number.min'=>'The Mobile Number Must be 10 digits',
            'pincode.required'=>'Please Enter Pincode',
            'pincode.regex'=>'The Pincode Must be Numeric',
            'pincode.max'=>'The Pincode Must be 6 digits',
            'pincode.min'=>'The Pincode Must be 6 digits',
            'email.required'=> 'Please Enter your Email In This Format abc@gmail.com',
            //'email.email'=> 'Please Enter your Email In This Format abc@gmail.com',
            'email.regex'=> 'Please Enter your Email In This  abc@gmail.com',
            'myfile.max'=>'Your file allowded size must not be more than 2 MB!',
            'myfile.required'=>'Please Upload Invoice',
            'myfile.mimes'=>'The file Must be Upload jpeg,jpg and pdf format',
            'age.required'=>'Please enter your Age',
            'age.regex'=>'The Age Must be Numeric',
            'age.max'=>'The Age Must be 3 digits',
            'gender.required'=>'Please Select gender',
            'comment.required'=>'Please Enter your comment',
            'checkbox.accepted'=>'You must agree with the terms and conditions',
            'Motor_no.required'=>'Please Enter motor number',
            'frame_number.required'=>'Please Enter your frame number',
        ];
        return $messages;
    }
}
