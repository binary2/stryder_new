<?php

namespace App\Http\Requests;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;
use App\Models\Role;

class StoreMasterRequest extends FormRequest{


    public function authorize(){
        return true;
    }

    public function rules(){
        $id = \Request::segment(4);

        $rules = [

            'store_code' => 'required|max:7|unique:store_master,store_code,'.$id . ',store_master_id',

            'store_name' => 'required|max:100',         

            'store_address' => 'required', 

            'store_slug' => 'required',

            'store_city' => 'required|max:50',         

            'store_pincode' => 'required|min:6',         

            'store_state' => 'required|max:50',

            'store_latitude' => 'sometimes|between:0,99.99',

            'store_longitude' => 'sometimes|between:0,99.99', 

            //'store_email' => 'required|max:50|regex:/[^@]+@[^\.]+\..+/|unique:store_master,store_email,'.$id . ',store_master_id',    

            //'google_map'=>'required',
            'categories'=>'sometimes',

            'google_link'=>'sometimes',
            // 'store_phone_number'=> 'required|min:10',

           
        ];
        return $rules;
    }

}