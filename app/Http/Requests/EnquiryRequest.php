<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

class EnquiryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {
        $segment = $request->segment(2);
        if($segment == 'dealership-enquiry'){
            $rules= [
                'name'=>'required',
                'phone_number'=>'required|regex:/^([0-9\s\-\+\(\)]*)$/|max:10|min:10',
                'email'=>'required|max:50|email|regex:/[^@]+@[^\.]+\..+/',
                'entity_name'=>'required',
                'state'=>'required',
                'pincode'=>'required|regex:/^([0-9\s\-\+\(\)]*)$/|max:6|min:6',
                'comment'=>'required',
            ];
        }else{
            $rules= [
                'name'=>'required',
                'phone_number'=>'required|regex:/^([0-9\s\-\+\(\)]*)$/|max:10|min:10',
                'email'=>'required|max:50|email|regex:/[^@]+@[^\.]+\..+/',
                //'entity_name'=>'required',
                'state'=>'required',
                'pincode'=>'required|regex:/^([0-9\s\-\+\(\)]*)$/|max:6|min:6',
                'comment'=>'required',
            ];
        }
        return $rules;
    }

    public function messages(){

        $messages = [
            'cust_name.required'=>'Please Enter Your Name',
            'phone_number.required'=>'Please Enter Your Mobile Number',
            //'phone_number.phone'=>'The Mobile Number Must be Numeric',
            'phone_number.regex'=>'The Mobile Number Must be Numeric',
            'phone_number.max'=>'The Mobile Number Must be 10 digits',
            'phone_number.min'=>'The Mobile Number Must be 10 digits',
            'email.required'=> 'Please Enter Your Email  In This Format abc@gmail.com',
            //'email.email'=> 'Please Enter Your Email  In This Format abc@gmail.com',
            'email.regex'=> 'Please Enter Your Email  In This  abc@gmail.com',
            'entity_name.required'=> 'Please Enter Entity Name',
            'pincode.required'=> 'Please Enter Pincode',
            'state.required'=> 'Please Enter state',
            'pincode.max'=> 'The Pincode Must be 6 digits',
            'pincode.min'=> 'The Pincode Must be 6 digits',
            'pincode.regex'=>'The pincode Must be Numeric',
            'comment.required'=>'Please Enter Your comment',
        ];
        return $messages;
    }
}
