<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Product;
use App\Jobs\ProductSyncJob;

class ProductSyncCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:ProductSyncCommand';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get products from shopify and Update Product table';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $product_url = '/admin/api/2019-07/products.json';
        $params = ['limit' => 250];
        $products = [
            'product_url' => $product_url,
            'params' => $params,
        ];
        \Bus::dispatch(new ProductSyncJob($products));
        
        return response()->json(['success'=>true]);
    }
}
