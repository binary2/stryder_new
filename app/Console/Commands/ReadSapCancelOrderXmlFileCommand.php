<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Jobs\SingleOrderXMLReadJob;
use File;

class ReadSapCancelOrderXmlFileCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:ReadSapCancelOrderXmlFileCommand';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'By using this command read cancel order xml file from sap';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        /**
         * undocumented constant
         **/
        $file_path = storage_path().'/app/order/cancel_order/store_cancel';
        $all_order_file = File::files($file_path);
        
        foreach($all_order_file as $all_order_file_key => $single_file){
            dispatch(new SingleOrderXMLReadJob($single_file));
        }
    }
}
