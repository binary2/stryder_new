<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DB,Log;
class DeleteOldInventoryTable extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:DeleteOldInventoryTablecron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    { 
        
        // echo "<pre>";
        // print_r('Please uncomment the code first');
        // exit();
        
        $list_of_table_array=[];

        $days_minus = DELETE_INVENTORY_TABLE_DAYS;
        $lable = "-".$days_minus." days";

        $NewDate = Date('Y-m-d',strtotime($lable));

        $query = "SELECT create_time,table_name FROM INFORMATION_SCHEMA.TABLES WHERE table_schema = 'Stryder' AND date(create_time) < '".$NewDate."' And table_name like 'inventory_management_%'";

        
        $db_response=DB::select($query);
        
        if($db_response != [])
        {

          $db_response=json_decode(json_encode($db_response),true);  

          foreach ($db_response as $key => $single_table_name) {
            $list_of_table_array[] = $single_table_name['TABLE_NAME'];
          }
          
          /*Two Level check */
          $regex = '~inventory_management_~';
          $result_diffrent_table = array_filter($list_of_table_array, function($item) use ($regex) {
              return ! preg_match($regex, $item);
          });
          /*Two Level check */
          
          if($result_diffrent_table == [])
          {
            $list_of_table_implode = implode(',',$list_of_table_array);
               
            $query_delete_table = "DROP TABLE IF EXISTS ".$list_of_table_implode; 

            $is_done = DB::statement($query_delete_table);

            if($is_done)
            {
                \Log::info("Cron DeleteOldInventoryTable | table_name :- ".$query_delete_table);
                echo "<pre>";
                print_r($list_of_table_array);
                exit();
            }

          }
          else
          {
              echo "<pre>";
              print_r("Please Contact Stryder Teams");
              exit();
          }
        }
        else
        {
             echo "<pre>";
             print_r($db_response);
             exit();
        }
    }
}
