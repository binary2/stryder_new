<?php
namespace App\Console\Commands;
use Illuminate\Console\Command;
use Illuminate\Foundation\Bus\Dispatchable;
use Mail,Log,File,DB,PDO;
use Illuminate\Support\Facades\Storage;
use App\Http\Traits\SendMailTrait;
//use Storage;

class GetCSVFileCommand extends Command
{
    use Dispatchable;
    use SendMailTrait;
/**
* The name and signature of the console command.
*
* @var string
*/
protected $signature = 'command:getcsvfile';
/**
* The console command description.
*
* @var string
*/
protected $description = 'get csv file from westside csv and make new table in database directly';
/**
* Create a new command instance.
*
* @return void
*/
public function __construct()
{
    parent::__construct();
}
/**
* Execute the console command.
*
* @return mixed
*/
    public function handle()
    {
        try{
            \Log::info('start inventory cron ' . now());

            /*$email = ['binarynotifications@gmail.com','minakshi@binaryic.in','rushik@binaryic.in','saumil.binary@gmail.com','jigar.patel@binaryic.in','rakesh.patel@binaryic.co.in'];
            $subject = 'Inventory sync start of stryder.com '.date('Y-m-d H:i:s');
            $email_view = "mail.inventory_error";
            $email_data = "Start Inventory Update cron";
            $this->SendMail($email_view,$subject,$email_data,$email);*/

            /*$folder_get_server_path_inventory_archive='/inventory/Archive/Inventory_25012023090049.csv';
            $folder_get_server_path_inventory = '/inventory/Inventory_25012023090049.csv';
            $is_done=Storage::disk('sftp')->move($folder_get_server_path_inventory_archive,$folder_get_server_path_inventory);
            //$is_done=Storage::disk('sftp')->move($folder_get_server_path_inventory,$folder_get_server_path_inventory_archive);
                 
           exit;*/

            $folder_get_server_path = '/inventory/';
            $get_all_file_server_files = Storage::disk('sftp')->files($folder_get_server_path);
           //echo "hi";
           //dd("hi ",$get_all_file_server_files);
            
            //if($get_all_file_server_files!=[])
            if(isset($get_all_file_server_files[0]) && !empty($get_all_file_server_files[0]))
            { 
                
                \Log::info('get csv and  dump data in SAP/inventory/inventory.csv' . now());
                //dd("if",$get_all_file_server_files);
                //dd($get_all_file_server_files);
                $table_remain_query = 'ALTER TABLE inventory_management RENAME TO inventory_management_'.date('Y_m_d_H_i_s');
                DB::statement($table_remain_query);
                //create table
                $table_create_query = "CREATE TABLE `inventory_management` (`sku` varchar(10) DEFAULT NULL,`store_code` varchar(20) DEFAULT NULL COMMENT 'location',`quantity` integer(20) DEFAULT NULL COMMENT 'quantity of vairant',`is_update` tinyint(1) DEFAULT 0 COMMENT 'update',`created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,`updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ) ENGINE=InnoDB DEFAULT CHARSET=utf8";
                DB::statement($table_create_query);


                foreach ($get_all_file_server_files as $key => $single_file){

                    \Log::info('get_all_file_server_files ' . $single_file);
    
                     $filename = last(explode('/',$single_file));
             
                     $content_file=Storage::disk('sftp')->get($single_file);
    
                     //dd("content ",$content_file);
    
                     //$url_path_static='/SAP/inventory/inventory.csv';
                     $url_path_static='/SAP/inventory/'.$filename;
     
                     $is_put = Storage::disk('public')->put($url_path_static,$content_file);                   
                     
                     //$is_put=Storage::disk('local')->put($url_path_static,$content_file,'public');
     
                     $folder_get = storage_path().'/app/public'.$url_path_static;                  
                      
                     $query = 'LOAD DATA LOCAL INFILE "'.$folder_get.'" INTO TABLE inventory_management FIELDS TERMINATED BY ";" LINES TERMINATED BY "\n" IGNORE 1 LINES';
                     
                     $inventory_dump = DB::connection()->getpdo()->exec($query);
                        
                     if($inventory_dump) {  
                         $folder_get_server_path_inventory=$folder_get_server_path.$filename;
     
                         $folder_get_server_path_inventory_archive='/inventory/Archive/'.$filename;
                         $is_done=Storage::disk('sftp')->move($folder_get_server_path_inventory, $folder_get_server_path_inventory_archive);
                                                 
                     }
                 }

               
            }
            else{
               
                \Log::info('get csv command not run' . now());
                //dd("fdfd",$get_all_file_server_files);
                \Log::info('NO file found!' . Now());

                /*$email = ['binarynotifications@gmail.com','minakshi@binaryic.in','rushik@binaryic.in','saumil.binary@gmail.com','jigar.patel@binaryic.in','rakesh.patel@binaryic.co.in'];
                $subject = 'Inventory sync inventory filenot found of stryder.com '.date('Y-m-d H:i:s');
                $email_view = "mail.inventory_error";
                $email_data = "Inventory Update cron File Not Found";
                $this->SendMail($email_view,$subject,$email_data,$email);*/
            }

           

            \Log::info('end inventory cron ' . now());
        }
        catch (Exception $e) {
            Log::error($e);
            $to = 'binarynotifications@gmail.com';
            $email = ['vivek@binaryic.co.in','binarynotifications@gmail.com','minakshi@binaryic.in','rushik@binaryic.in'];
            $subject = 'Inventory Sync Issue '.date('Y-m-d H:i:s');
            $email_data = "Error in Inventory Sync in Database";
            $this->SendMail($to,$email,$subject,$email_data);
        }
    }
}