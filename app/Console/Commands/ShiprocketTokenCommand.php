<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Jobs\ShiprocketTokenJob;
use Config;

class ShiprocketTokenCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:ShiprocketTokenCommand';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get Token from shiprocket and stored in database every night 12:05';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $creadential_array = Config::get('constant_array.shiprocket_array');
        $token_array = ['creadential_array' => $creadential_array, 'ship_url' => SHIPROCKET_API_URL];
        dispatch(new ShiprocketTokenJob($token_array));
    }
}
