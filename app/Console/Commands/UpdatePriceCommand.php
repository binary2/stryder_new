<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\ProductPrice;
use App\Models\Product;
use App\Jobs\ProductPriceJob;

class UpdatePriceCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:UpdatePriceCommand';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $products = ProductPrice::where('is_update','0')->groupBy('sku')->get()->toArray();
        

        if($products != "" || $products != null){

            foreach ($products as $key => $single_product) {
                \Log::info('Update Price Cron Start');
                dispatch(new ProductPriceJob($single_product));
            }
        }
    }
}
