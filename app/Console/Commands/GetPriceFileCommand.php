<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Storage,DB;

class GetPriceFileCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:GetPriceFileCommand';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        try{
            \Log::info('Start price cron ' . now());

            $folder_get_server_path = '/price/';
            $get_all_file_server_files = Storage::disk('sftp')->files($folder_get_server_path);

            if($get_all_file_server_files!=[])
            { 
                $table_remain_query = 'ALTER TABLE product_price RENAME TO product_price_'.date('Y_m_d_H_i_s');
                DB::statement($table_remain_query);
                //create table
                $table_create_query = "CREATE TABLE `product_price` ( `sku` varchar(20) DEFAULT NULL,`store_code` varchar(5) DEFAULT NULL,`price` double DEFAULT NULL,`discount` double DEFAULT NULL,`absolute_val_dis` double DEFAULT NULL,`selling_price` double DEFAULT NULL,`is_update` tinyint(1) DEFAULT 0,`id` int(10) unsigned NOT NULL AUTO_INCREMENT,`created_at` timestamp DEFAULT current_timestamp(),`updated_at` timestamp NOT NULL DEFAULT current_timestamp(),PRIMARY KEY (`id`)) ENGINE=InnoDB DEFAULT CHARSET=utf8";
                DB::statement($table_create_query);

                \Log::info('Create new price table');
            }

            foreach ($get_all_file_server_files as $key => $single_file){
                $filename = last(explode('/',$single_file));
                
                $content_file=Storage::disk('sftp')->get($single_file);

                $url_path_static='/SAP/price/price.csv';
                $is_put=Storage::disk('public')->put($url_path_static,$content_file,'public');
                $folder_get = storage_path().'/app/public'.$url_path_static;
                
                 
                $query = 'LOAD DATA LOCAL INFILE "'.$folder_get.'" INTO TABLE product_price FIELDS TERMINATED BY ";" LINES TERMINATED BY "\n" IGNORE 1 LINES';
                
                $inventory_dump = DB::connection()->getpdo()->exec($query);
                   
                if($inventory_dump) {  
                    \Log::info('Price data loaded in database');
                    $folder_get_server_path_inventory=$folder_get_server_path.$filename;

                    $folder_get_server_path_inventory_archive='/price/Archive/'.$filename;
                    $is_done=Storage::disk('sftp')->move($folder_get_server_path_inventory, $folder_get_server_path_inventory_archive);
                }
            }

        }
        catch (Exception $e) {
            Log::error($e);
            // $email = ['binarynotifications@gmail.com','minakshi@binaryic.in','rushik@binaryic.in','saumil.binary@gmail.com'];
            // $subject = 'Price sync error of stryder.com '.date('Y-m-d H:i:s');
            // $email_view = "mail.inventory_error";
            // $email_data = $e;
            // $this->SendMail($email_view,$subject,$email_data,$email);
        }
    }
}
