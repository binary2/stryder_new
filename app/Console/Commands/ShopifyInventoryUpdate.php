<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Product;
use App\Models\InventoryManagement;
use App\Jobs\ShopifyInventoryUpdateJob;
//use Log,DB,PDO;
use App\Http\Traits\ShopifyCurlTrait;
use Mail,Log,File,DB,PDO;

class ShopifyInventoryUpdate extends Command
{
    use ShopifyCurlTrait;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */

    protected $signature = 'command:ShopifyInventoryUpdate';
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'sum csv';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(){
        
        $product_inventory_update = DB::select('SELECT SUM(quantity) AS quantity,sku FROM inventory_management GROUP BY sku ORDER BY quantity DESC');
        
        $quantity_sum = json_decode(json_encode($product_inventory_update), true);

        foreach ($quantity_sum as $key => $single_product) {            
            dispatch(new ShopifyInventoryUpdateJob($single_product));
        }

       
        
        /*$email = ['binarynotifications@gmail.com','minakshi@binaryic.in','rushik@binaryic.in','saumil.binary@gmail.com','jigar.patel@binaryic.in','rakesh.patel@binaryic.co.in'];
        $subject = 'Inventory sync update of stryder.com '.date('Y-m-d H:i:s');
        $email_view = "mail.inventory_update";
        $email_data = "Inventary update";
        $this->SendMail($email_view,$subject,$email_data,$email);*/


    }
}
