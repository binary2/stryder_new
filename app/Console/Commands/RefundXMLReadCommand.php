<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Jobs\RefundXMLReadJob;

class RefundXMLReadCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:RefundXMLReadCommand';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Read refund files';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $xml_array = ['shiprocket_url' => SHIPROCKET_API_URL, 'location_id' => SHOPIFY_LOCATION_ID];
        dispatch(new RefundXMLReadJob($xml_array));
    }
}
