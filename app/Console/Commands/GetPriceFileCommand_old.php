<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Storage, DB;

class GetPriceFileCommand_old extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:GetPriceFileCommand';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */

     /*
        This Laravel console command retrieves CSV files from a remote server's /price/ folder, imports the data from these files into a database table named product_price, and archives the processed CSV files. The command also renames the existing database table with a timestamp for backup if new data is found in the CSV files.
     */
    public function handle()
    {
        try {
            // Log that the price cron has started
            \Log::info('start price cron ' . now());

            // Define the folder path on the remote server where CSV files are located
            $folder_get_server_path = '/price/';

            // Get a list of all files in the specified folder on the remote server
            $get_all_file_server_files = Storage::disk('sftp')->files($folder_get_server_path);

            // Check if there are any files in the folder
            if ($get_all_file_server_files != []) {
                // Rename the existing database table with a timestamp for backup
                $table_remain_query = 'ALTER TABLE product_price RENAME TO product_price_' . date('Y_m_d_H_i_s');
                DB::statement($table_remain_query);
                
                // Create a new database table for product prices
                $table_create_query = "CREATE TABLE `product_price` ( `sku` varchar(20) DEFAULT NULL,`store_code` varchar(5) DEFAULT NULL,`price` double DEFAULT NULL,`is_update` tinyint(1) DEFAULT 0,`id` int(10) unsigned NOT NULL AUTO_INCREMENT,`created_at` timestamp DEFAULT current_timestamp(),`updated_at` timestamp NOT NULL DEFAULT current_timestamp(),PRIMARY KEY (`id`)) ENGINE=InnoDB DEFAULT CHARSET=utf8";
                DB::statement($table_create_query);
            }

            // Process each CSV file in the folder
            foreach ($get_all_file_server_files as $key => $single_file) {
                // Extract the filename from the file path  
                $filename = last(explode('/', $single_file));

                // Get the content of the CSV file from the remote server
                $content_file = Storage::disk('sftp')->get($single_file);

                // Define the local path to store the CSV file
                $url_path_static = '/SAP/price/price.csv';
                $is_put = Storage::disk('public')->put($url_path_static, $content_file, 'public');
                $folder_get = storage_path() . '/app/public' . $url_path_static;

                // Define the SQL query to load data from the CSV file into the database table
                $query = 'LOAD DATA LOCAL INFILE "' . $folder_get . '" INTO TABLE product_price FIELDS TERMINATED BY ";" LINES TERMINATED BY "\n" IGNORE 1 LINES';

                // Execute the SQL query to import data from the CSV file
                $inventory_dump = DB::connection()->getpdo()->exec($query);

                // If data import is successful, move the CSV file to the Archive folder
                if ($inventory_dump) {
                    $folder_get_server_path_inventory = $folder_get_server_path . $filename;

                    $folder_get_server_path_inventory_archive = '/price/Archive/' . $filename;
                    $is_done = Storage::disk('sftp')->move($folder_get_server_path_inventory, $folder_get_server_path_inventory_archive);
                }
            }
        } catch (Exception $e) {
            // Log any exceptions that occur during the process
            Log::error($e);
        }
    }
}
