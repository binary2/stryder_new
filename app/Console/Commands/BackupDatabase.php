<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Process\Process;
use App\Http\Traits\SendMailTrait;
use Illuminate\Support\Facades\File;
use Carbon\Carbon;

class BackupDatabase extends Command
{

    use SendMailTrait;

    protected $signature = 'backup:database';

    protected $description = 'Backup the database to a SQL file';

    public function handle()
    {
        ini_set('max_execution_time', 0);
        set_time_limit(0);

        $databaseName = config('database.connections.mysql.database');

        $backupFileName = 'backup_' . date('Y-m-d_H') . '.sql';


        $folderPath = public_path('Database/');

        if (!file_exists($folderPath)) {
            mkdir($folderPath, 200, true);
        }

        // $storagePath = $folderPath . '/' . $backupFileName;
        $storagePath = $folderPath . $backupFileName;
        // dd($storagePath);

        $command = [
            // "mysqldump --single-transaction --skip-lock-tables '$databaseName'  --set-gtid-purged=OFF",
            'mysqldump',
            '--single-transaction',
            '--skip-lock-tables',
            '--user=' . config('database.connections.mysql.username'),
            '--password=' . config('database.connections.mysql.password'),
            '--host=' . config('database.connections.mysql.host'),
            '--port=' . config('database.connections.mysql.port'),
            $databaseName,
            '--result-file=' . $storagePath
        ];

        // dd($command);

        $process = new Process($command);
        $process->setTimeout(0);
        $process->run();



        if ($process->isSuccessful()) {

            $from = ['stryder.noreply@tatainternational.com'];
            $to = ['jigar.binary@gmail.com'];
            $cc = ['binarynotifications@gmail.com', 'rakesh.patel@binaryic.co.in', 'vivek@binaryic.co.in', 'parth@binaryic.co.in', 'vijay@binaryic.co.in', 'tejash@binaryic.in', 'tejashweta@gmail.com', 'helpdesk@binaryic.com'];
            $subject = 'Stryder Database Dump --' . $backupFileName;
            $msg = "<b>Hello Binary Team,</b><br><br>
                        Database Dump Successfully Every Friday 12AM - " . $backupFileName;
            $response = $this->sendMail($to, $cc, $subject, $msg, $from);

            $this->info("Database backup saved to: " . $storagePath);
        } else {

            $from = ['stryder.noreply@tatainternational.com'];
            $to = ['jigar.binary@gmail.com'];
            $cc = ['binarynotifications@gmail.com', 'rakesh.patel@binaryic.co.in', 'vivek@binaryic.co.in', 'parth@binaryic.co.in', 'vijay@binaryic.co.in', 'tejash@binaryic.in', 'tejashweta@gmail.com', 'helpdesk@binaryic.com'];
            $subject = 'Stryder Database Dump --' . $backupFileName;
            $msg = "Hello Binary Team,<br><br>
                        Database Dump Faile Every Friday 12AM  - " . $backupFileName;
            $response = $this->sendMail($to, $cc, $subject, $msg, $from);

            $this->error("Database backup failed: " . $process->getErrorOutput());
        }

        $directory = public_path('Database/'); // Change this to the path of your directory
        $files = File::files($directory);


        $date15DaysAgo = date('Y-m-d', strtotime("-2 week"));
        foreach ($files as $file) {

            if ($file->getExtension() === 'sql') {
                $formattedDate = Carbon::parse(Carbon::createFromTimestamp($file->getMTime())->toDateTimeString())->format('Y-m-d');
                if ($date15DaysAgo > $formattedDate) {
                    $path = $file->getRealPath();
                    if (file_exists($path)) {
                        unlink($path);
                        $this->info("Deleted previous week's backup: " . $file);
                    }
                } else {
                    $this->info("No backups found from last week to delete.");
                }
            }
        }
    }
}
