<?php

namespace App\Listeners;

use App\Events\StoreMasterEvent;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Foundation\Bus\DispatchesJobs;
use App\Jobs\StoreMasterJob;

class StoreMasterListener
{
    use DispatchesJobs;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(StoreMasterEvent $event)
    {
        $storemaster_data = $event->storemaster_data;
        $storemaster_data = $this->Dispatch(new StoreMasterJob($storemaster_data));
    }
}
