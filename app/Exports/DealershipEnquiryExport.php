<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use App\Models\DealershipEnquiry;

class DealershipEnquiryExport implements FromCollection, WithHeadings, WithEvents, ShouldAutoSize
{
	use Exportable;

    public function __construct($data) {
        $this->data = $data;
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        $data = $this->data;
       
        $sel_date = $data['yoy'];
        
        $where_str = '1 = ?';
        $where_params = [1];            

        if ($sel_date[0] != "") {
            $date_range_str = head($sel_date);
            $date_range_arr = explode(" - ", $date_range_str);
            
            $range_from = date('Y-m-d', strtotime($date_range_arr[0]));
            $range_from = str_replace('/', '-', $range_from);
            $range_to = date('Y-m-d', strtotime($date_range_arr[1]));
            $range_to = str_replace('/', '-', $range_to);

            $range_condition = " AND (DATE_FORMAT(created_at,'%Y-%m-%d') BETWEEN '$range_from' AND '$range_to') ";
            $where_str .= $range_condition;
        }

        $columns = ['name','phone_number','email','entity_name','state_name','pincode','comment','created_at'];

        $cartbuster_list = DealershipEnquiry::select($columns)
            ->whereRaw($where_str, $where_params)
            ->get()->toArray();
               
        return collect($cartbuster_list);
    }

    public function headings(): array{
    	$row = ['Name','Phone Number','Email','Entity Name','State Name','Pincode','Comment','Enquiry Date'];

    	return $row;
    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class    => function(AfterSheet $event) {
                $cellRange = 'A1:J1';  /*All headers*/
                $event->sheet->getDelegate()->getStyle($cellRange)->getFont()->setBold(true);
            },
        ];
    }
}
