<?php

namespace App\Models;

//use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;


class Warehouse extends Model
{
   // use HasFactory;

    protected $table = 'warehouses';
    protected $primaryKey = 'id';
    protected $fillable = ['id','store_code','store_name','store_address','store_pincode','store_state','store_city','store_email','store_phone_number','created_at','updated_at'];

    public function getCenterInventory(){
        return $this->hasOne('App\Models\InventoryManagement','warehouse_code','warehouse_code');
    }
        
}
