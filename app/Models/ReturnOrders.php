<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ReturnOrders extends Model
{
    use HasFactory;

    protected $table = 'return_orders';
    protected $primaryKey = 'return_order_id';
    protected $fillable = ['shopify_order_id','sku','title','total_qty','return_qty','return_reason','is_approved'];
    public $timestamp = true;
}
