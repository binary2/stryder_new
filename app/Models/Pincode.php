<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Pincode extends Model
{
    protected $table="pincodes";
    protected $primaryKey = 'pincode_id';
    protected $fillable = ['pincode_id','pincode','city','state','zone','country','active_status','warehouse_code_1','warehouse_code_2','warehouse_code_3'];
}
