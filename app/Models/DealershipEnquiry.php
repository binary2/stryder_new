<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DealershipEnquiry extends Model
{
    protected $table = "dealership_enquiry";
    protected $primaryKey = "id";
    protected $fillable = ['name','phone_number','email','entity_name','state_name','city','pincode','comment','created_at','updated_at'];

}
