<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GeneralEnquiry extends Model
{
    protected $table = "general_enquiry";
    protected $primaryKey = "id";
    protected $fillable = ['name','phone_number','email','state_name','city','pincode','comment','created_at','updated_at'];

}
