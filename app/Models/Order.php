<?php

namespace App\Models;

//use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Order extends Model
{
    use softDeletes;
    
    protected $table = "orders";
    protected $fillable = ['id','shopify_order_id','customer_name','customer_email','note','total_price','subtotal_price','total_tax','financial_status','total_discounts','discount_code','shipping_charge','name','fulfillment_status','order_number','is_return','created_at','updated_at'];

    protected $dates = ['deleted_at'];

    protected $softDelete = true;
    
    public function getOrderItem(){

        return $this->hasMany('App\Models\OrderItem','shopify_order_id','shopify_order_id');
    }
}
