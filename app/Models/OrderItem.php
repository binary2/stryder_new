<?php

namespace App\Models;

//use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OrderItem extends Model
{
     use softDeletes;
    
    protected $table = "order_items";
    protected $fillable = ['order_id','shopify_order_id','shopify_item_id','variant_id','title','quantity','cancel_quantity','sku','variant_title','fulfillment_service','product_id','gift_card','price','unit_price','compare_price','fulfillment_status','awb_no','state','created_at','updated_at','gst','total_discounts','discount_code'];

    protected $dates = ['deleted_at'];

    protected $softDelete = true;
}
