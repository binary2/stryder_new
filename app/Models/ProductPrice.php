<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProductPrice extends Model
{
    use HasFactory;

    protected $table = 'product_price';
    protected $primaryKey = 'id';
    protected $fillable = ['sku','store_code','price','is_update'];
    public $timestamps = true;
}
