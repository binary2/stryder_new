<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderStoreAssignStatus extends Model
{
    protected $table = 'order_store_assign_status';

    protected $primaryKey = 'order_store_assign_status_id';

    protected $fillable = ['order_store_assign_id','shopify_order_id','shopify_item_id','sku','store_code','quantity','status','note'];

}
