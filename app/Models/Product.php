<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{
    use HasFactory;

    protected $table="products";
    
    protected $primaryKey = 'product_id';

    protected $fillable = ['shopify_product_id','title','body_html','vendor','product_type','handle','tags','published_scope','option_json','image','images','product_json','shopify_variant_id','variant_title','variant_price','variant_sku','variant_position','variant_inventory_policy','variant_compare_at_price','','variant_fulfillment_service','variant_inventory_management','variant_option1','variant_option2','variant_option3','variant_taxable','variant_barcode','variant_grams','variant_image_id','variant_weight','variant_weight_unit','variant_inventory_item_id','variant_inventory_quantity','variant_old_inventory_quantity','variant_requires_shipping','single_variant_json','is_sync','shopify_inventory_quantity'];


    protected $dates = ['deleted_at'];

    protected $softDelete = true;

    public function setVariantJsonAttribute($value)
    {
        $this->attributes['variant_json'] = json_encode($value);
    }

    public function sapProductMapping(){
    return $this->hasOne('App\Models\InventoryManagement','sku','variant_sku');

    }

    public function getVariantJsonAttribute($value)
    {
        return json_decode($value,true);
    }

    public function setOptionJsonAttribute($value)
    {
        $this->attributes['option_json'] = json_encode($value);
    }

    public function getOptionJsonAttribute($value)
    {
        return json_decode($value,true);
    }

    public function setImagesAttribute($value)
    {
        $this->attributes['images'] = json_encode($value);
    }
    public function getImagesAttribute($value)
    {
        return json_decode($value,true);
    }

    public function setImageAttribute($value)
    {
        $this->attributes['image'] = json_encode($value);
    }
    public function getImageAttribute($value)
    {
        return json_decode($value,true);
    }

    public function setProductJsonAttribute($value)
    {
        $this->attributes['product_json'] = json_encode($value);
    }
    public function getProductJsonAttribute($value)
    {
        return json_decode($value,true);
    }

    public function setSingleVariantJsonAttribute($value)
    {
        $this->attributes['single_variant_json'] = json_encode($value);
    }
    public function getSingleVariantJsonAttribute($value)
    {
        return json_decode($value,true);
    }
}
