<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class State extends Model
{
    use HasFactory;

    protected $table="states";
    protected $primaryKey = 'state_id';
    protected $fillable = ['destination','zone','priority_1','priority_2','priority_3'];
}
