<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OrderStoreAssign extends Model
{
    use softDeletes;

    protected $table = 'order_store_assign';

    protected $primaryKey = 'order_store_id';

    protected $fillable = ['shopify_order_id','shopify_item_id','title','sku','binary_id','binary_int_id','sap_id','shiprocket_id','store_code','quantity','total_quantity','awb_number','status','shiprocket_order_id','shiprocket_shipment_id','courier_partner','invoice_no','invoice_nature_transaction','invoice_total_gross_amt','invoice_total_discount','invoice_net_sp','invoice_total_amt_payable','invoice_transaction_id','is_delivered','is_surface','is_order_generate', 'fulfillment_id','is_mail','is_out_for_delivery','delivery_date','is_check','order_received','order_pickup','order_out_for_delivery','order_delivered','order_cancel','order_return','order_refund','order_return_pickup','order_number','is_shiprocket_picked_up','order_picked_up','sap_pack_id','is_refund','refund_amount','refund_date','is_shopify_return'];

    protected $dates = ['deleted_at'];
    public function getOrderItem(){
        return $this->hasOne('App\Models\OrderItem','shopify_item_id','shopify_item_id');
    }
    public function getOrder(){
        return $this->hasOne('App\Models\Order','shopify_order_id','shopify_order_id');
    }
    public function getReturnOrder(){
        return $this->hasOne('App\Models\ReturnOrderItem','order_item_id','shopify_item_id');
    }
    public function getStoreMaster(){
        return $this->hasOne('App\Models\Warehouse','store_code','store_code');
    }

    protected $softDelete = true;

    public $timestamps = true;

}
