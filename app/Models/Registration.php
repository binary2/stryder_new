<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
// use Illuminate\Database\Eloquent\Factories\HasFactory;

class Registration extends Model
{
  
    protected $table = "registration";
    protected $primaryKey = "id";
    protected $fillable = ['bike_type','date_of_purchase','retail_name','retail_city','product_name','product_price','personal_retail_name','email','gender','age','phone','state','city','address','pincode','comment','invoice_file'];
    
}
