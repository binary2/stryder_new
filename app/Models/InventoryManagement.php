<?php

namespace App\Models;

//use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class InventoryManagement extends Model
{
    //use HasFactory;
    protected $table = "inventory_management";
    // protected $table = "inventory_management_testing";
    protected $fillable = ['store_code','sku','quantity'];
}
