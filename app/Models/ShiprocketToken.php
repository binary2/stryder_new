<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ShiprocketToken extends Model
{
    protected $table = 'shiprocket_token';
    protected $fillable = ['token'];
}
