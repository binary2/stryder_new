<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Custom\StoreLocatorController;
use App\Http\Controllers\Admin\Auth\LoginController;
use App\Http\Controllers\Admin\EnquiryController;
use App\Http\Controllers\Admin\RegisterBikeController;
use App\Http\Controllers\Admin\StoreMasterController;
use App\Models\Order;
use App\Models\OrderItem;
use App\Models\OrderStoreAssign;
use App\Jobs\SAPShiprocketOrderJob;
use App\Jobs\InvoiceXMLReadJob;
use App\Http\Controllers\Admin\ReturnOrderController;
use App\Console\Commands\ShiprocketTokenCommand;





/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

//Route::get('/', function () {     return view('welcome'); })->middleware(['auth.shopify'])->name('home');

Route::get('/',function(){
	dd(1235);
});
Route::get('/testing',function(){
    phpinfo();
});

Route::get('/testing-job',function(){
    $sap_shiprocket_order_job = [
                'order_id' => "4793951322364",
                'store_code' => "2203",
                'sap_id' => [ 
                    0 => "1000162393",
                    1 => "1000162394"],
                'shiprocket_url' => "https://apiv2.shiprocket.in/v1/external",
            ];
    dispatch(new SAPShiprocketOrderJob($sap_shiprocket_order_job));
});

// Route::get('test',function(){
//     $html = "Gautam";
//     return response($html)->withHeaders(['Content-Type' => 'application/json']);
// });

// Route::get('storelocators/{state_name?}/{city_name?}',[StoreLocatorController::class,'listingNew']);

// Route::get('storelocators/{state}/{city}/{slug}',[StoreLocatorController::class,"MoreDetail"])->name('custom.storeLocator.detail');

Route::post('/getstore-name',[StoreLocatorController::class,'getStoreName'])->name('StoreName.get');

Route::post('/getstore-data',[StoreLocatorController::class,'getStoreDetail'])->name('StoreDetail.get');

Route::get('/getstore-all',[StoreLocatorController::class,'getStoreDetailAll'])->name('StoreDetail.getAll');

//Auth::routes();

Route::get('/admin/login', [LoginController::class, 'showLoginForm'])->middleware('guest')->name('admin.login');
Route::post('/admin/login-post', [LoginController::class, 'login'])->name('admin.login.post');
Route::get('/admin/logout', [LoginController::class, 'logout'])->name('admin.logout');
Route::get('/home', 'HomeController@index')->name('home');

Route::middleware('admin')->namespace('Admin')->prefix('admin')->group( function() {
    Route::get('dealership/enquiry/index',[EnquiryController::class,'dealershipEnquiryindex'])->name('dealership.enquiry.index');
    Route::get('general/enquiry/index',[EnquiryController::class,'generalEnquiryindex'])->name('general.enquiry.index');
    Route::get('general/enquiry/export',[EnquiryController::class,'exportgeneralenquiryExcel'])->name('general.enquiry.export');
    Route::get('dealership/enquiry/export',[EnquiryController::class,'exportdealershipenquiryExcel'])->name('dealership.enquiry.export');


    Route::get('register-your-bikes/index',[RegisterBikeController::class,'RegisterYourBikeindex'])->name('register.bike.index');
    Route::get('register-your-bikes/export',[RegisterBikeController::class,'exportExcel'])->name('register.bike.export');

    Route::get('/invoice/download/{id}',[RegisterBikeController::class,'InvoiceDownload'])->name('invoice-download');


    /*store master route*/
    Route::get('storemasters', [StoreMasterController::class,'index'])->name('storemasters.index');
    Route::get('storemasters/create', [StoreMasterController::class,'create'])->name('storemasters.create');
    Route::post('storemasters/store', [StoreMasterController::class,'store'])->name('storemasters.store');
    Route::get('storemasters/{id}', [StoreMasterController::class,'show'])->name('storemasters.edit');
    Route::post('storemasters/update/{id}', [StoreMasterController::class,'update'])->name('storemasters.update');
    route::post('/storemasters/destroy',[StoreMasterController::class,'destroy'])->name('storemasters.destroy');

    Route::get('/return-orders',[ReturnOrderController::class,'index'])->name('return_order.index');
    Route::post('/return-orders',[ReturnOrderController::class,'orderStatus'])->name('return_order.status');

});
//Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::get('storage_put',function(){
$content= View('sap/invoice_xml');
//$exists = Storage::disk('sftp')->download('invoicepdf/9231650018.pdf');
$exists = Storage::disk('sftp')->put('invoicexml/update_invoicexml/1111.xml',$content);

    return $exists;

});

Route::get('dowload-invoice/{shopify_order_id}',function($shopify_order_id){

    $invoice_no = OrderStoreAssign::select('invoice_no')
                                                ->where('invoice_no','!=',null)
                                                ->where('is_delivered',1)
                                                ->where('shopify_order_id',$shopify_order_id)
                                                ->get()->toArray();

    $invoice_numbers = array_column($invoice_no, 'invoice_no');
    $unique_invoice_number = array_unique($invoice_numbers);
    
    $zip = new ZipArchive();
    $zip_name = $shopify_order_id.".zip"; // Zip name
    $zip->open($zip_name,  ZipArchive::CREATE);

    foreach ($unique_invoice_number as $key => $single_invoice_number) {
        $get_invoice = Storage::disk('sftp')->get('invoicepdf/'.$single_invoice_number.'.pdf');
         $zip->addFromString(basename($single_invoice_number.".pdf"),$get_invoice);  
    }
    $zip->close(); 
   return response()->download(public_path($zip_name));
});

Route::get('/call-job',function(){
     $xml_array = ['shiprocket_url' => SHIPROCKET_API_URL, 'location_id' => SHOPIFY_LOCATION_ID];
     dispatch(new InvoiceXMLReadJob($xml_array));
});


Route::get('shiprocket-token-generate',function(){
    dispatch(new ShiprocketTokenCommand());
});