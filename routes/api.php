<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\API\EnquiryController;
use App\Http\Controllers\API\PincodeController;
use App\Http\Controllers\Custom\RegisterController;
use App\Http\Controllers\API\OrderController;
use App\Http\Controllers\API\ReAssignController;
use App\Http\Controllers\API\OrderCancelController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::post('general-enquiry',[EnquiryController::class,'generalEnquiry']);
Route::post('dealership-enquiry',[EnquiryController::class,'dealershipEnquiry']);

Route::get('getstate-city',[EnquiryController::class,'getstateandcity']);

//check pincode serviceable or not
Route::post('pincode-servicable',[PincodeController::class,'pincodeServicable']);

Route::post('/register-data',[RegisterController::class,'index']);

//create order api
Route::post('create-order',[OrderController::class,'createOrder']);
Route::post('get-shiprock-status',[OrderController::class,'shipRocketStatus']);

//Awb Number generate and order push into shiprocket
Route::post('/regenerate-awb',[ReAssignController::class,'lineItemAssignOrder']);
Route::post('/order-line-item-changes',[OrderCancelController::class,'lineItemAssignOrder']);
Route::post('/check-order-status',[OrderCancelController::class,'checkStatus']);


//shopify create refund webhook

Route::post('/create-refund',[OrderController::class,'createRefund']);