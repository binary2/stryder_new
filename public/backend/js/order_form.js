var order_form = angular.module('order_form',['ui.bootstrap'],function($interpolateProvider){ 
	$interpolateProvider.startSymbol('<%'); 
	$interpolateProvider.endSymbol('%>'); 
});
var add_product_form = angular.element('#add_data');
var approve_denide_form = angular.element('#customer_data');

order_form.controller('OrderFormController', function($rootScope,$scope,$http,$modal){
	$scope.product_json = product_json;
    $scope.search_term_product = '';
    //Main selected product
	$scope.selected_product = {};
    //list to show after add
    $scope.main_product_selected = {};
    $scope.main_product_selected['discount'] = 0;
    $scope.main_product_selected['sub_amount'] = 0;
    $scope.main_product_selected['shipping'] = 0;
    $scope.main_product_selected['total_tax'] = 0;
    $scope.main_product_selected['total_amount'] = 0;

    $scope.main_product_selected_count = 0;
	$scope.search_product_flag = true;
	$scope.search_product_varinat_flag = false;
    $scope.current_customer = '';
    $scope.new_customer_string = '+ Create New Customer';
    $scope.selected_address = '';
    var shipping = 0;
    var total_tax = 0;
    var discount = 0;

	// $scope.productModelOpen = function () {
	// 	var modalInstance = $modal.open({
	// 		templateUrl: 'product_add.html',
	// 		controller: AddProductController
	// 	});

	// 	modalInstance.result.then(function (selectedItem) {
	// 		$scope.selected = selectedItem;
	// 	}, function () {
	// 	});
	// };
    $scope.checkValue = function(value,flag) {
        if (flag == 1 && value != undefined) {
            return true;
        }else{
            if (flag == 0 && value == undefined) {
                return true;
            }else{
                return false;
            }
        }
    }

	$scope.$watch('search_term_product', function() {
    	if ($scope.search_term_product != undefined) {
    		$scope.search_product_varinat_flag = false;
    		$scope.search_product_flag = true;
    	}
  	}, true);

	$scope.$watch('search_basic', function() {
    	var search_basic = $scope.search_basic;
        console.log(search_basic)
    	if (search_basic != undefined) {
    		$scope.search_term_product = $scope.search_basic;
    		$scope.search_product_varinat_flag = false;
    		$scope.search_product_flag = true;
    		add_product_form.modal('show');
    	}
  	}, true);

  	$scope.makeProductList = function(single_product){
  		$scope.single_variant = single_product.product_json['variants'];

  		$scope.selected_product[single_product.product_json.id] = {};
        $scope.selected_product[single_product.product_json.id]['product'] = single_product.product_json;

  		$scope.search_product_flag = false;
  		$scope.search_product_varinat_flag = true;
  	}

    $scope.addProduct = function(selected_product){
        $scope.main_product_selected = {}
        $scope.main_product_selected_count = 0;

        angular.forEach($scope.selected_product,function(v,k){
            var variant_q = v.selected_qty;

            angular.forEach(variant_q,function(v_i,k_i){
                if (v_i.qty > 0) {
                    if ($scope.main_product_selected[v.product.id] == undefined) {
                        $scope.main_product_selected[v.product.id] = {};
                    }
                    $scope.main_product_selected_count = $scope.main_product_selected_count + 1;

                    $scope.main_product_selected[v.product.id]['product'] = v.product;
                    
                    if ($scope.main_product_selected[v.product.id]['selected_qty'] == undefined) {
                        $scope.main_product_selected[v.product.id]['selected_qty'] = {};
                    }

                    $scope.main_product_selected[v.product.id]['selected_qty'][k_i] = v_i;

                    angular.forEach(v.product.variants,function(f_v,f_i){                        
                        if (f_v.id == k_i) {
                            $scope.main_product_selected[v.product.id]['selected_qty'][k_i]['title'] = f_v.title;
                            $scope.main_product_selected[v.product.id]['selected_qty'][k_i]['price'] = f_v.price;
                            $scope.main_product_selected[v.product.id]['selected_qty'][k_i]['ava_qty'] = f_v.inventory_quantity;
                        }
                    })
                }
            });
        });
        add_product_form.modal('hide');
    }

    $scope.$watch('main_product_selected', function() {
        var sub_amount = 0;
        angular.forEach($scope.main_product_selected,function(single_product_v,single_product_k){
            if (single_product_v.product != undefined) {
                angular.forEach(single_product_v.selected_qty,function(f_v,f_i){
                    sub_amount = (parseFloat(sub_amount) + parseFloat(f_v.price))*parseInt(f_v.qty);
                })
            }
        });

        if ($scope.main_product_selected['discount'] == undefined) {
            $scope.main_product_selected['discount'] = discount;
        }

        if ($scope.main_product_selected['shipping'] == undefined) {
            $scope.main_product_selected['shipping'] = shipping;
        }

        if ($scope.main_product_selected['total_tax'] == undefined) {
            $scope.main_product_selected['total_tax'] = total_tax;
        }

        $scope.main_product_selected['discount'] = $scope.calculateDiscount();
        $scope.main_product_selected['sub_amount'] = $scope.calculateSubAmount(sub_amount);
        $scope.main_product_selected['shipping'] = $scope.calculateShipping();
        $scope.main_product_selected['total_tax'] = $scope.calculateTax();
        $scope.main_product_selected['total_amount'] = $scope.calculateTotalAmount();
    }, true);

    $scope.calculateSubAmount = function(sub_amount){
        sub_amount = sub_amount - $scope.main_product_selected['discount']
        return parseFloat(sub_amount.toFixed(2));
    };

    $scope.calculateDiscount = function(){
        discount = parseFloat($scope.main_product_selected['discount']);
        return parseFloat(discount.toFixed(2));
    }

    $scope.calculateShipping = function(){
        shipping = parseFloat($scope.main_product_selected['shipping']);
        return parseFloat(shipping.toFixed(2));
    }

    $scope.calculateTax = function(){
        total_tax = ((parseFloat($scope.main_product_selected['sub_amount']) + parseFloat($scope.main_product_selected['shipping'])) * 10)/100;
        console.log(total_tax)
        return parseFloat(total_tax.toFixed(2));
    };

    $scope.calculateTotalAmount = function(){
        var total_amount = $scope.main_product_selected['sub_amount'] + $scope.main_product_selected['total_tax'] + $scope.main_product_selected['shipping'];
        total_amount = parseFloat(total_amount);
        return parseFloat(total_amount.toFixed(2));
    };

    //code for get list of customer
    
    $scope.getCustomerList = function(customer_search){
        $http.post(customer_search_url, {
            customer_search : customer_search
        }).success(function(response) {
            $scope.customer_list = response.customer_data;
        }).error(function(response){
            
        });
    };

    //check customer

    $scope.checkCustomer = function(current_customer){
        if (current_customer.first_name != $scope.new_customer_string) {
            $scope.current_customer = current_customer;
            $scope.selected_address = $scope.current_customer.default_address;
            $scope.customer_list = [];

            console.log($scope.current_customer)
        }else{
            $scope.form_type = 'new_customer';
            approve_denide_form.modal('show');        
        }
    };

    //change address 
    $scope.openCustomerForm = function(type){
        $scope.form_type = type;
        approve_denide_form.modal('show');
    };

    $scope.selectAddress = function(change_address){
        if (change_address != 'new') {
            $scope.selected_address = JSON.parse(change_address);
            console.log($scope.selected_address)
        }else{
            $scope.selected_address = '';
        }
    };

    $scope.customerCancel = function(){
        if ($scope.form_type == 'new_address') {
            $scope.selected_address = $scope.current_customer.default_address;
        }
        approve_denide_form.modal('hide');
    };

    //Remove customer
    $scope.removeCustomer = function(){
        $scope.current_customer = '';
    }

    //create new customer
    $scope.newCustomer = function(selected_customer){
        $('.error_msg').text('');
        $('#error_message').html('');

        $http.post(customer_new_url, {
            selected_customer : selected_customer
        }).success(function(response) {
            if (response.success == true) {
                $scope.current_customer = response.customer;
                $scope.selected_address = $scope.current_customer.default_address;
                approve_denide_form.modal('hide');
            }else{
                var error_html = '';
                var counter = 1;
                angular.forEach(response.errors.errors,function(value,key){
                    console.log(value,key);
                    value = value[0];
                    key = key.replace('.',' ');
                    error_html += '<span id="" class="help-inline text-danger error_msg">'+counter+') '+key +' '+value+'</span><br>';
                    counter++;
                })
                $('#error_message').html(error_html);
            }
        }).error(function(response){
            if (response.errors != '') {
                angular.forEach(response.errors,function(value,key){
                    key = key.replace('.','_');
                    value = value[0];
                    $('#'+key+'_error').text(value);
                })
            }
            console.log(response,'response');
        });
    }

    //update or create address
    $scope.updateAddress = function(selected_address){
        console.log(updateAddress);
    }

    //save order
    $scope.saveOrder = function(){
        console.log($scope.current_customer,$scope.selected_address,$scope.main_product_selected)

        $http.post(order_create, {
            current_customer : $scope.current_customer,selected_address : $scope.selected_address,main_product_selected : $scope.main_product_selected
        }).success(function(response) {
            if (response.success == true) {
                location.reload();
            }
        }).error(function(response){
        });
    };
});