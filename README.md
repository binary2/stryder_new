# Stryder_new

## Gitlab

Gitlab URL : https://gitlab.com/binary2/stryder_new

Owner of Gitlab :

Permission to Other Team member :
 : Owner

Jigar Patel : Maintainer

Rakesh Patel : Developer

## Gamil Account Details

 - using this account send mail
Name: Stryder Cron

Email ID: stryder.cron@gmail.com

URL: gmail.com

## SMTP Server Details

MAIL_MAILER=smtp

MAIL_HOST=smtp.gmail.com

MAIL_PORT=587

MAIL_USERNAME=stryder.cron@gmail.com

MAIL_ENCRYPTION=tls

MAIL_FROM_NAME=StryderCron

## Shopify App Name, key and detail

Shopify URL : https://stryder-bikes-til.myshopify.com

Live Theme name : Dawn Live- 17-05-2022


## FTP

Production
	
	User name : tejash.shah-binary

	host : 14.140.252.152


UAT Server
	
	User name : tejash.shah-binary

	host : 14.140.95.186

## SFTP for both UAT & Production

User name :- sftp Ecomm_MiddleWare@135.53.31.2

## Putty Connection

Production

	host :- 14.140.252.152

	User name :- tejash.shah-binary
	

Uat

	host : 14.140.95.186

	User name : tejash.shah-binary

## Laravel front URL & Database Name & Detail

Laravel front URL : https://mdlw.tatainternational.com/

## Database

Production

	URL :- https://mdlw.tatainternational.com/adminer.php?

	User name :- tejash.shah-binary
	

Uat
	URL :- https://mdlw-uat.tatainternational.com/adminer.php?

	User name :- tejash.shah-binary

## Admin Url

Production :- https://mdlw.tatainternational.com/admin/login

UserName:  admin@gmail.com

Uat :- https://mdlw-uat.tatainternational.com/admin/login

## Shopify Webhook Detail

Refund create : https://mdlw.tatainternational.com/api/create-refund

Order creation : https://mdlw.tatainternational.com/api/create-order


## Log viewver

URL : https://mdlw.tatainternational.com/log-viewer/logs

## Horizon detail


## Third party API & URLs

    - SHIPROCKET -
    
	  API_URL: https://apiv2.shiprocket.in/v1/external
	
General Enquiry  API: https://tatastryder.kapturecrm.com/add-ticket-from-other-source.html/v.2.0
	
Dealership Enquiry API: https://novology.kapturecrm.com/add-ticket-from-other-source.html/v.2.0
   

## Cron JOB & Command Detail

1)Command: ShiprocketTokenCommand

  Description: This code is using cURL to send a POST request to a Shiprocket authentication API endpoint to get an access token. The token is then saved to a database using Eloquent ORM. : Every night 12:05

  Time : 12:05

  Frequency: Every night 

  Url: 5 12 * * * php /var/www/html/stryder_new/artisan command:ShiprocketTokenCommand

2)Command: getcsvfile

  Description: This code handles the inventory management for a website. It checks for new inventory files, loads their data into a MySQL database, and archives the processed files. In case of errors, it sends an email notification. : Every 3 Hour and 5 minutes

  Time : 06:05 09:05 12:05 03:05

  Frequency: Every 3 Hour and 5 minutes

  Url: 5 */3 * * * php /var/www/html/stryder_new/artisan command:getcsvfile

3)Command: ShopifyInventoryUpdate	

  Description: This code updates the inventory of a product in a Shopify store by sending a POST request to the Shopify API. It first retrieves the product data based on its SKU, then constructs the inventory data and sends it to the API. If the update is successful, it updates the inventory status in the database. Any exceptions are logged. : Every 3 Hour and 20 minutes

  Time : 06:20 09:20 12:20 03:20

  Frequency: Every 3 Hour and 20 minutes

  Url:20 */3 * * * php /var/www/html/stryder_new/artisan command:ShopifyInventoryUpdate

4)Command: ProductSyncCommand

  Description: This code retrieves product data from a Shopify store using a specified API endpoint and parameters. It then saves the data using a queued job. If there is a "next" link in the API response, it retrieves the next set of data and dispatches the same job again with updated parameters. : At 12:30

  Time : 12:30

  Frequency: Every Day

  Url:30 12 * * * php /var/www/html/stryder_new/artisan command:ProductSyncCommand

5)Command: InvoiceReadCron

  Description: This is a PHP script that appears to be handling the processing of an XML invoice file. It retrieves data from the XML file and uses it to update records in a database. It also calls a method called "reAssignStore" to re-assign store codes for certain items. : every 5 minutes

  Time : every 5 minutes

  Frequency: Every Day

  Url: */5 * * * * php /var/www/html/stryder_new/artisan command:InvoiceReadCron

6)Command: RefundXMLReadCommand

  Description: This code defines a PHP function called "handle" that moves a refund XML file from a remote server to a local server, parses the file content to retrieve refund details, and updates the corresponding order data if a refund has been processed successfully. : At minute 1 past every 12th hour

  Time : At minute 1 past every 12th hour

  Frequency: Every Day

  Url: 1 */12 * * * php /var/www/html/stryder_new/artisan command:RefundXMLReadCommand

7)Command:DeleteOldInventoryTablecron

  Description: This code deletes MySQL tables from the 'Stryder' database whose table name starts with 'inventory_management_' and has not been updated in a certain number of days, as specified by the constant 'DELETE_INVENTORY_TABLE_DAYS'. The table names are retrieved from the 'INFORMATION_SCHEMA.TABLES' table and dropped using a SQL query. The code also performs some error handling and logging. : At 12:15

  Time : 12:15

  Frequency: Every Day

  Url: 15 12 * * * php /var/www/html/stryder_new/artisan command:DeleteOldInventoryTablecron
